function z=gpinterp(x,y,xs,sigma) ;
% Given values y of a function at points x, estimate the value at points
% xs using Gaussian process. sigma is the spatial extent, passed to 'kernel'
%
% Jan Kybic, September 2012


assert(size(x,1)==1) ;  
assert(size(y,1)==1) ;  
n=size(x,2) ; 
assert(size(y,2)==n) ;
assert(size(xs,1)==1) ;
m=size(xs,2);

% first find the linear approximation
X= [ ones(n,1)  x' ] ; % columns are basis vectors
XX=X'*X ;
rhs=X'*y' ;
beta=XX\rhs ; % column vector
ylin=(X*beta)' ; % linear fit, row vector
r=y-ylin ; % residual

% linear interpolation at the 'output' points
ylins=([ ones(m,1)  xs' ]*beta)' ; % row vector

covm=zeros(n) ; % covariance matrix
for i=1:n,
  for j=1:n,
    if i>=j,
      d=abs(x(i)-x(j)) ;
      w=gpkernel(d,sigma) ;
      covm(i,j)=w ;
      if i>j,
        covm(j,i)=w ;
      end ;
    end ;
  end ;
end ;

ky=inv(covm)*r' ;

z=ylins ;

% GP interpolation
for i=1:m,
  w=gpkernel(abs(x-xs(i)),sigma) ;
  z(i)=z(i)+w*ky ;
end ;


  