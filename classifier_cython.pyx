# Classifiers in Python
#
# Jan Kybic, November 2013
#
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True

import numpy as np
import scipy
import scipy.optimize
import bigtools
import pdb
import time
import segregtools
import math
import sys

from segregtools cimport *

cimport numpy as np
cimport cython

from libc.math cimport log
from libc.math cimport exp

#cimport segregtools

# DTYPE = np.uint8
# ctypedef np.uint8_t DTYPE_t
# DTYPEs = np.uint16
# ctypedef np.uint16_t DTYPEs_t
# DTYPEw = np.int
# ctypedef np.int_t DTYPEw_t
# DTYPEf = np.float
#ctypedef np.float_t DTYPEf_t

#ctypedef segregtools.DTYPEf_t DTYPEf_t

cdef void dotv(DTYPEf_t[:,:] a, DTYPEf_t[:] b, DTYPEf_t[:] y) nogil:
     """ Matrix-vector multiplication """
     cdef int m=a.shape[0]
     cdef int n=a.shape[1]
     # assert b.shape[0]==n
     cdef int i,j
     cdef DTYPEf_t s

     for i in xrange(m):
         s=0.
         for j in xrange(n):
             s+=a[i,j]*b[j]
         y[i]=s


cdef inline DTYPEf_t sumv(DTYPEf_t[:] a) nogil:
    """ Sum a vector """
    cdef DTYPEf_t s=0.
    cdef int n = a.shape[0]
    cdef int i

    for i in xrange(n):
        s+=a[i]
    return s



class SoftmaxClassifier:
    """ train and evaluate a softmax classifier. Assume that the first
        element of the feature vector is always 1 """

    def __init__(self,tr,par):
        """ Create a classifier, tr is a vector of training examples -
        list of lists of pairs of weights and features for all classes """
        self.par=bigtools.Parameters(par,default={'lmbd':1e-3, 'kernel_order':1,
                                                  'beta':1.})
        self.ncls=len(tr)
        #pdb.set_trace()
        self.fdim=tr[0][1].shape[1]
        self.tr=tr
        self.kdim=kernelize_size(self.fdim,self.par.kernel_order)

        def crit(av):
           """ av is a vector of length ncls*kdim that will be reshaped to a matrix
             of coefficients a[ncls,kdim]. 'crit' returns function
             value and a gradient """
           a=av.reshape(self.ncls,self.kdim)
           s,g=self._softmax_crit(a)
           s+=self.par.lmbd/(2.*self.ncls)*np.sum(a**2) # here we sum the results
           g+=a*(self.par.lmbd/self.ncls) # to sum the gradient
           # print "av=",a," s=",s, "g=",g
           return s,g.reshape((self.ncls*self.kdim,))
        a0=np.zeros((self.ncls*self.kdim,))
        print "Sotfmax classifier Cython training started"
        t=time.time()
        self.a,fopt,d=scipy.optimize.fmin_l_bfgs_b(crit,a0,m=10,factr=10.,
 pgtol=1e-6,iprint=0,maxfun=1000)
        print "SoftmaxClassifier training took ", time.time()-t, " d=",\
               d['warnflag'],' funcalls=', d['funcalls']
        self.a=self.a.reshape((self.ncls,self.kdim))

    def _softmax_crit(self,a):
           """ av is a matrix of coefficients a[ncls,kdim]. It returns the softmax
           criterion and a gradient """
           cdef DTYPEf_t[:,:] g=np.zeros(a.shape) # to sum the gradient
           cdef DTYPEf_t s=0.
           cdef DTYPEf_t sumw=0.
           cdef DTYPEf_t eps=sys.float_info.min
           cdef int order=self.par.kernel_order
           tr=self.tr
           cdef DTYPEw_t[:] w
           cdef DTYPEf_t[:,:] f
           cdef int i,k

           for k in xrange(self.ncls): # k is the class number
               w,f=tr[k] # weight and features
               n=f.shape[0]
               assert f.shape[1]==self.fdim
               for i in xrange(n): # for all training samples
                   # apply the kernel phi function
                   v=segregtools.kernelize(f[i],order)
                   e=np.nan_to_num(np.exp(np.dot(a,v)))
                   # denominator \sum \exp \phi(x_n)
                   den=np.sum(e)
                   # get probabilities
                   z=e/den
                   s-=w[i]*math.log(z[k]+eps)
                   sumw+=w[i]
                   z[k]-=1
                   g+=np.outer(z,v)*(w[i])
           return s/sumw ,np.asarray(g)/sumw

    def softmax_crit(self,a):
      J,g=self._softmax_crit(a)
      return J*self.par.beta, g*self.par.beta

    def evalprob(self,DTYPEf_t[:,:] f):
        """ Evaluate class probabilities for given feature vectors """
        #print "Evaluating probabilities"
        #t=time.time()
        cdef int n=f.shape[0]
        cdef DTYPEf_t[:,:] a=self.a
        cdef int ncls=a.shape[0]
        assert f.shape[1]==self.fdim
        cdef DTYPEf_t[:,:] z=np.zeros((n,self.ncls))
        cdef int i,j
        cdef DTYPEf_t den
        cdef DTYPEf_t[:] e=np.zeros((self.ncls,))
        cdef int kernel_order=self.par.kernel_order
        cdef DTYPEf_t[:] v=np.zeros((kernelize_size(
                          self.fdim,kernel_order),))
        cdef double maxexp=500.
        cdef double maxexpval=exp(maxexp)

        with nogil:
            for i in xrange(n):
                fast_kernelize(f[i],kernel_order,v)
                dotv(a,v,e)
                den=0.
                for j in xrange(ncls):
                  if e[j]>maxexp:
                    e[j]=maxexpval
                  else:
                    e[j]=exp(e[j])
                  den+=e[j]
                for j in xrange(ncls):
                    z[i,j]=e[j]/den  # get probabilities
        # print "Evaluating Cython probabilities took ", time.time()-t
        return z

    def hard_classify(self,f):
        """ Return the most likely class for each feature vector """
#        t=time.time()
        z=self.evalprob(f)
        l=np.argmax(z,1).astype(np.int32)
#        print "Classification (including calculating probabilities) took ", time.time()-t
        return l
