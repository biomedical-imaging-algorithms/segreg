% show the influence of points in 1D TPS
%
n=100 ; % number of points
nc=floor(n/2) ;
sigma=2 ; % standard deviation
%c=sparse(n,n) ; % covariance matrix
c=zeros(n,n) ; % interpolation matrix
for i=1:n,
  for j=1:n,
    d=abs(i-j) ;
        c(i,j)=d*d*log(abs(d/100.)+eps) ;
  end ;
end ;
ci=inv(c) ;
figure(1) ; 
imagesc(ci) ;
figure(2) ;
plot(ci(nc,:)) ;