cimport numpy as np

ctypedef np.float_t DTYPEf_t
ctypedef np.uint8_t DTYPE_t
ctypedef np.uint16_t DTYPEs_t
ctypedef np.int_t DTYPEw_t


cpdef inline int kernelize_size(int n,int order) nogil

cdef inline void fast_kernelize(DTYPEf_t[:] v,int order,DTYPEf_t[:] y) nogil

cpdef kernelize(DTYPEf_t[:] v,int order)
