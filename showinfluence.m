% show the influence of points in 1D krigging
%
n=100 ; % number of points
nc=floor(n/2) ;
sigma=2 ; % standard deviation
%c=sparse(n,n) ; % covariance matrix
c=zeros(n,n) ; % covariance matrix
for i=1:n,
  for j=1:n,
    d=i-j ;
    if (abs(d)<=10),
      c(i,j)=exp(-d*d/(2*sigma)) ;
    end
  end ;
end ;
ci=inv(c) ;
figure(1) ; 
imagesc(ci) ;
figure(2) ;
plot(ci(nc,:)) ;