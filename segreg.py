
#!/usr/bin/python
#
# Experiments with registering segmented images in Python
#
# Jan Kybic, October 2012


import matplotlib
# for non-interactive use
#matplotlib.use('Agg')
import matplotlib.pyplot as plt

import numpy as np
import scipy
import scipy.ndimage
import scipy.misc
import scipy.spatial
import scipy.interpolate
import scipy.optimize
import pylab
import time
import pdb
import beliefpropagation as bp
#import beliefpropagation_cython as bp
from PIL import Image
import ImageFilter
import meshpy.triangle as triangle
import matplotlib.pyplot as plt
import matplotlib.collections
import bigtools
import math
import multiprocessing

# to import Cython
#import pyximport
#pyximport.install(reload_support=True)
import segregtools

def dist(x,y):
    d=x-y
    return np.dot(d,d)

import thinplatesplines as tps


inf=float('inf')

#import pdb

def nearest_neighbors(x,etalons):
    netalons=etalons.shape[0]
    def nearest_neighbor(r):
        i=-1 ; v=inf ;
        for j in xrange(netalons):
            d=dist(r,etalons[j])
            if d<v:
                v=d ; i=j
        return i
    n=x.shape[0]
    s=np.zeros((n,1),np.int8)
    # pdb.set_trace()
    for i in xrange(n):
        s[i]=nearest_neighbor(x[i])
    return s

# %timeit s=segment(img)
# 1 loops, best of 3: 1.44 s per loop

def parmap(f,l,nproc=1):
  """ parallel map """
  if nproc==1:
    return map(f,l)
  n=len(l)
  res=[None]*n

  if nproc==-1:
    ncpu=multiprocessing.cpu_count()
  else:
    ncpu=nproc
  print "Starting parallel map with ncpu=",n
  prs=[]
  queue=multiprocessing.Queue()
  def pf(i,x,q):
    print "pf called with i=",i
    fx=f(x)
    print "pf with i=",i, "fx calculated"
    queue.put((i,fx))
    print "pf with i=",i, "queue updated"
  for i in xrange(n):
    x=l[i]
    pr=multiprocessing.Process(target=pf,args=(i,x,queue))
    pr.start()
    prs.append(pr)
  print "waiting for all processes to finish"
  for i in xrange(n):
    j,fx=queue.get()
    res[j]=fx
  for pr in prs:
    pr.join()
  print "all processes finished"
  return res


def segment(img):
    etalons=np.array(((0,255,0),(0,0,255),(255,0,0)))
    imgr=img.reshape(-1,3)
    # pylab.imshow(img)
    # do segment
    s=nearest_neighbors(imgr,etalons)
    return s.reshape(img.shape[:2])

#%timeit s=segment2(img)
#100 loops, best of 3: 19.6 ms per loop

def segment2(img):
    etalons=np.array(((0,255,0),(0,0,255),(255,0,0)),'uint8')
    imgr=img.reshape(-1,3)
    # pylab.imshow(img)
    # do segment
    s=segregtools.nearest_neighbors(imgr,etalons)
    return s.reshape(img.shape[:2])

def segment3(img):
    """ segment to four classes denoted by red, green, blue and yellow """
    etalons=np.array(((0,255,0),(0,0,255),(255,0,0),(255,255,0)),'uint8')
    imgr=img.reshape(-1,3)
    # pylab.imshow(img)
    # do segment
    s=segregtools.nearest_neighbors(imgr,etalons)
    return s.reshape(img.shape[:2])



def try_segmentation():
    img=scipy.misc.imread('imgs/simple.png')
    #s=segment(img)
    s=segment2(img)
    #timeit.timeit('s=segment2(img)',number=1)
    pylab.imshow(s)

def find_diffs(img):
    """ Returns a mask of all points which are on the boundary of two classes """
    sh=img.shape
    r=np.zeros(sh,dtype='int8')
    img=img.astype('int8')
    hd=abs(img[:,:-1]-img[:,1:])
    r[:,:-1]+=hd
    r[:,1:]+=hd
    vd=abs(img[:-1,:]-img[1:,:])
    r[:-1,:]+=vd
    r[1:,:]+=vd
    return r>0

def try_diffs():
    img=scipy.misc.imread('imgs/simple.png')
    s=segment2(img)
    # pdb.set_trace()
    m=find_diffs(s)
    s1=s.copy()
    #s1[m]=3
    pylab.clf()
    #pylab.imshow(s1)
    pylab.imshow(img)
    # pdb.set_trace()
    seeds=subsample_seeds(m,10) ;
    print "nseeds=",len(seeds)
    #ax=pylab.gca()
    #ax.set_ylim(ax.get_ylim()[::-1])
    pylab.plot(seeds[:,1],seeds[:,0],'or',markersize=5)
    #plt.axes.invert_yaxis()
    pylab.axis([0, img.shape[1],img.shape[0],0])

def try_reg():
    img=scipy.misc.imread('imgs/simple.png')
    s=segment2(img)
    # warp the image
    #def deff((x,y)):
    #    return x+10*sin(y/5),y+x/50
    #imgw=scipy.ndimage.interpolation.geometric_transform(img,deff,img.shape,img.dtype,order=1,mode='nearest')
    imgw=scipy.ndimage.rotate(img,10,reshape=False,order=1,mode='nearest')
    imgw=scipy.ndimage.shift(imgw,[15,0,0],order=1,mode='nearest')
    sw=segment2(imgw)
    m=find_diffs(s)
    pts=subsample_seeds(m,20) ;
    print "npts=",len(pts)
    pylab.clf()
    if False:
        pylab.subplot(2,1,1)
        pylab.imshow(img) ; pylab.title('original')
        pylab.plot(pts[:,1],pts[:,0],'ok',markersize=5)
        pylab.axis([0, img.shape[1],img.shape[0],0])
        pylab.subplot(2,1,2)
        pylab.imshow(imgw) ; pylab.title('warped')
    pylab.imshow(img) ; pylab.title('original')
    pylab.plot(pts[:,1],pts[:,0],'ok',markersize=5)
    pylab.axis([0, img.shape[1],img.shape[0],0])
    # pdb.set_trace()
    if False:
        t=tps.TpsRegression(pts)
        t.get_reduced_matrix() # no approximation, no random SVD
        Q=t.get_smoothness_matrix()
    else:
        t=tps.Tps(pts)
        m=t.get_matrix()
        Q=np.linalg.inv(m)[:-3,:-3]
    edgs,edgw=tps.get_edges(Q,err=1e-6,maxedg=int(2*pts.shape[0]))

    for e in edgs:
     ii=e[0] ; ij=e[1]
     pylab.plot((pts[ii][1],pts[ij][1]),(pts[ii][0],pts[ij][0]),'k-')
    pylab.axis([0, img.shape[1],img.shape[0],0])

def affine_fit(z,y):
      """ Given z [n x 2] and y [n x 2], find coefficients a=a0,a1,a2 so
      that z[i,0]*a[0,0]+z[i,1]*a[0,1]+a[0,2] and
           z[i,0]*a[1,0]+z[i,1]*a[1,1]+a[1,2]
      is as close as possible to y[i]
      in the least squares sense """
      n=z.shape[0]
      assert z.shape[1]==2 and y.shape==(n,2)
      psi=np.hstack((z,np.ones((n,1))))
      a=np.linalg.lstsq(psi,y)[0]
      return a.T

def affine_transf(a,z):
  """ Given the affine coefficient matrix a and points z, transform them """
  psi=np.hstack((z,np.ones((z.shape[0],1))))
  return np.dot(psi,a.T)

def test_affine():
  z=np.array(((1,1),(10,1),(10,10)))
  y=np.array(((2,1),(11,1),(11,10)))
  a=affine_fit(z,y)
  print affine_transf(a,z)


class Criterion:
    """ Similarity criterion at points """

    def __init__(self,f,g,h=10,d=10):
        """ Given two segmented images (uint8) f,g, set window size h
        and maximum displacement d """
        assert f.dtype==np.dtype('uint8')
        assert g.dtype==np.dtype('uint8')
        self.f=f ; self.g=g ; self.h=h ; self.d=d
        self.nd=2*d+1 # size of the output matrix
        """ Given two images, calculate the matrix of
        log(p_ij/(p_i*p_j) needed to calculate mutual information"""
        # FIXME: the number of classes could be different in both images
        # FIXME: f<>255 is specific for uint8
        #ncls=max(np.max(f),np.max(g[g<>255]))+1 ; self.ncls=ncls
        ncls=max(np.max(f[f<>255]),np.max(g[g<>255]))+1 ; self.ncls=ncls
        npix=f.shape[0]*f.shape[1]
        print "ncls=",ncls, " npix=",npix
        # get count matrix
        t=time.time()
        c=segregtools.class_counts(f,g,0,min(f.shape[0],g.shape[0]),0,min(f.shape[1],g.shape[1]),0,0,ncls)
        t=time.time()-t
        print "class_counts took ",t
        totcnt=float(np.sum(c))
        print "totcnt=",totcnt
        # calculate marginal probabilities
        eps=1e-8
        pf=(np.sum(c,axis=1)+eps)/totcnt
        pg=(np.sum(c,axis=0)+eps)/totcnt
        c=(c+eps)/totcnt
        self.m=np.log(c/np.outer(pf,pg))

    def crit_at_point(self,x,y):
        """ Find the value of the criterion at point (x,y), with previously defined
        window size (2h+1)**2 and maximum displacement d. """
        r=np.zeros((self.nd,self.nd)) # output array
        for dx in xrange(-self.d,self.d+1):
            for dy in xrange(-self.d,self.d+1):
                c=segregtools.class_counts(self.f,self.g,x-self.h,
                                           x+self.h,y-self.h,y+self.h,
                                           dx,dy,self.ncls)
                r[dx+self.d,dy+self.d]=np.tensordot(c,self.m)/np.sum(c)
        return r

class MultiresCriterion:
    """ Similarity criterion at points, using multiresolution """
    def __init__(self,f,g,h=10,d=2,l=4):
        """ Given two segmented images (uint8) f,g, set window size h
        maximum displacement d, and number of multiresolution levels l """
        assert f.dtype==np.dtype('uint8')
        assert g.dtype==np.dtype('uint8')
        self.f=f ; self.g=g ; self.h=h ; self.d=d ; self.l=l
        self.nd=2*d+1 # size of the output matrix
        # Given two images, calculate the matrix of
        # log(p_ij/(p_i*p_j) needed to calculate mutual information"""
        # FIXME: the number of classes could be different in both images
        # FIXME: 255 comes from -1, denoting out of range points,
        # it is specific for the uint8 type
        ncls=max(np.max(f[f<>255]),np.max(g[g<>255]))+1 ; self.ncls=ncls
        npix=f.shape[0]*f.shape[1]
        print "ncls=",ncls, " npix=",npix
        # get count matrix
        t=time.time()
        c=segregtools.class_counts(f,g,0,min(f.shape[0],g.shape[0]),0,min(f.shape[1],g.shape[1]),0,0,ncls)
        t=time.time()-t
        print "class_counts took ",t
        totcnt=float(np.sum(c))
        print "totcnt=",totcnt
        # calculate marginal probabilities
        eps=1e-8
        pf=(np.sum(c,axis=1)+eps)/totcnt
        pg=(np.sum(c,axis=0)+eps)/totcnt
        c=(c+eps)/totcnt
        self.m=np.log(c/np.outer(pf,pg))
        # calculate reduced images
        # FIXME: could be done only around the keypoints
        t=time.time()
        self.pyramidf=[ segregtools.segmentation_to_planes(f,ncls) ]
        self.pyramidg=[ segregtools.segmentation_to_planes(g,ncls) ]
        for i in xrange(l-1):
            self.pyramidf.append(segregtools.reduce_planes(self.pyramidf[-1]))
            self.pyramidg.append(segregtools.reduce_planes(self.pyramidg[-1]))
        t=time.time()-t
        print "creating pyramids took ",t

    def crit_at_point(self,x,y,d0,l):
        """ Find the value of the criterion at point (x,y) at level l
        (0 is original), with previously defined window size (2h+1)**2 and
        maximum displacement self.d. d0 is the displacement offset"""
        r=np.zeros((self.nd,self.nd)) # output array
        np.seterr(all='raise')
        for dx in xrange(-self.d,self.d+1):
            for dy in xrange(-self.d,self.d+1):
                c=segregtools.class_counts_from_planes(
                    self.pyramidf[l],self.pyramidg[l],x-self.h,
                                           x+self.h,y-self.h,y+self.h,
                                           dx+d0[0],dy+d0[1])
                sc=np.sum(c)
                if sc==0:
                    #pdb.set_trace()
                    #print "Point ",x,",",y," is outside of the image at level ",l
                    # in absence of all information, consider all classes equally
                    # likely
                    r[dx+self.d,dy+self.d]=np.mean(self.m)
                else:
                    r[dx+self.d,dy+self.d]=np.tensordot(c,self.m)/sc
        return r


def try_mi():
    img=scipy.misc.imread('imgs/simple.png')
    s=segment2(img)
    # warp the image
    imgw=scipy.ndimage.rotate(img,10,reshape=False,order=1,mode='nearest')
    imgw=scipy.ndimage.shift(imgw,[15,0,0],order=1,mode='nearest')
    sw=segment2(imgw)
    pylab.clf()
    pylab.figure(1)
    pylab.subplot(2,1,1)
    pylab.imshow(img) ; pylab.title('original')
    pylab.axis([0, img.shape[1],img.shape[0],0])
    pylab.subplot(2,1,2)
    pylab.imshow(imgw) ; pylab.title('warped')
    crit=Criterion(s,sw,h=10,d=30)
    t=time.time()
    x=90 ; y=96
    c=crit.crit_at_point(x,y)
    t=time.time()-t
    print "crit_at_point took ",t
    print "c=",c
    ind=np.argmax(c)
    ix,iy=np.unravel_index(ind,c.shape)
    print "optimum displacement=(",ix-crit.d,",",iy-crit.d,")"
    pylab.figure(1)
    pylab.subplot(2,1,1)
    pylab.plot(y,x,'ok',markersize=5)
    pylab.axis([0, img.shape[1],img.shape[0],0])
    pylab.subplot(2,1,2)
    pylab.plot(y+iy-crit.d,x+ix-crit.d,'ok',markersize=5)
    pylab.axis([0, img.shape[1],img.shape[0],0])
    pylab.figure(2)
    pylab.imshow(c) ; pylab.title('criterion')

def try_mi2():
    img=scipy.misc.imread('imgs/simple.png')
    s=segment2(img)
    # warp the image
    imgw=scipy.ndimage.rotate(img,10,reshape=False,order=1,mode='nearest')
    imgw=scipy.ndimage.shift(imgw,[15,0,0],order=1,mode='nearest')
    sw=segment2(imgw)
    pylab.clf()
    pylab.figure(1)
    pylab.subplot(2,1,1)
    pylab.imshow(img) ; pylab.title('original')
    pylab.axis([0, img.shape[1],img.shape[0],0])
    pylab.subplot(2,1,2)
    pylab.imshow(imgw) ; pylab.title('warped')
    # pdb.set_trace()
    crit=MultiresCriterion(s,sw,h=10,d=30,l=4)
    t=time.time()
    l=0 ;
    x=90 ; y=96
    c=crit.crit_at_point(x/(2**l),y/(2**l),l)
    t=time.time()-t
    print "crit_at_point took ",t
    print "c=",c
    ind=np.argmax(c)
    ix,iy=np.unravel_index(ind,c.shape)
    print "optimum displacement=(",ix-crit.d,",",iy-crit.d,")"
    pylab.figure(1)
    pylab.subplot(2,1,1)
    pylab.plot(y,x,'ok',markersize=5)
    pylab.axis([0, img.shape[1],img.shape[0],0])
    pylab.subplot(2,1,2)
    pylab.plot(y+(iy-crit.d)*2**l,x+(ix-crit.d)*2**l,'ok',markersize=5)
    pylab.axis([0, img.shape[1],img.shape[0],0])
    pylab.figure(2)
    pylab.imshow(c) ; pylab.title('criterion')



def subsample_seeds(m,d):
    """ Given a boolean mask m and distance d, subsample so that seeds
are not closer than d. Return the seed coordinates as a 2D array"""
    l=np.nonzero(m) # input list
    nx=m.shape[0]
    ny=m.shape[1]
    d2=d*d
    lout=[] # output list
    for i in xrange(l[0].shape[0]):
        x=l[0][i] ; y=l[1][i] ;
        if m[x][y]: # add this seed to the output list
            lout.append((x,y))
            # clear the neighborhood
            xmin=max(0,x-d) ; xmax=min(x+d+1,nx) ;
            ymin=max(0,y-d) ; ymax=min(y+d+1,ny) ;
            for ix in xrange(xmin,xmax):
                for iy in xrange(ymin,ymax):
                    dx=ix-x ; dy=iy-y ;
                    if dx*dx+dy*dy<d2:
                        m[ix][iy]=False
    return np.array(lout)

class SegReg:
    """ Class encapsulating registration of segmented images """

    def __init__(self,f,g,d=20,maxd=10,h=10,adim='full',use_random=False,
                 use_regression=False,alpha=1.0,l=3,par=None):
        """ fixed (f) and moving (g) images.
            d - minimum distance between keypoints
            maxd - maximum displacement of keypoints
            h - window size
            adim - rank of the system matrix approximation, can be 'full' or 'auto'
            use_regression - use regression thin plate splines (RTPS)
            use_random - use random projections to calculate SVD for (RTPS)
            alpha - regularization weight
            l - number of multiresolution levels
            """
        par=bigtools.Parameters(par,default={'abstol':1e-3,'reltol':1e-3})
        assert f.dtype==np.dtype('uint8')
        assert g.dtype==np.dtype('uint8')
        self.f=f
        self.g=g
        self.d=d
        self.maxd=maxd
        self.h=h
        self.adim=adim
        self.use_random=use_random
        self.use_regression=use_regression
        self.graph=bp.BPGraph()
        self.alpha=alpha
        self.l=l

    def show_segmentations(self):
        pylab.figure(1) ; pylab.clf()
        pylab.imshow(self.f) ; pylab.title('fixed image')
        pylab.figure(2) ; pylab.clf()
        pylab.imshow(self.g) ; pylab.title('moving image')
        pylab.show(block=False)

    def find_keypoints(self,d=None):
        """ Find key points on edges and with minimum distance self.d pixels """
        if d is None:
            d=self.d
        t=time.time()
        m=find_diffs(self.f)
        t=time.time()-t
        print "find_diffs took ",t
        t=time.time()
        self.pts=subsample_seeds(m,d) ;
        t=time.time()-t
        print "subsample_seeds took ",t
        self.npts=len(self.pts) # number of all points
        self.ncritpts=self.npts # number of points with a criterion
        print "npts=",self.npts


    def show_segmentations_keypoints(self,figureofs=3,movedkpts=None,title=True,outprefix=None,suffix='before'):
        """ Show both images and fixed point keypoints """
        #pdb.set_trace()
        pylab.figure(figureofs) ; pylab.clf()
        pylab.imshow(self.f) ;
        if title: pylab.title('fixed image')
        pylab.plot(self.pts[:,1],self.pts[:,0],'ok',markersize=5)
        savefigure(outprefix+'fixed_keypoints_'+suffix+'.png')

        if outprefix is None:
            pylab.figure(figureofs+1) ;
        pylab.clf()
        pylab.imshow(self.g) ;
        if title: pylab.title('moving image')
        if movedkpts is None:
            movedkpts=self.pts
        pylab.plot(movedkpts[:,1],movedkpts[:,0],'ok',markersize=5)
        savefigure(outprefix+'moving_keypoints_'+suffix+'.png')

    def find_criteria(self,maxd=None,h=None):
        """ find criteria values for all keypoints up to maximum
        displacement maxd """
        if maxd is None:
            maxd=self.maxd
        if h is None:
            h=self.h
        t=time.time()
        crit=Criterion(self.f,self.g,h=h,d=maxd)
        self.graph.D=[]
        for i in xrange(self.ncritpts):
            p=self.pts[i]
            # mutual information is to be maximized, hence -
            c=-crit.crit_at_point(p[0],p[1])
            self.graph.D.append(c)
        t=time.time()-t
        print "find_criteria took ",t

    def find_criteria_level_seq(self,crit,ptsf,ptsg,l):
        """ find criteria values for all keypoints pts at level l using
        MultiresCriterion crit, ptsf are
        coordinates in the fixed image at this level. ptsg are initial
        coordinates in the moving image at this level """
        t=time.time()
        self.graph.D=[]
        for i in xrange(self.npts):
          if i<self.ncritpts:
            p=ptsf[i]
            # mutual information is to be maximized, hence -
            c=-crit.crit_at_point(p[0],p[1],ptsg[i]-p,l)
            self.graph.D.append(c)
          else:
            self.graph.D.append(None)
        t=time.time()-t
        print "find_criteria_level took ",t

    def find_criteria_level(self,crit,ptsf,ptsg,l,nproc=1):
        """ find criteria values for all keypoints pts at level l using
        MultiresCriterion crit, ptsf are
        coordinates in the fixed image at this level. ptsg are initial
        coordinates in the moving image at this level """
        if nproc==1:
          return self.find_criteria_level_seq(crit,ptsf,ptsg,l)

        t=time.time()
        self.graph.D=self.npts*[None]
        if self.par.nproc==-1:
          ncpu=multiprocessing.cpu_count()
        else:
          ncpu=nproc
        chunksize=int(math.ceil(float(self.ncritpts)/ncpu))
        queue=multiprocessing.Queue()
        def do_some(i0,i1):
          #print "do_some  ", i0,i1,self.ncritpts
          r=[]
          for i in xrange(i0,i1):
            p=ptsf[i]
            # mutual information is to be maximized, hence -
            c=-crit.crit_at_point(p[0],p[1],ptsg[i]-p,l)
            r.append(c)
          queue.put((i0,i1,r))
        prs=[]
        for i in xrange(0,self.ncritpts,chunksize):
          pr=multiprocessing.Process(target=do_some,
            args=(i,min(i+chunksize,self.ncritpts)))
          pr.start()
          prs.append(pr)
        for i in xrange(len(prs)):
          q0,q1,m=queue.get()
          self.graph.D[q0:q1]=m
        for pr in prs:
          pr.join()

        t=time.time()-t
        #pdb.set_trace()
        print "find_criteria_level took ",t



    def find_edges(self,use_regression=None,use_random=None,adim=None,
                   show=False,outprefix=None):
        tm=time.time()
        if use_regression is None:
            use_regression=self.use_regression
        if use_random is None:
            use_random=self.use_random
        if adim is None:
            adim=self.adim
        if use_regression:
            # faster but worse quality
            self.t=tps.TpsRegression(self.pts)
            self.t.get_reduced_matrix(adim=adim,use_random=use_random)
            Q=self.t.get_smoothness_matrix()
        else:
            # no approximation, slower
            self.t=tps.Tps(self.pts)
            m=self.t.get_matrix()
            Q=np.linalg.inv(m)[:-3,:-3]
        edgs,edgw=tps.get_edges(Q,err=1e-6,maxedg=int(2*self.pts.shape[0]))
        tm=time.time()-tm
        print "find_edges get_edges took ",tm
        # convert to BPGraph structure
        tm=time.time()
        self.graph.edges=[[] for i in xrange(self.npts)]
        self.graph.weights=[[] for i in xrange(self.npts)]
        for e,w in zip(edgs,edgw):
            ii=e[0] ; ij=e[1]
            assert ii<>ij
            self.graph.edges[ii].append(ij)
            self.graph.edges[ij].append(ii)
            self.graph.weights[ii].append(w*self.alpha)
            self.graph.weights[ij].append(w*self.alpha)
        tm=time.time()-tm
        #pdb.set_trace()
        print "find_edges converting to BPGraph took ",tm
        if show:
            pylab.clf() ; self.show_edges(outprefix=outprefix)

    def show_edges(self,outprefix=None):
            pylab.clf()
            pylab.plot(self.pts[:,1],self.pts[:,0],'or',markersize=5)
            lines=[]
            for ii in xrange(len(self.graph.edges)):
                for ij in self.graph.edges[ii]:
                  lines.append((self.pts[ii,::-1],self.pts[ij,::-1]))
                  #                  pylab.plot((self.pts[ii][1],self.pts[ij][1]),(self.pts[ii][0],self.pts[ij][0]),'k-')
            lc=matplotlib.collections.LineCollection(lines,colors='k')
            pylab.axes().add_collection(lc)
            pylab.axis([0, self.f.shape[1],self.f.shape[0],0])
            pylab.axes().set_aspect('equal')
            plt.show(block=False)
            if outprefix is not None:
                savefigure(outprefix+'edges.pdf')

    def find_displacement(self):
        """ Use BP to find displacement of all keypoints """
        tm=time.time()
        energy,displ=self.graph.solve(abstol=self.par.abstol,reltol=self.par.reltol)
        tm=time.time()-tm
        print "belief propagation took ",tm
        self.newpts=np.asarray(map(lambda (dx,dy),(x,y): (x+dx,y+dy),displ,self.pts))

    def find_displacement_level(self,ptsf,ptsg):
        """ Use BP to find displacement of all keypoints. ptsf are
        coordinates in the fixed image at this level. ptsg are initial
        coordinates in the moving image at this level"""
        tm=time.time()
        energy,displ=self.graph.solve(offsets=ptsg-ptsf,abstol=self.par.abstol,reltol=self.par.reltol)
        tm=time.time()-tm
        print "belief propagation took ",tm
        self.newpts=np.asarray(map(lambda (dx,dy),(x,y): (x+dx,y+dy),displ,ptsg))
        return energy

    def find_displacement_level_affine(self,ptsf,ptsg):
      """ Find an affine displacement of all keypoints,
          using ptsg as an initial guess. """
      t=time.time()
      #a0=affine_fit(ptsf,ptsg).ravel() # initial parameter
      shift=(self.graph.D[0].shape[0]-1)/2
      y=np.zeros((self.ncritpts,2))
      for i in xrange(self.ncritpts):
        d=self.graph.D[i]
        ind=np.argmin(d)
        y[i]=np.array(np.unravel_index(ind,d.shape))-shift+ptsg[i]
      aopt=affine_fit(ptsf[:self.ncritpts],y).ravel()
      energy=0.  

      # def crit(a):
      #   """ calculate the criterion for a given coefficient a """
      #   dxy=affine_transf(a.reshape((2,3)),ptsf[:self.ncritpts])-ptsg[:self.ncritpts]
      #   s=0
      #   for i in xrange(self.ncritpts):
      #     d=self.graph.D[i]
      #     s+=scipy.ndimage.interpolation.map_coordinates(d,
      #             dxy[i].reshape((2,1))+shift,
      #             order=1,mode='constant',cval=np.max(d))
      #   # to encourage solutions close to ptsg
      #   #s=s+1e-6*abs(s)*scipy.linalg.norm(dxy)/self.ncritpts
      #   return s
      # aopt,energy,iter,funcalls,warnflags=scipy.optimize.fmin(crit,a0,maxiter=1000,full_output=True,disp=True)
      # #aopt,energy,iter,funcalls,warnflags=scipy.optimize.fmin_powell(crit,a0,maxiter=100,xtol=0.5,full_output=True,disp=False)
      self.newpts=np.round(affine_transf(aopt.reshape((2,3)),ptsf)).astype('i2')
      # #pdb.set_trace()
      # print "find_displacement_level_affine took ", time.time()-t, " iter=", iter, " funcalls=", funcalls
      return energy

    def warp_image(self,img):
        """ warp image img according to the displacement found"""
        tm=time.time()
        # self.t.
        #xf=self.t.get_interpolator(self.newpts[:,0])
        #yf=self.t.get_interpolator(self.newpts[:,1])
        #def mapfun(xy):
        #    return (xf(xy),yf(xy))
            # return (xy[0]-1,xy[1])
        #def warp(i):
        #    return scipy.ndimage.geometric_transform(i,mapfun,
        #            output_shape=self.f.shape,order=1)
        xc=self.t.get_coefs(self.newpts[:,0])
        yc=self.t.get_coefs(self.newpts[:,1])
        #pdb.set_trace()
        def warp(i):
            return segregtools.tps_interpolate_image(xc,yc,np.asarray(self.pts,'float'),
                                                 self.f.shape[0],self.f.shape[1],i)

        #pdb.set_trace()
        if len(img.shape)==2: # bw images
            wg=warp(img)
        elif len(img.shape)==3 and img.shape[2]==3: #rgb images
            imgt=np.transpose(img,(2,0,1))
            wg=np.zeros((3,self.f.shape[0],self.f.shape[1]),dtype=imgt.dtype)
            for i in xrange(3):
                wg[i]=warp(imgt[i])
            wg=np.transpose(wg,(1,2,0))
        else:
            raise ValueError('warp_image: Incorrect image dimensionality')
        tm=time.time()-tm
        print "warping took ",tm
        return wg

    def warp_image_slow(self,img):
        """ warp image g according to the displacement found"""
        tm=time.time()
        xf=self.t.get_interpolator(self.newpts[:,0])
        yf=self.t.get_interpolator(self.newpts[:,1])
        def mapfun(xy):
            return (xf(xy),yf(xy))
        def warp(i):
            return scipy.ndimage.geometric_transform(i,mapfun,
                    output_shape=self.f.shape,order=1)
        if len(img.shape)==2: # bw images
            wg=warp(i)
        elif len(img.shape)==3 and img.shape[2]==3: #rgb images
            imgt=np.transpose(img,(2,0,1))
            wg=np.zeros(imgt.shape,dtype=imgt.dtype)
            for i in xrange(3):
                wg[i]=warp(imgt[i])
            wg=np.transpose(wg,(1,2,0))
        else:
            raise ValueError('warp_image: Incorrect image dimensionality')
        tm=time.time()-tm
        print "warping took ",tm
        return wg


    def _register(self):
        #show_segmentations()
        self.find_keypoints()
        self.show_segmentations_keypoints()
        self.find_edges(show=True)
        self.find_criteria()
        #pdb.set_trace()
        self.find_displacement()
        self.show_segmentations_keypoints(figureofs=6,movedkpts=self.newpts)

    def register(self):
      return self._register()

    def register_multires(self,ptsg=None,outprefix=None,do_affine=False):
        return self._register_multires(ptsg=ptsg,outprefix=outprefix,
                                       do_affine=do_affine)


    def _register_multires(self,ptsg=None,outprefix=None,cols=None,nproc=1,show_edges=False,do_affine=False):
        """ ptsg is the initial keypoint position in the moving image """
        tt=time.time()
        self.find_keypoints()
        if outprefix is not None:
            self.show_segmentations_keypoints(figureofs=1,title=False,outprefix=outprefix,cols=cols)
            #self.find_edges(show=False)
            # pdb.set_trace()
        self.find_edges(show=show_edges,outprefix=outprefix)
        crit=MultiresCriterion(self.f,self.g,h=self.h,d=self.maxd,l=self.l)
        # store original edge weights
        weights=list(self.graph.weights)
        # initial point location
        if ptsg is None:
          ptsg=self.pts
        ptsg=ptsg/(2**(self.l-1))
        for l in xrange(self.l-1,-1,-1): # count l-1,l-2,...,1,0
            print "Registration at level ",l, " started. size:", crit.pyramidf[l].shape[:2], " factor: ", crit.pyramidf[0].shape[0]/crit.pyramidf[l].shape[0]
            t=time.time()
            ptsf=self.pts/(2**l) # positions in the fixed image
            #print "ptsg=",ptsf[2], "ptsg=",ptsg[2]
            self.find_criteria_level(crit,ptsf,ptsg,l,nproc=nproc)
            # scale weights. Because the pixel size changes
            wc=4**l
            #wc=1
            self.graph.weights=map(lambda x: x*wc,weights)
            # pdb.set_trace()
            if do_affine: # first level
              self.find_displacement_level_affine(ptsf,ptsg)
            else:
              self.find_displacement_level(ptsf,ptsg)
            #print "newpts=",self.newpts[2]
            # the displacement is in self.newpts, show it
            #pylab.clf()
            #pyr=crit.pyramidg[l]
            #gimg=(pyr[:,:,0]*1+pyr[:,:,1]*2+pyr[:,:,2]*3+pyr[:,:,3]*4)/(pyr[:,:,0]+pyr[:,:,1]+pyr[:,:,2]+pyr[:,:,3])
            #pylab.imshow(gimg,interpolation='nearest') ; pylab.title('moving pyramid')
            #pylab.plot(ptsg[:,1],ptsg[:,0],'or',markersize=5)
            #pylab.plot(self.newpts[:,1],self.newpts[:,0],'ok',markersize=5)
            #for i in xrange(len(self.pts)):
            #    pylab.plot((ptsg[i,1],self.newpts[i,1]),(ptsg[i,0],self.newpts[i,0]),'or-')
            #pdb.set_trace()
            # create new initial estimate for the next level
            t=time.time()-t
            print "Registration at level ", l, " took ", t
            if l>0:
                ptsg=self.newpts*2
        tt=time.time()-tt
        print "Registration_multires took ", tt
        if outprefix is not None:
            self.show_segmentations_keypoints(figureofs=1,movedkpts=self.newpts,title=False,outprefix=outprefix,suffix='after',cols=cols)

        # self.show_segmentations_keypoints(figureofs=6,movedkpts=self.newpts)

##################################################################

class LandmarksTransform:
  """ image and label geometrical transform based on landmarks.
      TODO: could be factorized into two - a generic transform and
      one specific for landmarks
  """

  def __init__(self,size,par=None):
    """ par.warpinterporder is 1 or 2 """
    self.par=bigtools.Parameters(par,default={"warpinterporder":1})
    self.size=size

  def set_from_landmarks(self,pts,newpts):
    """ Creates a transformation for an image of size [size] that
    transforms point coordinates [pts] (in the fixed image) to [newpts]
    (in the transformed image, i.e. the point newpts in the moving image is moved
    to point [pts] in the output image. """
    tm=time.time()
    coords=np.mgrid[0:self.size[0],0:self.size[1]]
    coords=np.transpose(coords,(1,2,0)).reshape((-1,2))
    if self.par.warpinterporder==1:
      mapf=scipy.interpolate.LinearNDInterpolator(pts,newpts)
    elif self.par.warpinterporder==2:
      mapf=scipy.interpolate.CloughTocher2DInterpolator(pts,newpts,tol=1e-3)
    else:
      raise ValueError("LandmarsTransform:par.warpinterporder=%d not supported"
                       % self.par.warpinterporder)
    newcoords=mapf(coords).reshape((self.size+(2,)))
    self.newcoords=np.transpose(newcoords,(2,0,1))
    print "coordinate array created in ", time.time()-tm


  def warp_image(self,img):
    """ warp image img according to the displacement found """
    return self._warp_image(img,order=1)

  def warp_labels(self,img):
    """ warp an integer label field according to the displacement found, i.e. use nearest neighbors interpolation """
    return self._warp_image(img,order=0,cval=-1)

  def _warp_image(self,img,order=0,mode='constant',cval=0):
    """ apply the warping """
    tm=time.time()
    def warp(i):
      return scipy.ndimage.interpolation.map_coordinates(i,self.newcoords,
                    order=order,mode=mode,cval=cval)
    if len(img.shape)==2: # bw images
      wg=warp(img)
    elif len(img.shape)==3 and img.shape[2]==3: #rgb images
      imgt=np.transpose(img,(2,0,1))
      wg=np.zeros((3,self.size[0],self.size[1]),dtype=imgt.dtype)
      for i in xrange(3):
        wg[i]=warp(imgt[i])
      wg=np.transpose(wg,(1,2,0))
    else:
        raise ValueError('warp_image: Incorrect image dimensionality')
    print "warping took ",time.time()-tm
    return wg

  def compose(self,other):
    """ to this transformation, add another (to be applied afterwards) and return a new object composing the two. If other is None, return self """
    if other is None:
      return self
    assert other.size==self.size
    new=LandmarksTransform(self.size, par=self.par)
    def warp(i):
      return scipy.ndimage.interpolation.map_coordinates(i,other.newcoords,
                    order=1,mode='constant',cval=0)
    new.newcoords=np.zeros(self.newcoords.shape)
    for j in xrange(2): # for both x and y
      new.newcoords[j]=warp(self.newcoords[j])
    return new

  def saveimg(self,fn):
    """ show transformation as an image """
    sz=self.newcoords.shape[1:]
    h=20 # square size
    x=(-1)**(np.arange(sz[0])/h)
    y=(-1)**(np.arange(sz[1])/h)
    img=((np.outer(x,y)+1)/2).astype(np.uint8)
    w=self.warp_image(img)*255
    plt.imshow(w) ; plt.show(block=False)
    scipy.misc.imsave(fn,w)



##################################################################

class SegRegTri(SegReg):
  """ modification of SegReg for meshes created by triangulation
      the point positions are to be set by set_pts() set_shift()
  """

  def __init__(self,f,g,par):
    """ f,g - fixed and moving images
        par.wsize - window size
        par.maxdispl - maximum displacement
        par.alpha - regularization
        par.mlevels - number of multiresolution levels """
    # assert f.dtype==np.dtype('uint8')
    # assert g.dtype==np.dtype('uint8')
    assert np.max(f)<=255 and np.min(f)>=0
    assert np.max(g)>=0   and np.max(g)>=0

    self.f=f.astype('uint8')
    self.g=g.astype('uint8')
    self.par=bigtools.Parameters(par,default={'wsize':10,'maxdispl':10,
                                              'alpha':1.0,'l':3,
                                              'mindist':5,'nproc':1,
                                              'abstol':1e-3,'reltol':1e-3,
                                              'show_edges':False})

    self.h=self.par.wsize
    self.alpha=self.par.alpha
    self.l=self.par.mlevels
    self.pts=None
    self.newpts=None
    self.graph=bp.BPGraph()
    # by default, shift so that the centers coincide
    self.shift=0.5*(np.array(self.g.shape)-np.array(self.f.shape))
    # same colors as in simseg.py
    self.cols=np.array( (((0,0,0),(0,0,255),(255,0,0),(0,255,0),# black,blue,red,green
                              (255,255,0),(0,255,255),(255,0,255))), # yellow, cyan, magenta
                      dtype='u1')
    self.maxd=self.par.maxdispl


  def find_keypoints(self):
    """ find_keypoints does not do anything, call set_pts instead """
    pass

  def set_pts(self,ipts):
    """ Set landmark positions in image f. We eliminate points closer than
    self.par.wsize"""
    assert ipts.shape[1]==2
    print "set_pts started with ", ipts.shape[0], " points"
    t=time.time()
    tree=scipy.spatial.KDTree(ipts) # default leafsize
    ptsset=set(xrange(ipts.shape[0])) # set of all points
    closepts=tree.query_ball_tree(tree,self.par.mindist,eps=0.1)
    print "KD tree search took ", time.time()-t
    t=time.time()
    for i in xrange(ipts.shape[0]):
      if i in ptsset: # point i not yet eliminated
        for j in closepts[i]: # points to be eliminated
          if j>i:
            ptsset.discard(j)
    self.ipts=ipts[list(ptsset)]
    print "Eliminating points took ", time.time()-t, " yielding ",\
       self.ipts.shape[0], " points"
    self.npts=self.ipts.shape[0]
    self.ncritpts=self.npts
    # self.ptsg=ptsf+self.shift # uses broadcasting
    self.pts=self.ipts.copy()
    self.apply_shift()

  def set_shift(self,v):
    """ Set the shift to apply to landmark positions in image g """
    assert np.all(v.shape==(2,))
    self.shift=v
    self.apply_shift()

  def apply_shift(self):
    self.newpts=self.pts+self.shift # uses broadcasting

  def register_multires(self,ptsg=None,outprefix=None,cols=None,do_affine=False):
    # super(SegReg,self).register_multires(ptsg=ptsg,outprefix=outprefix)
    self._register_multires(ptsg=ptsg,outprefix=outprefix,cols=cols,
                            nproc=self.par.nproc,show_edges=self.par.show_edges,do_affine=do_affine)
    self.set_transform()

  def register(self):
    #super(SegReg,self).register()
    self._register()
    self.set_transform()


  def find_edges(self,show=True,outprefix=None):
    """ Given points in the first images, find edges. Some points are also added, i.e. ptsf is changed. shift should be set and is used to calculate ptsg First self.ncritpts are used in criteria evaluation. """
    assert self.pts is not None
    s=self.f.shape
    time0=time.time()

    # we need points in the corners
    points= np.array(( (0,0), (s[0]-1,0), (s[0]-1,s[1]-1), (0,s[1]-1)))
    def round_trip_connect(start, end):
      result = []
      for i in range(start, end):
        result.append((i, i+1))
      result.append((end, start))
      return result

    points=np.vstack((self.ipts,points))
    # find a triangulation, possibly adding points
    t=time.time()
    info=triangle.MeshInfo()
    info.set_points(points.astype(np.float))
    info.set_facets(round_trip_connect(self.ipts.shape[0],self.ipts.shape[0]+3))
    mesh = triangle.build(info, min_angle=15)
    print "Triangulation took ",time.time()-t
    t0=time.time()
    self.pts=np.array(mesh.points).astype(np.int)
    self.npts=self.pts.shape[0]
    # self.ptsg=self.ptsf+self.shift
    self.apply_shift() # set newpts

    # convert triangles to edges
    h={}
    def add_edge(i,j,w):
      if i<j:
        ij=j,i
      else:
        ij=i,j
      # print "Adding edge ",i,j,w
      if h.has_key(ij):
        h[ij]+=w
      else:
        h[ij]=w


    for t in np.array(mesh.elements):
      # triangle vertex coordinates
      x=np.array([ self.pts[i] for i in t ]).astype(float)
      # calculate triangle area
      lv=[ x[2]-x[1], x[2]-x[0], x[1]-x[0] ]
      area=0.5*abs(lv[1][0]*lv[2][1]-lv[1][1]*lv[2][0])
      # edge lengths
      l=[ math.sqrt(np.dot(v,v)) for v in lv ]
      epsilon=1e-6
      def process_triangle(t0,t1,t2,l0,l1,l2):
        """ Calculate the stiffness of the spring 0 (connecting vertices 1,2)
        Using the formula of H.Delingette        """
        sina=2.*area/(l1*l2)
        if sina>=1.:
          cosa=0.
        else:
          cosa=math.sqrt(1-sina*sina)
        cot2a=(cosa/(sina+epsilon))**2
        w=l0**2/(8*(area+epsilon))*(3*cot2a+0.5)*self.alpha
        add_edge(t1,t2,w)
      process_triangle(t[0],t[1],t[2],l[0],l[1],l[2])
      process_triangle(t[1],t[2],t[0],l[1],l[2],l[0])
      process_triangle(t[2],t[0],t[1],l[2],l[0],l[1])

    # convert to BPGraph structure
    self.graph.edges=[[] for i in xrange(self.npts)]
    self.graph.weights=[[] for i in xrange(self.npts)]
    for ij in h:
      i,j=ij
      w=h[ij]
      self.graph.edges[i].append(j)
      self.graph.edges[j].append(i)
      self.graph.weights[i].append(w)
      self.graph.weights[j].append(w)

    print "Converting edges and finding weights ",time.time()-t0, " ntriangles=", np.array(mesh.elements).shape[0], " npts=", self.npts, " ncritpts=", self.ncritpts
    print "find_edges took ",time.time()-time0
    if show:
      pylab.clf() ; self.show_edges(outprefix=outprefix)

  def show_edges(self,outprefix=None):
    print "show_edges started"
    #pdb.set_trace()
    pylab.clf()
    pylab.imshow(self.cols[self.f+1])
    pylab.axis([0, self.f.shape[1],self.f.shape[0],0])
    pylab.plot(self.pts[:self.ncritpts,1],self.pts[:self.ncritpts,0],'or',markersize=10)
    pylab.plot(self.pts[self.ncritpts:,1],self.pts[self.ncritpts:,0],'or',
               markersize=1)
    lines=[]
    for ii in xrange(len(self.graph.edges)):
      for ij in self.graph.edges[ii]:
        lines.append((self.pts[ii,::-1],self.pts[ij,::-1]))

        #        pylab.plot((self.pts[ii][1],self.pts[ij][1]),(self.pts[ii][0],self.pts[ij][0]),'k-')
    lc=matplotlib.collections.LineCollection(lines,colors='k')
    pylab.axes().add_collection(lc)
    pylab.axis([0, self.f.shape[1],self.f.shape[0],0])
    pylab.axes().set_aspect('equal')
    if outprefix is not None:
      savefigure(outprefix+'edges.png')
    print "show_edges finished"

  def set_transform(self):
    """ Setup the geometrical transform. It is called automatically from "register"
    """
    self.transform=LandmarksTransform(self.f.shape,self.par)
    self.transform.set_from_landmarks(self.pts,self.newpts)

  def get_transform(self):
    """ get transform object """
    assert self.transform is not None
    return self.transform

  def warp_image(self,img):
    """ warp image img according to the displacement found """
    return self.transform.warp_image(img)

  def warp_labels(self,img):
    """ warp an integer label field according to the displacement found, i.e. use nearest neighbors interpolation """
    return self.transform.warp_labels(img)


  def show_overlaid_segmentations(self):
    gw=self.warp_labels(self.g)
    pylab.imshow(self.cols[self.f+1]/2+self.cols[gw+1]/2)
    #pdb.set_trace()

  def show_segmentations_keypoints(self,figureofs=3,movedkpts=None,title=True,outprefix=None,suffix='before',cols=None):
        """ Show both images and fixed point keypoints """
        #pdb.set_trace()
        pylab.figure(figureofs) ; pylab.clf()
        if cols is None:
          pylab.imshow(self.f)
        else:
          pylab.imshow(cols[self.f+1])
        if title: pylab.title('fixed image')
        pylab.plot(self.pts[:self.ncritpts,1],self.pts[:self.ncritpts,0],'ok',
                   markersize=5)
        savefigure(outprefix+'fixed_keypoints_'+suffix+'.png')

        if outprefix is None:
            pylab.figure(figureofs+1) ;
        pylab.clf()
        if cols is None:
          pylab.imshow(self.g) ;
        else:
          pylab.imshow(cols[self.g+1]) ;
        if title: pylab.title('moving image')
        if movedkpts is None:
            movedkpts=self.pts[:self.ncritpts]
        pylab.plot(movedkpts[:self.ncritpts,1],movedkpts[:self.ncritpts,0],
                   'ok',markersize=5)
        savefigure(outprefix+'moving_keypoints_'+suffix+'.png')


##################################################################


def show_combined_images(img1,img2):
    """ Given two RGB images, show their linear combination for visualization """
    if img1.shape<>img2.shape:
        nsize=np.max(np.vstack((img1.shape,img2.shape)),0)
        shift=[ (nsize-i.shape)/2 for i in [img1,img2] ]
        img1w=np.zeros(nsize,dtype=img1.dtype)
        img2w=np.zeros(nsize,dtype=img1.dtype)
        img1w[shift[0][0]:shift[0][0]+img1.shape[0],shift[0][1]:shift[0][1]+img1.shape[1]]=img1
        img2w[shift[1][0]:shift[1][0]+img2.shape[0],shift[1][1]:shift[1][1]+img2.shape[1]]=img2
        img1=img1w ; img2=img2w
    pylab.imshow(img1/2+img2/2)


##################################################################



def try_warp():
    img=scipy.misc.imread('imgs/simple.png')
    pts=np.array(((50,50),(50,270),(150,50),(150,270)))
    newpts=np.array(((30,50),(50,270),(150,50),(150,270)))
    t=tps.Tps(pts)
    xf=t.get_interpolator(newpts[:,0])
    yf=t.get_interpolator(newpts[:,1])
    #pdb.set_trace()
    def mapfun(xy):
        return (xf(xy),yf(xy))
        #return (xy[0]-10,xy[1])
    def warp(i):
        return scipy.ndimage.geometric_transform(i,mapfun,
           output_shape=img.shape[:2],order=1)
    imgt=np.transpose(img,(2,0,1))
    ri=imgt[0]
    pylab.figure(1) ; pylab.clf() ; pylab.imshow(ri) ; pylab.title('red before')
    riw=warp(ri)
    pylab.figure(2) ; pylab.clf() ; pylab.imshow(riw) ; pylab.title('red after')

def try_warp2():
    """ Try faster warping using segregtools.tps_interpolate_image """
    img=scipy.misc.imread('imgs/simple.png')
    pts=np.array(((50,50),(50,270),(150,50),(150,270)))
    newpts=np.array(((30,50),(50,270),(150,50),(150,270)))
    t=tps.Tps(pts)
    xc=t.get_coefs(newpts[:,0])
    yc=t.get_coefs(newpts[:,1])
    #pdb.set_trace()
    def warp(i):
        return segregtools.tps_interpolate_image(xc,yc,np.asarray(pts,'float'),
                                                 img.shape[0],img.shape[1],i)
    imgt=np.transpose(img,(2,0,1))
    ri=imgt[0]
    pylab.figure(1) ; pylab.clf() ; pylab.imshow(ri) ; pylab.title('red before')
    riw=warp(ri)
    pylab.figure(2) ; pylab.clf() ; pylab.imshow(riw) ; pylab.title('red after')



def try_register():
    img=scipy.misc.imread('imgs/simple.png')
    s=segment2(img)
    # warp the image
    imgw=scipy.ndimage.rotate(img,2,reshape=False,order=1,mode='nearest')
    imgw=scipy.ndimage.shift(imgw,[2,0,0],order=1,mode='nearest')
    sw=segment2(imgw)
    reg=SegReg(s,sw,alpha=0.1)
    reg.register()
    imgr=reg.warp_image(imgw)
    #imgr=reg.warp_image_slow(imgw)
    pylab.figure(8) ; pylab.clf() ; pylab.imshow(imgr) ;
    pdb.set_trace()

def try_register_multires():
    img=scipy.misc.imread('imgs/simple.png')
    s=segment2(img)
    # warp the image
    imgw=scipy.ndimage.shift(img,[10,0,0],order=1,mode='nearest')
    imgw=scipy.ndimage.rotate(imgw,10,reshape=False,order=1,mode='nearest')
    sw=segment2(imgw)
    reg=SegReg(s,sw,alpha=1,maxd=3,l=4)
    reg.register_multires()
    imgr=reg.warp_image(imgw)
    #imgr=reg.warp_image_slow(imgw)
    pylab.figure(8) ; pylab.clf() ; pylab.imshow(imgr) ;
    pylab.figure(9) ; pylab.clf() ; pylab.imshow(imgr-img) ;
    pdb.set_trace()

def savefigure(filename):
    pylab.axis('image') ; pylab.axis('off') ;
    pylab.gca().get_xaxis().set_visible(False)
    pylab.gca().get_yaxis().set_visible(False)
    pylab.gcf().set_size_inches((10,15))
    pylab.savefig(filename,bbox_inches='tight',pad_inches=0,dpi=100)

def article_exp_multires():
    pylab.close()
    img=scipy.misc.imread('imgs/simple.png')
    s=segment2(img)
    # warp the image
    imgw=scipy.ndimage.shift(img,[10,0,0],order=1,mode='nearest')
    imgw=scipy.ndimage.rotate(imgw,10,reshape=False,order=1,mode='nearest')
    sw=segment2(imgw)
    pylab.close('all')
    pylab.figure(1)
    pylab.imshow(imgw,interpolation='nearest',aspect='equal')
    savefigure('output/simple_moving.png')
    reg=SegReg(s,sw,alpha=1,maxd=3,l=4)
    # reg.find_edges()
    reg.register_multires()
    imgr=reg.warp_image(imgw)
    pylab.clf()
    pylab.imshow(imgr,interpolation='nearest',aspect='equal')
    savefigure('output/simple_registered.png')
    #reg.show_segmentations_keypoints(figureofs=1,movedkpts=reg.newpts,title=False)
    #pylab.figure(1);
    #savefigure('output/simple_fixed_keypoints.png')
    #pylab.figure(2) ;
    #savefigure('output/simple_moving_keypoints.png')
    pylab.figure(1) ; pylab.clf() ;
    reg.show_edges() ;
    savefigure('output/simple_edges.pdf')

def article_exp_multigene():
    """ Register case03-3-psap-segment and case03-5-he-segment """
    img=scipy.misc.imread('imgs/case03-3-psap.png')
    imgw=scipy.misc.imread('imgs/case03-5-he.png')
    #imgs=scipy.misc.imread('output/case03-3-psap-segment.png')
    #imgsw=scipy.misc.imread('output/case03-5-he-segment.png')
    # read segmentations and apply median filter
    im=Image.open('output/case03-3-psap-segment.png')
    im=im.filter(ImageFilter.ModeFilter(size=10))
    imgs=np.copy(im)
    im=Image.open('output/case03-5-he-segment.png')
    im=im.filter(ImageFilter.ModeFilter(size=10))
    imgsw=np.copy(im)
    # convert colours to numbers 0,1,2,3
    s=segment3(imgs)
    sw=segment3(imgsw)
    # pdb.set_trace()
    reg=SegReg(s,sw,alpha=50,d=50,maxd=3,l=6)
    # pdb.set_trace()
    reg.register_multires(outprefix='output/case03_')
    imgr=reg.warp_image(imgw)
    #scipy.misc.imsave('output/case03_registered.png',imgr)
    #pylab.clf()
    #pylab.imshow(imgr,interpolation='nearest',aspect='equal')
    #savefigure('output/case03_registered.png')

    pylab.clf()
    pylab.imshow((imgw[39:-39,:]/2+img/2))
    scipy.misc.imsave('output/case03_fused_before.png',(imgw[39:-39,:]/2+img/2))
    #savefigure('output/case03_fused_before.png')
    pylab.imshow((imgr/2+img/2))
    scipy.misc.imsave('output/case03_fused_after.png',imgr/2+img/2)
    # savefigure('output/case03_fused_after.png')

    #pylab.clf()
    #pylab.imshow(np.asarray(imgw[:img.shape[0],:img.shape[1]],'i1')-img,interpolation='nearest',aspect='equal')
    #savefigure('output/case03_diffbefore.png')
    #pylab.clf()
    #pylab.imshow(np.asarray(imgr,'i1')-img,interpolation='nearest',aspect='equal')
    #savefigure('output/case03_diffafter.png')

    #imgr=reg.warp_image(sw)
    #pylab.clf()
    #pylab.imshow(imgr,interpolation='nearest',aspect='equal')
    #savefigure('output/case03_segment-registered.png')

    #pylab.clf()
    #pylab.imshow(np.asarray(s,'i1')-sw[:s.shape[0],:s.shape[1]],interpolation='nearest',aspect='equal')
    #savefigure('output/case03_segment-diffbefore.png')
    #pylab.clf()
    #pylab.imshow(s-imgr,interpolation='nearest',aspect='equal')
    #savefigure('output/case03_segment-diffafter.png')

    #reg.show_edges() ;


def article_exp_ratkidney():
    """ Register Rat_Kidney_HE and Rat_Kidney_PanCytokeratin """
    #pylab.close('all') ; pylab.figure(1) ;
    img=scipy.misc.imread('imgs/Rat_Kidney_HE_Section02-0.png')
    imgw=scipy.misc.imread('imgs/Rat_Kidney_PanCytokeratin_Section03-0.png')
    #imgs=scipy.misc.imread('output/case03-3-psap-segment.png')
    #imgsw=scipy.misc.imread('output/case03-5-he-segment.png')
    # read segmentations and apply median filter
    im=Image.open('output/Rat_Kidney_HE-segment.png')
    im=im.filter(ImageFilter.ModeFilter(size=10))
    imgs=np.copy(im)
    im=Image.open('output/Rat_Kidney_PanCytokeratin-segment.png')
    im=im.filter(ImageFilter.ModeFilter(size=10))
    imgsw=np.copy(im)
    # convert colours to numbers 0,1,2,3
    s=segment3(imgs)
    sw=segment3(imgsw)
    # pdb.set_trace()
    reg=SegReg(s,sw,alpha=50,d=50,maxd=3,l=6)
    # pdb.set_trace()
    reg.register_multires(outprefix='output/ratkidney-hecyto_')
    imgr=reg.warp_image(imgw)
    pylab.clf()
    pylab.imshow(imgr,interpolation='nearest',aspect='equal')
    scipy.misc.imsave('output/ratkidney-hecyto_registered.png',imgr)
    #savefigure('output/ratkidney-hecyto_registered.png')

    pylab.clf()
    pylab.imshow((imgw/2+img[32:-32,:]/2))
    scipy.misc.imsave('output/ratkidney-hecyto_fused_before.png',imgw/2+img[32:-32,:]/2)
    #savefigure('output/ratkidney-hecyto_fused_before.png')
    pylab.imshow((imgr/2+img/2))
    scipy.misc.imsave('output/ratkidney-hecyto_fused_after.png',(imgr/2+img/2))
    #savefigure('output/ratkidney-hecyto_fused_after.png')

    #pylab.clf()
    #pylab.imshow(np.asarray(imgw[:img.shape[0],:img.shape[1]],'i1')-img,interpolation='nearest',aspect='equal')
    #savefigure('output/case03_diffbefore.png')
    #pylab.clf()
    #pylab.imshow(np.asarray(imgr,'i1')-img,interpolation='nearest',aspect='equal')
    #savefigure('output/case03_diffafter.png')

    # imgr=reg.warp_image(sw)
    # pylab.clf()
    # pylab.imshow(imgr,interpolation='nearest',aspect='equal')
    # savefigure('output/ratkidney-hecyto_segment-registered.png')

    # pylab.clf()
    # pylab.imshow(np.asarray(s,'i1')-sw[:s.shape[0],:s.shape[1]],interpolation='nearest',aspect='equal')
    # savefigure('output/ratkidney-hecyto_segment-diffbefore.png')
    # pylab.clf()
    # pylab.imshow(s-imgr,interpolation='nearest',aspect='equal')
    # savefigure('output/ratkidney-hecyto_segment-diffafter.png')

    #reg.show_edges() ;

def try_mesh_segregtri():
    plt.close('all')
    plt.ion()
    img0=scipy.misc.imread('imgs/case03-5-he-small.png')
    #segm1=scipy.misc.imread('output/case03-5-he-small-autosegment.png')
    img1=scipy.misc.imread('imgs/case03-3-psap-small.png')
    #segm2=scipy.misc.imread('output/case03-3-psap-small-autosegment.png')
    par=bigtools.Parameters({'wsize':10, 'maxdispl':3, 'alpha':1.0,
                             'mlevels':4,'show_edges':True,'warpinterporder':2})
    file=np.load('output/case03-5-he-small-autosegment.npz')
    pts=file['pts']
    segm0=file['segm0']
    segm1=file['segm1']
    reg=SegRegTri(segm0,segm1,par)
    #pdb.set_trace()
    reg.set_pts(pts)
    #plt.figure()
    reg.find_edges(show=False)
    #plt.title('image with overlaid mesh')
    plt.figure()
    # pdb.set_trace()
    reg.set_transform() # needed for show_overlaid_segmentations
    reg.show_overlaid_segmentations()
    plt.title('Before')
    plt.figure() ; plt.clf()
    plt.imshow(img1) ; plt.title('Moving image') ;
    plt.show(block=False)
    #pdb.set_trace()
    #imgr=reg.warp_image(img1)
    reg.register_multires()
    imgr=reg.warp_image(img1)
    segm1w=reg.warp_labels(segm1)
    plt.figure()
    reg.show_overlaid_segmentations()
    plt.title('After')
    plt.figure() ; plt.clf()
    plt.imshow(imgr) ; plt.title('Warped') ;
    reg.transform.saveimg('output/deformation.png')


if __name__ == "__main__":  # running interactively
    # try_segmentation()
    #try_diffs()
    #try_reg()
    #try_mi()
    #pylab.show()
    #pylab.close('all')
    #try_register()
    #try_mi2()
    #try_warp2()
    #try_register_multires()
    #article_exp_multires()
    #article_exp_multigene()
    #article_exp_ratkidney()
    try_mesh_segregtri()
    #test_affine()
