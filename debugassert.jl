# Assert macro @debugassert works like @assert except that it can be switched
# on and off by calling switchasserts(true) and switchasserts(false)
#
# Example:
# include("debugassert")
# switchasserts(true)
# @debugassert(1==2)
# switchasserts(false)
# @debugassert(1==2)
#
# Jan Kybic, kybic@fel.cvut.cz, August 2014

function switchasserts(doassert::Bool)
    if doassert
        # define debugassert as assert
        @eval macro debugassert(ex,msgs...)
            return :(@assert($(esc(ex)),$(msgs...)))
        end
    else
        # define debugassert as empty
        @eval macro debugassert(ex,msgs...)
            return :nothing
        end
    end
end
    
switchasserts(false) # by default, do nothing
