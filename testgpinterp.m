% test of gpinterp

x= [ 1  5  6  7  9  10 ] ;
 y= [ 3  5  4  5  3  3 ] ;
%y= [ 3  5  7  8  9  10 ] ;

t=-4:0.1:15 ;
n=length(t) ;
sigma=0.9 ;
z=gpinterp(x,y,t,sigma) ;

sigmak=0.7 ;
zk=zeros(1,n) ;
for i=1:n,
  zk(i)=kernelinterp(x,y,t(i),sigmak) ;
end ;


figure(1) ; clf ;
plot(x,y,'o') ;
hold on ;
plot(t,z,'b-') ;
plot(t,zk,'r-') ;
hold off ;
legend('measured','GP','kernel') 