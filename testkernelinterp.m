% test of kernelinterp

x= [ 1 ; 5 ; 6 ; 7 ; 9 ; 10 ] ;
y= [ 3 ; 5 ; 4 ; 5 ; 3 ; 3 ] ;
t=-4:0.1:15 ;
n=length(t) ;
sigma=0.4 ;
z=zeros(1,n) ;

for i=1:n,
  z(i)=kernelinterp(x,y,t(i),sigma) ;
end ;

figure(1) ; clf ;
plot(x,y,'o') ;
hold on ;
plot(t,z,'-') ;
