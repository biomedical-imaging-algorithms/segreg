# Thin plate splines module
#
# Jan Kybic, October 2012

import numpy as np
import scipy
import scipy.linalg as la
import pylab
from segreg import dist
import math
import matplotlib
import pdb
import time
# to import Cython
#import pyximport
#pyximport.install(reload_support=True)
import segregtools

def sym_svd(m,adim=None):
  """ For symmetric m, return u,l such that m=u*diag(l)*u'. Optionally
truncate u and l to adim vectors"""
  u,l,v = np.linalg.svd(m)
  n=l.shape[0]
  for i in xrange(n):
    if np.dot(u[:,i],v[i])<0:
      l[i]=-l[i]
      v[i]=-v[i]
  if adim is not None:
    l=l[:adim] ; u=u[:,:adim] ; v=v[:adim,:]
  return u,l,v

def randomized_svd(a,adim):
  """ For symmetric a, calculate an approximate
  truncated SVD decomposition a=u*diag(l)*u' """
  t=time.time()
  n=a.shape[0]
  assert a.shape[1]==n
  k=2*adim # number of random vectors
  omega=np.random.normal(size=(n,k))
  y=np.dot(a,omega)
  for i in xrange(2):
    y=np.dot(np.transpose(a),y)
    y=np.dot(a,y)
  # find an orthonormal basis for y  
  q,_=la.qr(y)
  b=np.dot(np.transpose(q),a)
  ut,s,v=la.svd(b)
  ut=ut[:,:adim] ; s=s[:adim] ; v=v[:adim,:]
  u=np.dot(q,ut)
  t=time.time()-t
  print "randomized_svd took ",t
  return u,s,v

class Tps:
  """ Thin plate spline implementation """

  def __init__(self,points):
      """ Points in 2D in which the value is known (Numpy array)"""
      self.points = points
      assert points.shape[1]==2
      self.n=points.shape[0] # number of points
      self.matrix=None
      self.coefs=None
      self.redmatrix=None
      self.redcoefs=None
      self.adim=None
      self.lu=None

  def get_matrix(self):
      """ Get the system matrix. Calculation is only performed the first time. """
      #pdb.set_trace()
      if self.matrix is not None:
          return self.matrix

      self.matrix=segregtools.thin_plate_spline_matrix(self.points.astype('float'))
      # a=np.zeros((self.n+3,self.n+3))
      # #a=np.zeros((self.n,self.n))
      # for i in xrange(self.n):
      #   for j in xrange(i+1,self.n):
      #     r2=dist(self.points[i],self.points[j])
      #     v=0.5*r2*math.log(r2)
      #     #v=math.exp(-r2/(2*1))
      #     a[i,j]=v ; a[j,i]=v
      #     a[i,-3:]=[1,self.points[i][0],self.points[i][1]]
      #     a[-3:,i]=[1,self.points[i][0],self.points[i][1]]
      # pdb.set_trace()  
      # self.matrix=a
      return self.matrix

  def get_coefs(self,values):
      """ Get the thin-plate spline coefficients from values """
      #if self.coefs is not None:
      #    return self.coefs
      b=np.concatenate((values,np.zeros(3)))
      if self.lu is None:
        self.lu=la.lu_factor(self.get_matrix())
      # w=np.linalg.solve(self.get_matrix(),b)
      w=la.lu_solve(self.lu,b)
      self.coefs=w
      return w

  def interpolate_one(self,x):
      """ Interpolate at one position x """
      assert self.coefs is not None
      #assert x.shape==(2,)
      #w=self.coefs
      #s=w[-3]+w[-2]*x[0]+w[-1]*x[1]
      #for i in xrange(self.n):
      #    r2=dist(x,self.points[i]) 
      #    if r2>0:
      #        v=0.5*r2*math.log(r2)
      #        s+=v*self.coefs[i]
      #return s
      return segregtools.interpolate_tps_one(self.coefs,self.points.astype('float'),
                                             x[0],x[1])    

  def get_interpolator(self,values):
      """ Given [values] at [points], return an interpolator function that accepts
      a tuple (x,y) and returns the interpolated value. Several independent interpolator can be generated """
      coefs=self.get_coefs(values)
      pts=self.points.astype('float')
      #pdb.set_trace()
      def f ((x,y)):
        # pdb.set_trace()
        return segregtools.interpolate_tps_one(coefs,pts,x,y)
      return f
          
  def interpolate(self,x):
      """ Interpolate at positions x """
      m=x.shape[0]
      y=np.zeros(m)
      for i in xrange(m):
          y[i]=self.interpolate_one(x[i])
      return y

class TpsRegression(Tps):
  """ Thin plate regression splines (Wood) """

  def __init__(self,points):
      """ Points in 2D in which the value is known (Numpy array)"""
      self.points = points
      assert points.shape[1]==2
      self.n=points.shape[0] # number of points
      self.matrix=None
      self.coefs=None
      self.redmatrix=None
      self.redcoefs=None
      self.adim=None

  def get_matrix(self):
      """ Get the system matrix. Calculation is only performed the first time. """
      if self.matrix is not None:
          return self.matrix
      t=time.time()
      a=np.zeros((self.n+3,self.n+3))
      #a=np.zeros((self.n,self.n))
      for i in xrange(self.n):
          for j in xrange(i+1,self.n):
              r2=dist(self.points[i],self.points[j])
              v=0.5*r2*math.log(r2)
              #v=math.exp(-r2/(2*1))
              a[i,j]=v ; a[j,i]=v
          a[i,-3:]=[1,self.points[i][0],self.points[i][1]]
          a[-3:,i]=[1,self.points[i][0],self.points[i][1]]
      self.matrix=a
      t=time.time()-t
      print "get_matrix took ",t
      return a

  def get_reduced_matrix(self,adim='full',use_random=False):
      """ Get reduced matrix (see article by Wood) """
      if self.redmatrix is not None:
        return self.redmatrix
      t=time.time()
      n=self.n
      if adim=='full':
        adim=n+3
      elif adim=="auto":
        adim=3+math.ceil(math.pow(n,1/2.))
      print "adim=",adim
      self.get_matrix()
      p=self.matrix[:n,n:] # matrix P of the orthogonality costraints
      k=self.matrix[:n,:n] # matrix K of the RBF
      # find adim approximation of k
      if use_random:
        u,l,v=randomized_svd(k,adim=adim)
      else:
        u,l,v=sym_svd(k,adim=adim)
      #print "k=",k[:5,:5]
      ka=np.dot(np.dot(u,np.diag(l)),v)
      #print "ka=",ka[:5,:5]
      print "diff=",la.norm(k-ka)/la.norm(k)
      #pdb.set_trace()
      self.u=u
      self.l=l
      self.p=p
      # find basis of the null space of p'*u
      ptu=np.dot(np.transpose(p),u)
      _, _,puv = np.linalg.svd(ptu)
      # extract the basis of the null space
      z=np.transpose(puv[3:])
      self.z=z
      # create reduced system matrix
      l1=np.dot(np.dot(np.transpose(z),np.diag(l**2)),z)
      l2=np.dot(np.dot(np.transpose(z),np.diag(l)),np.transpose(ptu))
      l3=np.dot(np.transpose(p),p)
      self.redmatrix=np.vstack((np.hstack((l1,l2)),np.hstack((np.transpose(l2),l3))))
      t=time.time()-t
      print "get_reduced_matrix took ",t
      return self.redmatrix
      
  def get_coefs(self,v):
      """ Get the thin-plate spline coefficients from values """
      if self.coefs is not None:
          return self.coefs
      L=self.get_reduced_matrix()
      z=self.z ; u=self.u ; l=self.l ; p=self.p
      rhs=np.concatenate((np.dot(np.dot(np.transpose(z),np.diag(l)),np.dot(np.transpose(u),v)),np.dot(np.transpose(p),v)))
      w=np.linalg.solve(L,rhs)
      self.redcoefs=w
      self.coefs=np.concatenate((np.dot(np.dot(u,z),w[:-3]),w[-3:]))
      return self.coefs

  def get_smoothness_matrix(self):
    # invert the reduced system matrix and parition it
    Li=la.inv(self.get_reduced_matrix())
    La=Li[:-3,:-3] ; Lb=Li[:-3,-3:]
    M=np.dot(np.dot(np.dot(La,np.transpose(self.z)),np.diag(self.l)),
             np.transpose(self.u))+np.dot(Lb,np.transpose(self.p))
    ZM=np.dot(self.z,M)
    Q=np.dot(np.dot(np.transpose(ZM),np.diag(self.l)),ZM)
    return Q
    
# -----------------------------------------------
          
def try_tps():
    pts=np.array(((1,3),(2,3),(3,3),(4,4),(3,5),(2,5),(1,5),(1,4)),'float')
    z=np.array((1,1,1,2,3,4,4,2),'float')
    pylab.plot(pts[:,0],pts[:,1],'o')
    pylab.axis([0,5,2,6])
    t=Tps(pts)
    m=t.get_matrix()
    print m
    w=t.get_coefs(z)
    print w
    y=t.interpolate(pts)
    print y
    xs=np.arange(0,5,0.1) ; nxs=xs.shape[0]
    pts2=np.transpose(np.vstack((xs,5*np.ones(nxs))))
    y2=t.interpolate(pts2)
    f=t.get_interpolator(z)
    y3=np.zeros(nxs)
    for i in xrange(nxs):
      y3[i]=f((pts2[i,0],pts2[i,1]))
    pylab.clf()
    #pdb.set_trace() 
    pylab.plot(xs,y2,'b-',xs,y2,'r--',pts[4:7][:,0],z[4:7],'go')
    return m

def find_first_larger(x,t):
    """ Find the index of the first element of x, larger or equal tot """
    for i in xrange(x.shape[0]):
      if x[i]>=t:
        return i
    raise KeyError("not found in find_first_larger")  
 
def get_edges(mi,err=0.1,maxedg=None):
    """ Find important interactions given the top left part of the
    inverse of the TPS system matrix"""
    # sort
    # pdb.set_trace()
    mi=np.copy(mi)
    #np.fill_diagonal(mi,0)
    t=np.ravel(mi)
    t=abs(t*(t<0))
    nrm=sum(t) # norm
    t=np.sort(t)
    t=t[-1::-1] # descend order
    i=find_first_larger(np.cumsum(t),(1-err)*nrm)
    if not maxedg is None:
      i=min(maxedg*2,i) # because the matrix is symmetric 
    thr=t[i] # threshold
    edgs=np.nonzero(mi<-thr)
    # convert to list and filter duplicate entries
    edgs=list(np.transpose(np.array(edgs)))
    edgs=filter(lambda a: a[0]>a[1],edgs)
    edgw=map(lambda a: -mi[a[0],a[1]], edgs)
    print "Edges: ", len(edgs)
    return edgs,edgw
    
def get_rectangle_pts(n=10):
    bottom=np.transpose(np.vstack((np.arange(1,n),np.ones(n-1))))
    top=np.transpose(np.vstack((np.arange(1,n),(n-1)*np.ones(n-1))))
    right=np.transpose(np.vstack((n-1*np.ones(n-3),np.arange(2,n-1))))
    left=np.transpose(np.vstack((1*np.ones(n-3),np.arange(2,n-1))))
    pts=np.concatenate((bottom, right, top, left))
    return pts
    

def show_struct():
    pts=get_rectangle_pts()
    n=pts.shape[0] ;
    pylab.clf()
    pylab.plot(pts[:,0],pts[:,1],'bo')
    pylab.axis([0,10,0,10])
    z=np.zeros(n) ;
    t=Tps(pts)
    m=t.get_matrix()
    mi=np.linalg.inv(m)[:-3,:-3]
    # large elements here mean important interactions
    edgs,edgw=get_edges(mi)
    for e in edgs:
      ii=e[0] ; ij=e[1]
      pylab.plot((pts[ii][0],pts[ij][0]),(pts[ii][1],pts[ij][1]),'k-')
    pylab.axis([0,10,0,10])
    pdb.set_trace()

def eig_approx(w,v):
    """ Approximate a matrix given a (truncated) eigenvalue decomposition """
    n=w.shape[0] # number of eigenvalues
    m=v.shape[0] # size of the matrix = length of the eigenvectors
    assert v.shape[1]==n
    a=np.zeros((m,m))
    for i in xrange(n):
      a+=np.outer(v[:,i],v[:,i])*w[i]
    return a

def try_eigendecomp():
    pts=get_rectangle_pts()
    #pts=np.array(((1,3),(2,3),(3,3),(4,4),(3,5),(2,5),(1,5),(1,4)),'float')
    n=pts.shape[0] ;
    t=Tps(pts)
    m=t.get_matrix()
    print "m=",m
    # calculate eigenvalues
    w,v=np.linalg.eig(m)
    print "w=",w
    print v
    # sort eigenvalues
    ind=np.argsort(abs(w))[-1::-1]
    w=w[ind]
    v=v[:,ind]
    print "sorted w=",w
    print v
    m1=eig_approx(w,v)
    print "m1=",m1
    print "diff1=", sum(abs(m1-m).ravel())
    #pylab.clf()
    #pylab.plot(abs(w))
    # how many eigenvalues to keep 
    #k=math.ceil(math.pow(w.shape[0],1/3.))+3
    #print "k=",k
    k=10 ;
    wa=w[:k]
    va=v[:,:k]
    m2=eig_approx(wa,va)
    print "m2=",m2
    print "diff2=", sum(abs(m2-m).ravel())
    # approximate inverse
    mi=np.linalg.inv(m)[:-3,:-3]
    print "mi=",mi
    mi1=eig_approx(1./wa,va)[:-3,:-3]
    print "mi1=",mi1
    print "diffi1=", sum(abs(mi1-mi).ravel())
    pdb.set_trace()
    pylab.clf() ; pylab.imshow(m,interpolation='nearest') ; pylab.colorbar()
    pylab.clf() ; pylab.imshow(m2,interpolation='nearest') ; pylab.colorbar()
    pylab.clf() ; pylab.imshow(m2-m,interpolation='nearest') ; pylab.colorbar()
    pylab.clf() ; pylab.imshow(mi,interpolation='nearest') ; pylab.colorbar()
    pylab.clf() ; pylab.imshow(mi1,interpolation='nearest') ; pylab.colorbar()
    pylab.clf() ; pylab.imshow(mi1-mi,interpolation='nearest') ; pylab.colorbar()

def largest_eigen(m,k=None):
  """ Find k largest eigenvalues of m and corresponding eigenvectors """
  w,v=np.linalg.eig(m)
  # sort eigenvalues
  ind=np.argsort(abs(w))[-1::-1]
  if k is None:
    k=w.shape[0]
  w=w[ind][:k]
  v=v[:,ind][:k]
  return w,v


def try_zerospace():
  """ Test the zero space approximation suggested by Woods """
  pts=get_rectangle_pts(n=10)
  n=pts.shape[0] ;
  t=Tps(pts)
  m=t.get_matrix()
  p=m[:n,n:] # matrix P of the orthogonality costraints
  k=m[:n,:n] # matrix K of the RBF
  print "k=",k[:5,:5]
  adim=10
  # find adim approximation of k
  #u,l,v = np.linalg.svd(k)
  u,l=sym_svd(k,adim=adim)
  ka=np.dot(np.dot(u,np.diag(l)),np.transpose(u))
  print "ka=",ka[:5,:5]
  print "diff=",np.linalg.norm(ka-k)/la.norm(k)          
  # find basis of the null space of p'*u
  ptu=np.dot(np.transpose(p),u)
  puu, pul,puv = np.linalg.svd(ptu)
  # extract the basis of the null space
  z=np.transpose(puv[3:])
  # check
  #print np.dot(ptu,z)

np.set_printoptions(precision=3)


def try_redtps():
    pts=get_rectangle_pts(n=10)
    z=pts[:,0]**2
    pylab.plot(pts[:,0],pts[:,1],'o')
    pylab.axis([0,10,0,10])
    t=TpsRegression(pts)
    t.get_reduced_matrix()
    t.get_coefs(z)
    y=t.interpolate(pts)
    n=pts.shape[0]
    print np.transpose(np.vstack((z,y)))
    print "diff=",la.norm(z-y)
    pylab.clf()
    pylab.plot(np.arange(n),z,'ro',np.arange(n),y,'g+')
    # pdb.set_trace()

def show_struct2():
   nn=20
   pts=get_rectangle_pts(n=nn)
   n=pts.shape[0] ;
   print "n=",n
   pylab.clf()
   pylab.plot(pts[:,0],pts[:,1],'bo')
   pylab.axis([0,nn,0,nn])
   t=TpsRegression(pts)
   t.get_reduced_matrix()
   Q=t.get_smoothness_matrix()
   edgs,edgw=get_edges(Q,err=1e-6,maxedg=int(2*n))
   for e in edgs:
     ii=e[0] ; ij=e[1]
     pylab.plot((pts[ii][0],pts[ij][0]),(pts[ii][1],pts[ij][1]),'k-')
   pylab.axis([0,nn,0,nn])
   print "edgw=",max(edgw)
   #pdb.set_trace()

if __name__ == "__main__":  # running interactively
    pylab.clf()
    try_tps()
    #show_struct()
    #try_eigendecomp()
    #try_zerospace()
    #try_redtps()
    #t=time.time()
    #show_struct2()
    #t=time.time()-t
    #print "Elapsed time ",t
    #pylab.show()
    
