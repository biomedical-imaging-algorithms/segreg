""" My implementation of the pool """

import multiprocessing

def pmaprange(f,start,end):
  """ Equivalent of r=[] ; for i in xrange(start,end): r.append(f[i]) """
  ncpu=multiprocessing.cpu_count()
  queue=multiprocessing.Queue()
  chunksize=int(math.ceil(float(end-start)/ncpu))
  numchunks=int(math.ceil(end-start)/chunksize))
  def exec(i0,i1):
    r=[]
    for j in xrange(i0,i1):
      r.append(f
