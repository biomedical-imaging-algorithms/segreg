#!/usr/bin/python
#
# Experiments with simultaneous segmentation in Python
#
# Jan Kybic, November 2012

import matplotlib
# for non-interactive use
#matplotlib.use('Agg')
import matplotlib.pyplot as plt

import numpy as np
import scipy
import scipy.ndimage
import scipy.misc
import time
import pdb
import thinplatesplines as tps
#import beliefpropagation as bp
from PIL import Image
import ImageFilter

#import classifier
import pylab
import classifier_cython as classifier
#import classifier
import supergraphcuts

# to import Cython
#import pyximport
#pyximport.install(reload_support=True)
import segregtools
import bigtools
import slic  # in ~/builds/slic-python
import segreg

class SuperpixelImage:
    """ Image segmented to superpixel and its methods """

    def __init__(self,img,par=None): # par are the parameters
        """ find superpixel segmentation. img is a RGB image """
        self.par=bigtools.Parameters(par,default=
          {'supsize':1000, 'supcomp':10, 'show_superpixels':False,
           'kernelorder':1 })
        self.img=img
        # apparently, the first component element is ignored (why???)
        argb= np.dstack([img[:, :, :1], img]).copy("C")
        print "Calculating superpixels..."
        t=time.time()
        self.slabels=slic.slic_n(argb,self.par.supsize,self.par.supcomp)
        print "Superpixels took", time.time()-t
        self.nsuper=np.max(self.slabels)+1
        print "Numer of superpixels=",self.nsuper
        self.sizes_super=segregtools.superpixel_sizes(self.slabels,self.nsuper)
        if self.par.show_superpixels:
            self.show_superpixels(self.slabels)
        self.features=None
        self.cols=np.array( (((0,0,0),(0,0,255),(255,0,0),(0,255,0),# black,blue,red,green
                      (255,255,0),(0,255,255),(255,0,255))), # yellow, cyan, magenta
                      dtype='u1')

    def show_superpixels(self,l):
        argb= np.dstack([self.img[:, :, :1], self.img]).copy("C")
        slic.contours(argb,l,50)
        plt.imshow(argb[:, :, 1:].copy())


    def get_features(self):
        """ Calculate features for all superpixel. feature[:,0]=1 """
        if self.features is not None:
            return self.features
        # setup parameters for features
        #n=3 # number of primitive features - colours
        # if self.par.kernelorder==1:
        #     self.numfeatures=1+n
        # elif self.par.kernelorder==2:
        #     self.numfeatures=1+n+n*(n-1)/2
        # elif self.par.kernelorder==3:
        #     self.numfeatures=1+n+n*(n-1)/2+n*(n-1)*(n-2)/6
        # else:
        #     raise ValueError('SuperpixelImage.get_features(): invalid kernelorder')

        t=time.time()
        # features are simply RGB values
        self.features=segregtools.mean_over_superpixels(self.slabels,self.img.astype(np.float),self.nsuper)
        print "mean_over_superpixels took",time.time()-t
        self.numfeatures=self.features.shape[1]
        # normalize features
        mean=np.sum(self.features,0)/self.nsuper
        mean[0]=0. # the first column stays
        self.features-=mean
        stdev= np.sqrt(np.sum(self.features**2,0)/self.nsuper)
        self.features/=stdev+1e-30 # to avoid division by zero
        return self.features

    def close_superpixels(self,x,y,r):
        return segregtools.close_superpixels(self.slabels,x,y,r)

    def show_fgbg(self,fgs,bgs,opacity=1):
        img=self.img.copy()
        imgr=img.reshape(-1,3)
        lblr=self.slabels.reshape(-1,)
        # foreground will be green
        fgcol=np.array([0,255,0])
        for i in fgs:
            imgr[lblr==i,:]=np.round(fgcol*opacity+imgr[lblr==i,:]*(1.-opacity))
        # background will be blue
        bgcol=np.array([0,0,255])
        for i in bgs:
            imgr[lblr==i,:]=np.round(bgcol*opacity+imgr[lblr==i,:]*(1.-opacity))
        plt.imshow(img) ; plt.title('img with foreground, background')



    def show_classes(self,cl,opacity=0.5):
        """ Shows an image overlaid with segmentation, provided as a list of superpixels and colours for each class """
        img=self.img.copy()
        imgr=img.reshape(-1,3)
        lblr=self.slabels.reshape(-1,)
        for (x,c) in cl:
            c=np.array(c)
            for i in x:
                imgr[lblr==i,:]=np.round(c*opacity+imgr[lblr==i,:]*(1.-opacity))
        plt.imshow(img) ; plt.title('classes')

    def show_classes_superpixellist(self,sl,opacity=0.5):
        """ Shows an image overlaid with segmentation, provided as a list of superpixels for each class """
        self.show_classes(zip(sl,self.cols),opacity)


    def show_classes_labels(self,l,opacity=0.5):
        """ Shows an image overlaid with segmentation, provided as an array of classes for each superpixel. class==-1 means unknown"""
        ncls=self.cols.shape[0]
        img=self.img.copy()
        imgr=img.reshape(-1,3)
        lblr=self.slabels.reshape(-1,)
        # pdb.set_trace()
        segregtools.overlay_labels_over_image(imgr,lblr,l.astype('i4'),self.cols[1:],opacity)

        plt.imshow(img)

    def show_segmentations_keypoints(self,l,pts,opacity=0.5):
      """ Show a segmentation overlaid with points """
      plt.figure() ; plt.clf()
      self.show_classes_labels(l,opacity=opacity)
      pylab.plot(pts[:,1],pts[:,0],'ok',markersize=5)
      plt.show(block=False)


class MutInf:
    """ Evaluate the criterion and gradient of mutual information """

    def __init__(self,P):
        """ P is the cross-probability matrix """
        assert abs(np.sum(P)-1.)<1e-3
        self.eps=1e-6
        self.P=P+self.eps/P.ravel().shape[0]
        self.Pi=np.sum(self.P,1) # marginal wrt i
        self.Pj=np.sum(self.P,0) # marginal wrt i
        self.logp=np.nan_to_num(np.log(self.P/np.outer(self.Pi,self.Pj)))

    def crit(self):
        # tensordot returns a 0-dimensional array
        return np.tensordot(self.P,self.logp).ravel()[0]

    def grad(self):
        g=self.logp-1
        return g

def show_overlay_classes(y1,y2,l1,l2,classes):
    nsize=np.max(np.vstack((l1.shape,l2.shape)),0)
    #ncls=len(classes)
    shift1=(nsize-l1.shape)/2
    shift2=(nsize-l2.shape)/2
    #pdb.set_trace()
    o=segregtools.overlay_classes(np.asarray(y1+1,'i4'),np.asarray(y2+1,'i4'),
                                  l1,l2,nsize,shift1,shift2,
                                  np.asarray(classes,dtype='u1'))
    plt.imshow(o) ;
    plt.show(block=False)


def find_maximum_permutation(P):
  """ Given a matrix P, find a permutation q(i), so that P[i,q(i)] are high.
      We use a greedy approach """
  sh=P.shape
  q=-np.ones(sh[0],np.int)
  s=set() # set of already
  for i in xrange(sh[0]):
    bestj=-1 ; bestval=-np.inf
    for j in xrange(sh[1]):
      if j not in s:
        val=P[i,j]
        if val>bestval:
          bestj=j ; bestval=val
    q[i]=bestj
    s.add(bestj)
    # print "i=",i," bestj=",bestj," s=",s," q=",q
  # pdb.set_trace()
  return q



class TwoImages:
    """ encapsulates two images, their superpixel segmentations. It can calculate the initial segmentations, the mutual information criterion and can optimize it.
    it can also show the segmentations """

    def __init__(self,img1,img2,par):
        self.par=bigtools.Parameters(par,default={'opacity':0.5,'lmbd_mi':0.1,
           'pgtol':1e-6,'use_barrier':False,'ninits':10,'ncls':3,'initr':20,
           'iprint':0,'numiter':3,'wsize':10, 'maxdispl':3, 'alpha':1.0,
           'mlevels':4,'do_affine':False})
        self.imgs=[img1,img2]
        #        self.s=segreg.parmap(lambda img: SuperpixelImage(img,self.par),self.imgs,
        #                     nproc=self.par.nproc)
        self.s=map(lambda img: SuperpixelImage(img,self.par),self.imgs)
        self.numimgs=2
        self.z=None
        self.calculate_shift()
        self.calculate_superpixel_overlaps()

    def calculate_shift(self):
        self.nsize=np.max(np.vstack([ i.shape for i in self.imgs ]),0)
        self.minsize=np.min(np.vstack([ i.shape for i in self.imgs ]),0)
        self.shift=[ (self.nsize-i.shape)/2 for i in self.imgs ]

    def calculate_superpixel_overlaps(self):
        """ calculate superpixel overlaps. Called from __init__ """
        print "Calculating superpixel overlaps"
        t=time.time()
        self.overlaps=segregtools.superpixel_overlaps(self.s[0].slabels,
           self.s[1].slabels,self.nsize,self.shift[0],self.shift[1])
        print "Calculating superpixel overlaps took", time.time()-t

    def transform_moving_superpixels(self,transform,transform_img=False):
        """ Transform superpixel labels in image 2 given transform
        (segreg.LandmarksTransform). Intended for iterative registration.
        """
        self.s[1].slabels=transform.warp_labels(self.s[1].slabels)
        if transform_img:
          self.s[1].img=transform.warp_image(self.s[1].img)
        self.calculate_shift()
        self.calculate_superpixel_overlaps()

    def show_superpixels(self):
      fgs=[]
      for i in xrange(self.numimgs):
            plt.figure()
            fgs.append(plt.gcf().number)
            self.s[i].show_superpixels(self.s[i].slabels)
            plt.show(block=False)
      return fgs

    def random_init(self):
        """ Generate one random initialization """
        coords=[]
        for j in xrange(self.par.ncls):
            # generate initial point for class j
            # make sure the points are inside both images
            m=self.minsize-2*self.par.initr
            s=(self.nsize-self.minsize)/2
            x=np.random.randint(m[0])+s[0]
            y=np.random.randint(m[1])+s[1]
            coords.append((x,y,self.par.initr))
        # coords=((200,500,10),(100,100,10),(320,180,10))
        self.initial_segmentation(coords)

    def best_random_init(self,showinitial=False,output_prefix=None):
        """ Randomly repeat initialization self.par.ninits times and choose the
        best result """
        bestcrit=np.inf ; besta=None ; besti=None
        fgss=[]
        for i in xrange(self.par.ninits):
            self.random_init()
            if showinitial:
              fgs=self.show_initial_segmentation()
              fgss.append(fgs)
            crit=self.optimize()
            if showinitial:
              self.hard_classify()
              self.show_segmentation()
              plt.show(block=False)
            print "best_random_init: i=%d crit=%g" % (i,crit)
            # pdb.set_trace()
            if crit<bestcrit:
                bestcrit=crit
                besta=self.get_a()
                besti=i
        if showinitial and output_prefix is not None:
          plt.figure(fgss[besti][0])
          segreg.savefigure(output_prefix+'segimg0.png')
          plt.figure(fgss[besti][1])
          segreg.savefigure(output_prefix+'segimg1.png')

        self.set_a(besta)
        # self.hard_classify() # to get the labels
        # find a permutation so that the classes have identical numbers of possible
        bestar=besta.reshape(2,self.ncls,self.c[0].kdim)
        a=bestar[1].copy()
        #z0= self.z[:]
        #pdb.set_trace()
        perm=self.best_permutation()
        for i in xrange(self.ncls):
          bestar[1,i]=a[perm[i]]
        self.set_a(besta)
        #perm=self.best_permutation()
        self.hard_classify() # I am not sure if this is needed
        return besta


    def initial_segmentation(self,coords):
        """ coords is a list of triples (x,y,r). Assume that the
        neighborhood around x[i],y[i] of size r[i] belongs to class i and
        train the classifier in both images accordingly. If the images differ
        in sizes, the positions are shifted to make the centers coincide"""
        self.ncls=len(coords) # number of classes
        self.f=[]  # features
        self.c=[]  # classifiers
        self.gt=[] # ground truth superpixels
        # self.labels=[]  # segmentation
        for i in xrange(self.numimgs):
            print "Identifying hard constraints for image ", i
            s=self.s[i]
            # get a list of superpixels for each class in this image
            gt=[]
            for j in xrange(self.ncls):
              x,y,r=coords[j]
              gt.append(list(
                  s.close_superpixels(x-self.shift[i][0],y-self.shift[i][1],r)))
            self.gt.append(gt)
            print "Calculating features for image ", i
            t=time.time()
            f=s.get_features()
            print "Calculating features took ", time.time()-t
            self.f.append(f)
            tr=[ (s.sizes_super[x],f[x]) for x in gt ]
            c=classifier.SoftmaxClassifier(tr,self.par)
            self.c.append(c)
            #l=c.hard_classify(f)
            #for i in xrange(len(tr)):
            #  print "class tr ",i, " ", c.hard_classify(tr[i][1])
            #  print "class f ",i, " ", c.hard_classify(f[gt[i]])
            #  print "class l",i, " ", l[gt[i]]
            #pdb.set_trace()

            # l=c.hard_classify(f)
            # self.labels.append(l)
        print "Calculating initial classification"
        self.evalprobs()
        self.hard_classify()

    def show_initial_segmentation(self,fignum=None):
        assert (self.c is not None)
        fgs=[]
        for i in xrange(self.numimgs):
            if fignum is None:
                plt.figure()
            else:
                plt.figure(fignum+2*i)
            fgs.append(plt.gcf().number)
            self.s[i].show_classes_superpixellist(self.gt[i])
            plt.title('Image %d seed superpixels' % i)
            plt.show(block=False)
            if fignum is None:
                plt.figure()
            else:
                plt.figure(fignum+1+2*i)
            self.s[i].show_classes_labels(self.labels[i])
            plt.title('Image %d initial segmentation' % i)
            plt.show(block=False)
        return fgs

    def show_overlaid_segmentations(self,fignum=None,maketitle=True,title='superimposed segmentation'):
        plt.figure(num=fignum)
        show_overlay_classes(self.labels[0],self.labels[1],self.s[0].slabels,
                             self.s[1].slabels,self.s[0].cols)
        if maketitle: plt.title(title)
        plt.show(block=False)

    def get_a(self):
        """ Get coefficients of both classifiers, as a vector """
        return np.hstack([ c.a.ravel() for c in self.c ])

    def set_a(self,a):
        """ Set coeficients of both classifiers and calculate class probabilities """
        #if np.all(a==self.get_a()) and self.z is not None:
        #    print "Same coefficients, nothing to do"
        #    return False
        n=self.c[0].kdim*self.ncls
        assert a.shape==(n*2,)
        self.c[0].a=a[:n].reshape(self.ncls,self.c[0].kdim)
        self.c[1].a=a[n:].reshape(self.ncls,self.c[0].kdim)
        self.z=[]
        #print "TwoImages: Calculating probabilities"
        self.evalprobs()
        # t=time.time()
        # print "Calculating probabilities took ", time.time()-t
        return True

    def evalprobs(self):
        self.z=[]
        for i in xrange(self.numimgs):
            self.z.append(self.c[i].evalprob(self.f[i]))


    def mutual_inf(self,a,calc_grad=False):
        """ Calculate mutual information criterion and its gradient, if required. """
        # print "mutual_inf called with calc_grad=",calc_grad, " a=",a
        #t0=time.time()
        self.set_a(a)
        totpix,P=segregtools.assemble_probability_matrix(self.ncls,self.overlaps,
                                                self.z[0],self.z[1])
        #print "Assembling probability matrix took", time.time()-t
        mi=MutInf(P)
        # pdb.set_trace()
        crit=-mi.crit()+self.par.lmbd_mi/(2.*self.ncls)*np.sum(a**2)
        # print "Probability matrix: ", P, " crit=", crit
        #self.hard_classify()
        #self.show_overlaid_segmentations(fignum=1)
        if not calc_grad:
            return crit
        gradP=mi.grad()
        # the minus sign is compensated by the minus sign at return
        grad=-self.par.lmbd_mi/float(self.ncls)*a.reshape(2,self.ncls,self.c[0].kdim)
        segregtools.mutual_inf_gradient(self.ncls,self.overlaps,self.z[0],self.z[1],
                                        self.f[0],self.f[1],P,gradP,grad,totpix,
                                        self.par.kernel_order)
        # print "Calculating mutual_inf took ", time.time()-t0, " crit=",crit
        return crit,-grad.ravel()

    def best_permutation(self):
        """ Permute classes for the second image so that they have maximum
        overlap with classes in the first image. The solution is only approximative.         """
        totpix,P=segregtools.assemble_probability_matrix(self.ncls,self.overlaps,self.z[0],self.z[1])
        #totpix,P=segregtools.assemble_probability_matrix_from_labels(self.ncls,self.overlaps,self.labels[0],self.labels[1])
        perm=find_maximum_permutation(P)
        return perm
        #self.labels[1]=perm[self.labels[1]].astype(np.int32)


    def optimize(self):
        """ Optimize classifiers to maximize mutual information """
        ncls=self.ncls
        kdim=self.c[0].kdim
        assert self.c[1].kdim==kdim
        def crit(a):
            J,g=self.mutual_inf(a,calc_grad=True)
            if not self.par.use_barrier:
                return J,g
            av=a.reshape((2,ncls,kdim))
            Js0,gs0=self.c[0].softmax_crit(av[0])
            Js1,gs1=self.c[1].softmax_crit(av[1])
            return J+Js0+Js1,g+np.hstack((gs0.ravel(),gs1.ravel()))
        # print "TwoImages: Classifier optimization started"
        t=time.time()
        a0=self.get_a()
        #opta,fopt,d=scipy.optimize.fmin_l_bfgs_b(critonly,a0,fprime=fprime,m=10,
        #    factr=1e-12,pgtol=self.par.pgtol,iprint=1,maxfun=1000)
        opta,fopt,d=scipy.optimize.fmin_l_bfgs_b(crit,a0,m=10,
            factr=1e-12,pgtol=self.par.pgtol,iprint=self.par.iprint,maxfun=1000)
        J=self.mutual_inf(opta,calc_grad=False)
        print "TwoImages: Classifier optimization took ", time.time()-t,  " d=",\
               d['warnflag'],' funcalls=', d['funcalls'], ' MI=',J
        self.set_a(opta)
        return J

    def optimize_gradient(self):
        """ Optimize using a gradient descent """
        ncls=self.ncls
        kdim=self.c[0].kdim
        assert self.c[1].kdim==kdim
        def crit(a):
            return self.mutual_inf(a,calc_grad=True)
        print "TwoImages: optimizer_gradient started"
        t=time.time()
        a=self.get_a()
        stepsize=1e-3
        J,g=crit(a)
        for i in xrange(100):
            J,g=crit(a)
            print "optimize_gradient: i=%d J=%g" % (i,J)
            if i>0:
                if J>Jold:
                    print "Warning: Criterion increases"
                    J=Jold ; g=gold ;
                    stepsize*=0.1
                else:
                    stepsize*=2.
            Jold=J ; gold=g ;
            a-=stepsize*g
        self.set_a(a)

    def hard_classify(self):
        """ Calculate hard classification"""
        self.labels=[ c.hard_classify(f) for (c,f) in zip(self.c,self.f) ]

    def show_segmentation(self,maketitle=True):
        assert (self.labels is not None)
        fgs=[]
        for i in xrange(self.numimgs):
            plt.figure()
            fgs.append(plt.gcf().number)
            self.s[i].show_classes_labels(self.labels[i])
            if maketitle:
              plt.title('Image %d segmentation' % i)
            plt.show(block=False)
        return fgs

    def optimal_graphcut_segmentation(self):
        """ Segment the image using calculated probabilities with Graphcut,
        choosing the best regularization in the sense of MI. Final segmentation
        is in self.labels """
        #gammas=[0,0.01,0.03,0.1,0.3,1,3,10,30,100]
        gammas=[0,0.1,0.3,1,3,10]
        sg= [ supergraphcuts.SuperGraphcuts(self.s[i],self.z[i])
              for i in xrange(self.numimgs) ]
        bestmi=-np.inf ; bestr=None ; bestgamma=None
        for gamma in gammas:
            print "Graphcut segmentation with gamma=", gamma
            r= [ sg[i].solve(gamma) for i in xrange(self.numimgs) ]
            totpix,P=segregtools.assemble_probability_matrix_from_labels(
                self.ncls,self.overlaps,r[0],r[1])
            mi=MutInf(P)
            crit=mi.crit()
            print "    MI=",crit
            if crit>bestmi:
                bestmi=crit
                bestr=r
                bestgamma=gamma
            #self.labels= [ x.astype(np.int) for x in bestr ]
            #self.show_segmentation()
            #plt.show(block=False)
            #pdb.set_trace()
        print "optimal_graphcut_segmentation for gamma=%f mi=%f" % (bestgamma,bestmi)
        self.labels= [ x.astype(np.int) for x in bestr ]

    def get_pixel_segmentations(self):
        """ Based on calculated superpixel segmentation, create
            pixel-level segmentations for both images."""
        return [ segregtools.distribute_labels_to_pixels(
                   self.labels[i],self.s[i].slabels)
                   for i in xrange(self.numimgs) ]

    # def iterate_with_affine(self,output_prefix=None,visualize=True):
    #   """ First iterate with affine, then without """
    #   wimg,transf,segms=self.iterate(output_prefix=output_prefix,visualize=visualize,do_affine=True)
    #   self.transform_moving_superpixels(transf,transform_img=visualize)
    #   wimg,transf,segms=self.iterate(output_prefix=output_prefix,visualize=visualize,do_affine=Fa)
    #   return wimg,transf,segms

    def iterate(self,output_prefix=None,visualize=True):
      """ Iterate between segmentation and registration.
      If output_prefix is set, save itermediate results to fines. """
      accumulated_transf=None
      i=0 # iteration number
      mi=None # mutual information
      while True:
        print "Iteration ", i, " in TwoImages.iterate; outerloop."
        if i<2: # for first two iterations
          # pdb.set_trace()
          if output_prefix is None:
            self.best_random_init()
          else:
            self.best_random_init(showinitial=False,
                                  output_prefix=output_prefix+'%d-seeds-' % i)
          #self.best_permutation()
          self.hard_classify()
          if visualize:
            #pdb.set_trace()
            fgs=self.show_segmentation(maketitle=False)
            plt.figure(fgs[0])
            if output_prefix is not None:
              segreg.savefigure(output_prefix+'segimg0-randominit-%d.png' % i)
            plt.figure(fgs[1])
            if output_prefix is not None:
              segreg.savefigure(output_prefix+'segimg1-randominit-%d.png' % i)
            self.show_overlaid_segmentations(maketitle=False)
            #plt.title('best random initialization %d' % i)
            if output_prefix is not None:
              segreg.savefigure(output_prefix+'combinedsegm-randominit-%d.png' % i)
          #self.optimize() # optimize segmentation
          #self.best_permutation()
          #pdb.set_trace()
          segms=self.get_pixel_segmentations()
          # if visualize:
          #   fgs=self.show_segmentation(maketitle=False)
          #   plt.figure(fgs[0])
          #   if output_prefix is not None:
          #     segreg.savefigure(output_prefix+'segimg0-optim-%d.png' % i)
          #   plt.figure(fgs[1])
          #   if output_prefix is not None:
          #     segreg.savefigure(output_prefix+'segimg1-optim-%d.png' % i)
          #   self.show_overlaid_segmentations(maketitle=False)
          #   # plt.title('optimized segmentations %d' % i)
          #   plt.show(block=False)
          #   segreg.savefigure(output_prefix+'combinedsegm-optim-%d.png' % i)
        # extract landmark positions in the first image
        pts=segregtools.find_boundary_points(self.s[0].slabels,
                                             self.labels[0].astype(np.int32))
        # start registration
        regpar=self.par
        if i>0:
          regpar=bigtools.Parameters(self.par,override={'show_edges':False})
        reg=segreg.SegRegTri(segms[0],segms[1],regpar)
        reg.set_pts(pts)
        if output_prefix is not None:
          outprefix=output_prefix+'reg-%d-' % i
        else:
          outprefix=None
        reg.register_multires(outprefix=outprefix,cols=self.s[0].cols,
                            do_affine=self.par.do_affine)
        #transform=reg.get_transform()
        #pdb.set_trace()
        #accumulated_transf=transform.compose(accumulated_transf)
        #self.transform_moving_superpixels(transform,transform_img=visualize)
        #reg.register_multires(outprefix=outprefix,ptsg=reg.newpts.copy(),
        #                      cols=self.s[0].cols,do_affine=False)
        # segm1w=reg.warp_labels(segms[1])
        #pdb.set_trace()
        if visualize:
          plt.figure()
          reg.show_overlaid_segmentations()
          #plt.title('after registration %d' % i)
          plt.show(block=False)
          if output_prefix is not None:
            segreg.savefigure(output_prefix+'combinedsegm-registered-%d.png' % i)
        transform=reg.get_transform()
        accumulated_transf=transform.compose(accumulated_transf)
        # later on, this can be done once at the end
        warped_image=accumulated_transf.warp_image(self.imgs[1])
        if visualize:
          plt.figure() ; segreg.show_combined_images(self.imgs[0],warped_image)
          #plt.title('after registration %d' % i)
          if output_prefix is not None:
            segreg.savefigure(output_prefix+'combinedimgs-registered-%d.png' % i)
          plt.figure() ; plt.imshow(warped_image)
          if output_prefix is not None:
            segreg.savefigure(output_prefix+'warped-%d.png' % i)
          plt.show(block=False)
        self.transform_moving_superpixels(transform,transform_img=visualize)
        # evaluate mutual information
        self.evalprobs()
        totpix,P=segregtools.assemble_probability_matrix(self.ncls,self.overlaps,
                                                self.z[0],self.z[1])
        oldmi=mi
        mi=MutInf(P).crit()
        print "i=", i, "Mutual information is ", mi
        i+=1
        if i>=self.par.numiter:
          print "Maximum number of outerloop iterations reached."
          break
        if i>1 and mi<oldmi+abs(oldmi)*0.01:
          print "Mutual information has not changed substantially."
          break
      segms=self.get_pixel_segmentations()
      return warped_image, accumulated_transf, segms


# ----------------------------------------------------------------------



def test_mi_grad():
    #a=np.array(((10,1,2),(3,5,5),(1,8,2),(7,2,6))).astype('float')
    a=np.array(((10,1,2),(3,5,5),(0,0,0),(7,2,6))).astype('float')
    #a=np.array(((10,1),(0,0))).astype('float')
    a=a/np.sum(a)
    # pdb.set_trace()
    m=MutInf(a)
    i0=m.crit()
    g=m.grad()
    h=1e-12
    for i in xrange(a.shape[0]):
        for j in xrange(a.shape[1]):
            ah=a.copy() ; ah[i,j]+=h ;
            i1=MutInf(ah).crit()
            print "i=",i," j=",j," analg=",g[i,j]," numgr=",(i1-i0)/h




def test_superpixel_segmentation():
    img=scipy.misc.imread('imgs/case03-5-he.png')
    par=bigtools.Parameters({'show_superpixels':True})
    pylab.figure(1) ; pylab.clf()
    # pdb.set_trace()
    s=SuperpixelImage(img,par)
    s.get_features()
    plt.figure(2) ; plt.clf()
    x=segregtools.distribute_to_pixels(s.features,s.labels,0)
    plt.imshow(x) ; plt.colorbar() ; plt.title('feature 1')
    plt.figure(3) ; plt.clf()
    x=segregtools.distribute_to_pixels(s.features,s.labels,1)
    plt.imshow(x) ; plt.colorbar() ; plt.title('feature 2')
    plt.figure(4) ; plt.clf()
    x=segregtools.distribute_to_pixels(s.features,s.labels,2)
    plt.imshow(x) ; plt.colorbar() ; plt.title('feature 3')
    plt.show()
    return s

def test_close_superpixels():
    img=scipy.misc.imread('imgs/case03-5-he.png')
    par=bigtools.Parameters({'show_superpixels':True})
    pylab.figure(1) ; pylab.clf()
    # pdb.set_trace()
    s=SuperpixelImage(img,par)
    z=s.close_superpixels(400,1000,10)
    f=np.zeros((s.nsuper,2))
    x=np.zeros(img.shape[:2],dtype='i4')
    for i in z:
        x[s.labels==i]=1
    # pdb.set_trace()
    pylab.figure(2) ; pylab.clf()
    s.show_superpixels(x)
    plt.title('close')
    plt.show(block=False)

def test_classification():
    img=scipy.misc.imread('imgs/case03-5-he-small.png')
    par=bigtools.Parameters({'show_superpixels':True,
                             'kernel_order':1})
    pylab.figure(1) ; pylab.clf()
    # pdb.set_trace()
    s=SuperpixelImage(img,par)
    # foreground superpixels
    fgs=list(s.close_superpixels(200,500,20))
    bgs=list(s.close_superpixels(200,100,20))
    pylab.figure(2) ; pylab.clf()
    s.show_fgbg(fgs,bgs,opacity=0.2)
    plt.show(block=False)
    #pdb.set_trace()
    f=s.get_features()
    # create a classifier
    tr=[ (s.sizes_super[bgs],f[bgs]), (s.sizes_super[fgs],f[fgs]) ]
    c=classifier.SoftmaxClassifier(tr,par)
    p=c.evalprob(f)[:,1]
    #pdb.set_trace()
    x=segregtools.distribute_to_pixels_vector(p,s.labels)
    pylab.figure(3) ; pylab.clf()
    plt.imshow(x) ; plt.title('probability map') ; plt.colorbar() ;
    plt.show(block=False)
    pylab.figure(4) ; pylab.clf()
    fgsl=np.nonzero(p>0.5)[0]
    bgsl=np.nonzero(p<=0.5)[0]
    s.show_fgbg(fgsl,bgsl,opacity=0.9) ; plt.title('segmented')
    plt.show(block=False)

def test_classify_three_classes():
    img=scipy.misc.imread('imgs/case03-5-he-small.png')
    par=bigtools.Parameters({'show_superpixels':True,
                             'kernel_order':1, 'supcomp':30,
                             'show_superpixels':True})
    pylab.figure(1) ; pylab.clf()
    s=SuperpixelImage(img,par)
    c1=list(s.close_superpixels(200,500,20))
    c0=list(s.close_superpixels(200,100,20))
    c2=list(s.close_superpixels(250,250,10))
    pylab.figure(2) ; pylab.clf()
    s.show_classes(((c0,[0,0,255]),(c1,[255,0,0]),(c2,[0,255,0])),opacity=0.5)
    plt.show(block=False)
    f=s.get_features()
    tr=[ (s.sizes_super[x],f[x]) for x in (c0,c1,c2) ]
    c=classifier.SoftmaxClassifier(tr,par)
    l=c.hard_classify(f)
    pylab.figure(3) ; pylab.clf()
    l0=np.nonzero(l==0)[0]
    l1=np.nonzero(l==1)[0]
    l2=np.nonzero(l==2)[0]
    s.show_classes(((l0,[0,0,255]),(l1,[255,0,0]),(l2,[0,255,0])),opacity=0.9) ; plt.title('segmented')
    plt.show(block=False)



def test_two_classifications():
    img1=scipy.misc.imread('imgs/case03-5-he-small.png')
    img2=scipy.misc.imread('imgs/case03-3-psap-small.png')
    par=bigtools.Parameters({'show_superpixels':True,
                             'kernel_order':1,'supcomp':30,'show_superpixels':True})
    pylab.figure(1) ; pylab.clf()
    s1=SuperpixelImage(img1,par)
    pylab.figure(2) ; pylab.clf()
    s2=SuperpixelImage(img2,par)
    # foreground superpixels
    xf,yf=(200,500)
    xb,yb=(200,100)
    r=20
    fgs1=list(s1.close_superpixels(xf,yf,r))
    bgs1=list(s1.close_superpixels(xb,yb,r))
    fgs2=list(s2.close_superpixels(xf,yf,r))
    bgs2=list(s2.close_superpixels(xb,yb,r))
    pylab.figure(3) ; pylab.clf()
    s1.show_fgbg(fgs1,bgs1,opacity=0.2)
    plt.title('initial segmentation 1')
    plt.show(block=False)
    pylab.figure(4) ; pylab.clf()
    s2.show_fgbg(fgs2,bgs2,opacity=0.2)
    plt.title('initial segmentation 2')
    plt.show(block=False)

    f1=s1.get_features()
    f2=s2.get_features()
    tr1=[ (s1.sizes_super[x],f1[x]) for x in (bgs1,fgs1) ]
    tr2=[ (s2.sizes_super[x],f2[x]) for x in (bgs2,fgs2) ]
    c1=classifier.SoftmaxClassifier(tr1,par)
    l1=c1.hard_classify(f1)
    c2=classifier.SoftmaxClassifier(tr2,par)
    l2=c2.hard_classify(f2)

    print "Showing segmentations"
    plt.figure(5) ; pylab.clf()
    l10=np.nonzero(l1==0)[0]
    l11=np.nonzero(l1==1)[0]
    s1.show_classes(((l10,[0,0,255]),(l11,[255,0,0])),opacity=0.7) ;
    plt.title('segmentation 1')
    plt.figure(6) ; pylab.clf()
    l20=np.nonzero(l2==0)[0]
    l21=np.nonzero(l2==1)[0]
    s2.show_classes(((l20,[0,0,255]),(l21,[255,0,0])),opacity=0.7) ;
    plt.title('segmentation 2')

    print "Showing overlay"
    pylab.figure(7) ; pylab.clf()
    show_overlay_classes(l1,l2,s1.labels,s2.labels,([0,0,255],[255,0,0,]))
    plt.title('superimposed segmentations')
    plt.show(block=False)

def test_two_images():
    plt.close('all')
    img1=scipy.misc.imread('imgs/case03-5-he-small.png')
    img2=scipy.misc.imread('imgs/case03-3-psap-small.png')
    par=bigtools.Parameters({'kernel_order':1,'supcomp':30,'opacity':0.5})
    t=TwoImages(img1,img2,par)
    #t.show_superpixels()
    # initial segmentation seeds for background and foreground
    t.initial_segmentation(((200,100,20),(200,500,20)))
    #t.show_initial_segmentation()
    #t.show_overlaid_segmentations()
    a=t.get_a()
    J,g=t.mutual_inf(a,calc_grad=True)
    h=1e-5
    for i in xrange(a.shape[0]):
        ah=a.copy() ; ah[i]+=h
        J1=t.mutual_inf(ah)
        gn=(J1-J)/h
        err=(gn-g[i])/gn
        print "i=%d analgr=%f numgr=%f relerr=%f" % (i,g[i],gn,err)

def test_two_images_optimize():
    plt.close('all')
    plt.ion()
    img1=scipy.misc.imread('imgs/case03-5-he-small.png')
    img2=scipy.misc.imread('imgs/case03-3-psap-small.png')
    par=bigtools.Parameters({'kernel_order':2,'supcomp':30,'opacity':0.5,
                             'pgtol':1e-6})
    t=TwoImages(img1,img2,par)
    #t.show_superpixels()
    # initial segmentation seeds for background and foreground
    t.initial_segmentation(((200,100,20),(200,500,20)))

    t.show_initial_segmentation()
    t.show_overlaid_segmentations() ; plt.title('initial')
    t.optimize()
    t.hard_classify()
    t.show_segmentation()
    t.show_overlaid_segmentations() ; plt.title('final')
    #pdb.set_trace()

def test_three_classes_optimize():
    plt.close('all')
    img1=scipy.misc.imread('imgs/case03-5-he-small.png')
    img2=scipy.misc.imread('imgs/case03-3-psap-small.png')
    par=bigtools.Parameters({'supsize':300,
                             'kernel_order':1,'supcomp':30,'opacity':0.5,
                             'use_barrier':False, 'beta':0.5})
    t=TwoImages(img1,img2,par)
    t.initial_segmentation(((200,100,20),(200,500,20),(250,270,20)))
    #t.show_initial_segmentation()
    #t.show_overlaid_segmentations() ; plt.title('initial')
    #classifier.test_gradfun(lambda x: t.mutual_inf(x,calc_grad=True),t.get_a(),h=1e-5)
    t.optimize()
    #t.optimize_gradient()
    t.hard_classify()
    t.show_segmentation()
    t.show_overlaid_segmentations() ; plt.title('final')
    pts=segregtools.find_boundary_points(t.s[0].slabels,t.labels[0].astype(np.int32))
    # pdb.set_trace()
    t.s[0].show_segmentations_keypoints(t.labels[0],pts,opacity=0.5)
    segm0=segregtools.distribute_labels_to_pixels(t.labels[0],t.s[0].slabels)
    #scipy.misc.imsave('output/case03-5-he-small-autosegment.png',x)
    segm1=segregtools.distribute_labels_to_pixels(t.labels[1],t.s[1].slabels)
    #scipy.misc.imsave('output/case03-3-psap-small-autosegment.png',x)
    np.savez_compressed('output/case03-5-he-small-autosegment.npz',pts=pts,segm0=segm0,segm1=segm1)

def test_same_image():
    plt.close('all')
    img1=scipy.misc.imread('imgs/case03-5-he-small.png')
    par=bigtools.Parameters({'kernel_order':2,'supcomp':30,'opacity':0.5})
    t=TwoImages(img1,img1,par)
    t.initial_segmentation(((200,100,20),(200,500,20),(250,270,20)))
    t.show_initial_segmentation()
    t.show_overlaid_segmentations() ; plt.title('initial')
    a0=t.get_a()
    a=a0+np.random.normal(scale=0.01,size=a0.shape)
    t.set_a(a)
    t.hard_classify()
    t.show_segmentation()
    t.show_overlaid_segmentations() ; plt.title('modified')
    t.optimize()
    t.hard_classify()
    t.show_segmentation()
    t.show_overlaid_segmentations() ; plt.title('final')

def test_order_two():
    plt.close('all')
    img1=scipy.misc.imread('imgs/case03-5-he-small.png')
    img2=scipy.misc.imread('imgs/case03-3-psap-small.png')
    par=bigtools.Parameters({'kernel_order':2,'supcomp':30,'opacity':0.5})
    t=TwoImages(img1,img2,par)
    t.initial_segmentation(((200,100,20),(200,500,20),(250,270,20)))
    t.show_initial_segmentation()
    t.show_overlaid_segmentations() ; plt.title('initial')

def test_three_classes_randomly():
    plt.close('all')
    img1=scipy.misc.imread('imgs/case03-5-he-small.png')
    img2=scipy.misc.imread('imgs/case03-3-psap-small.png')
    par=bigtools.Parameters({'kernel_order':1,'supcomp':30,'opacity':0.5,
                             'use_barrier':False, 'beta':0.5,
                             'ninits':10, 'initr':20, 'ncls':3 })
    t=TwoImages(img1,img2,par)
    t.best_random_init()
    t.optimize()
    #t.optimize_gradient()
    t.hard_classify()
    t.show_segmentation()
    t.show_overlaid_segmentations() ; plt.title('final')


def test_graphcut_segmentation():
    plt.close('all')
    img1=scipy.misc.imread('imgs/case03-5-he-small.png')
    img2=scipy.misc.imread('imgs/case03-3-psap-small.png')
    par=bigtools.Parameters({'kernel_order':1,'supcomp':30,'opacity':0.5,
                             'use_barrier':False, 'beta':0.5,
                             'ninits':10, 'initr':20, 'ncls':3 })
    t=TwoImages(img1,img2,par)
    plt.figure()
    t.s[0].show_superpixels(t.s[0].slabels)
    t.initial_segmentation(((200,100,20),(200,500,20),(250,270,20)))
    t.show_initial_segmentation()
    t.evalprobs() # just in case
    sg=supergraphcuts.SuperGraphcuts(t.s[0],t.z[0])
    gamma=1
    r=sg.solve(gamma)
    plt.figure()
    t.s[0].show_classes_labels(r)
    plt.title('Graphcut, gamma=%g' % gamma)
    plt.show(block=False)

def test_optimal_graphcut():
    plt.close('all')
    img1=scipy.misc.imread('imgs/case03-5-he-small.png')
    img2=scipy.misc.imread('imgs/case03-3-psap-small.png')
    par=bigtools.Parameters({'kernel_order':1,'supcomp':30,'opacity':0.5,
                             'use_barrier':False, 'beta':0.5})
    t=TwoImages(img1,img2,par)
    t.initial_segmentation(((200,100,20),(200,500,20),(250,270,20)))
    t.show_initial_segmentation()
    t.show_overlaid_segmentations() ; plt.title('initial')
    t.optimize()
    t.hard_classify()
    t.show_segmentation()
    t.show_overlaid_segmentations() ; plt.title('optimized')
    t.optimal_graphcut_segmentation()
    t.show_segmentation()
    t.show_overlaid_segmentations() ; plt.title('after graphcut')

def test_iterative_segmentation_registration(save=True):
    plt.close('all')
    prefix='output/iterative-case03-'
    img1=scipy.misc.imread('imgs/case03-5-he-small.png')
    img2=scipy.misc.imread('imgs/case03-3-psap-small.png')
    par=bigtools.Parameters({'kernel_order':1,'supcomp':30,'opacity':0.5,
                             'use_barrier':False, 'beta':0.5,
                             'ninits':10, 'initr':5, 'ncls':4,
                             'numiter':10, 'wsize':10, 'maxdispl':3, 'alpha':1.0,
           'mlevels':4,'show_edges':True})
    plt.figure() ; segreg.show_combined_images(img1,img2) ;
    if save:
      segreg.savefigure(prefix+'combinedimgs-before.png')
    plt.title('before registration')
    t=TwoImages(img1,img2,par)
    fgs=t.show_superpixels()
    for i in xrange(2):
      plt.figure(fgs[i])
      if save:
        segreg.savefigure(prefix+'superpixels%d.png' % i)
    wimg,transf,segms=t.iterate(output_prefix=prefix)
    plt.figure() ; segreg.show_combined_images(img1,wimg) ;
    if save:
      segreg.savefigure(prefix+'combinedimgs-after.png')
    plt.title('after final registration')
    t.show_segmentation()
    t.show_overlaid_segmentations() ;
    if save:
      segreg.savefigure(prefix+'combinedsegm-after.png')
    pdb.set_trace()

def reduce_to_size(img,target_width=1024,interp='bilinear'):
    """ Given an image, resize it so that its width is target_width if it is bigger.
        If the image is smaller, it is kept as it is.
        Returns the resized image and a reduction factor (<1.) """
    w=img.shape[1]
    if w<=target_width:
      return img, 1.
    sf=float(target_width)/w
    ht=int(sf*img.shape[0])
    return scipy.misc.imresize(img,(ht,target_width)),sf

def register_kidney_manual():
    plt.close('all')
    prefix='output/iterative-kidneyhepc-'
    img1=scipy.misc.imread('imgs/Rat_Kidney_HE_Section02-0.png')
    img2=scipy.misc.imread('imgs/Rat_Kidney_PanCytokeratin_Section03-0.png')
    par=bigtools.Parameters({'kernel_order':1,'supcomp':30,'opacity':0.5,
                             'use_barrier':False, 'beta':0.0,
                             'ninits':10, 'initr':5, 'ncls':3, 'lmbd':1e-3,
                             'lmbd_mi':1e-3, 'mindist':20,
                             'numiter':10, 'wsize':10, 'maxdispl':3, 'alpha':1.0,
           'mlevels':4,'iprint':0})
    plt.figure(1) ; plt.imshow(img1) ; plt.title('Image 1')
    plt.figure(2) ; plt.imshow(img2) ; plt.title('Image 2')
    plt.show(block=False)
    t=TwoImages(img1,img2,par)
    t.show_superpixels()
    t.best_random_init(showinitial=True)
    #t.initial_segmentation(((600,900,10),(600,1800,20),(200,200,20)))
    #t.show_initial_segmentation() ;plt.title('initial')
    #t.show_overlaid_segmentations() ;
    crit=t.optimize()
    print "crit=%g" % crit
    t.hard_classify()
    t.show_segmentation()
    #t.show_overlaid_segmentations() ; plt.title('optimized')
    plt.show(block=False)
    pdb.set_trace()



def register_kidney():
    plt.close('all')
    prefix='output/iterative-kidneyhepc-'
    img1o=scipy.misc.imread('imgs/Rat_Kidney_HE_Section02-0.png')
    img2o=scipy.misc.imread('imgs/Rat_Kidney_PanCytokeratin_Section03-0.png')
    img1,sf1=reduce_to_size(img1o)
    img2=scipy.misc.imresize(img2o,sf1)
    par=bigtools.Parameters({'kernel_order':1,'supcomp':15,'opacity':0.5,
                             'use_barrier':False, 'beta':0.0,
                             'ninits':10, 'initr':5, 'ncls':4,
                             'lmbd':1e-3, 'lmbd_mi':1e-3, 'mindist':20,
                             'numiter':10, 'wsize':10, 'maxdispl':3, 'alpha':10.0,
           'mlevels':4,'nproc':2,'show_edges':True})
    plt.figure() ; segreg.show_combined_images(img1,img2) ;
    segreg.savefigure(prefix+'combinedimgs-before.png')
    plt.title('before registration')
    t=TwoImages(img1,img2,par)
    fgs=t.show_superpixels()
    for i in xrange(2):
      plt.figure(fgs[i])
      segreg.savefigure(prefix+'superpixels%d.png' % i)
    wimg,transf,segms=t.iterate(output_prefix=prefix)
    plt.figure() ; segreg.show_combined_images(img1,wimg) ;
    segreg.savefigure(prefix+'combinedimgs-after.png')
    plt.title('after final registration')
    t.show_segmentation()
    t.show_overlaid_segmentations() ;
    segreg.savefigure(prefix+'combinedsegm-after.png')

def register_small():
    plt.close('all')
    prefix='output/small-'
    img1=scipy.misc.imread('imgs/simple.png')
    img2=scipy.ndimage.rotate(img1,10,reshape=False,order=1,mode='nearest')
    img2=scipy.ndimage.shift(img2,[2,0,0],order=1,mode='nearest')
    par=bigtools.Parameters({'kernel_order':1,'supcomp':15,'opacity':0.5,
                             'use_barrier':False, 'beta':0.0,
                             'ninits':10, 'initr':5, 'ncls':4,
                             'lmbd':1e-3, 'lmbd_mi':1e-3, 'mindist':20,
                             'numiter':10, 'wsize':10, 'maxdispl':3, 'alpha':1.0,
           'mlevels':5,'nproc':4,'show_edges':True})
    plt.figure() ; segreg.show_combined_images(img1,img2) ;
    segreg.savefigure(prefix+'combinedimgs-before.png')
    plt.title('before registration')
    t=TwoImages(img1,img2,bigtools.Parameters(par,override={'do_affine':True}))
    #fgs=t.show_superpixels()
    #pdb.set_trace()
    wimg,transf,segms=t.iterate(visualize=True,output_prefix=prefix)
    plt.figure() ; segreg.show_combined_images(img1,wimg) ;
    segreg.savefigure(prefix+'combinedimgs-after.png')
    t.show_segmentation()
    t.show_overlaid_segmentations() ;
    segreg.savefigure(prefix+'combinedsegm-after.png')
    transf.saveimg(prefix+'deformation.png')
    t2=TwoImages(img1,wimg,par)
    wimg2,transf2,segms2=t2.iterate(visualize=True,output_prefix=prefix)
    plt.figure() ; segreg.show_combined_images(img1,wimg2) ;
    segreg.savefigure(prefix+'combinedimgs-after2.png')
    t2.show_segmentation()
    t2.show_overlaid_segmentations() ;
    segreg.savefigure(prefix+'combinedsegm-after2.png')
    transf2.saveimg(prefix+'deformation2.png')
    total_transf=transf2.compose(transf)
    wimg3=total_transf.warp_image(img2)
    plt.figure() ; segreg.show_combined_images(img1,wimg2) ;
    segreg.savefigure(prefix+'combinedimgs-after3.png')
    #pdb.set_trace()


if __name__ == "__main__":  # running interactively
    # test_superpixel_segmentation()
    #test_close_superpixels()
    #test_classification()
    #test_classify_three_classes()
    #test_two_classifications()
    #test_mi_grad()
    #test_two_images()
    #test_two_images_optimize()
    #test_three_classes_optimize()
    #test_same_image()
    #test_order_two()
    #test_three_classes_randomly()
    #test_graphcut_segmentation()
    #test_optimal_graphcut()
    #test_iterative_segmentation_registration(save=True)
    #register_kidney()
    #register_kidney_manual()
    register_small()
