# compiling with  python setup.py build_ext --inplace

from distutils.core import setup
from Cython.Build import cythonize
#from distutils.extension import Extension

#setup(ext_modules = cythonize(
#           "segregtools.pyx",                 # our Cython source
#           sources=["Rectangle.cpp"],  # additional source file(s)
#           language="c++",             # generate C++ code
#      ))

from distutils.core import setup
from Cython.Build import cythonize

setup(
    name = "segregtools",
    ext_modules = cythonize([ 'segregtools.pyx',
                              'classifier_cython.pyx'])
)
