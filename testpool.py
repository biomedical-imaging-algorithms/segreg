import multiprocessing

class C:

  def f(self,x,q):
    print "x=",x," 2x=",2*x
    q.put(2*x)
    return 2*x

  def run(self):
    pool=multiprocessing.Pool(processes=4)
    m=pool.map(self.f,xrange(10))
    print m

if __name__ == '__main__':
  c=C()
  q=multiprocessing.Queue()
  p1=multiprocessing.Process(target=c.f,args=(10,q,))
  p2=multiprocessing.Process(target=c.f,args=(2,q,))
  p1.start()
  p2.start()
  p1.join()
  p2.join()
  print q.get()
  print q.get()
  
 #c.run()
