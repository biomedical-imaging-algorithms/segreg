function z=krigkernel(t,sigma)
% evaluate a 1D Krigging kernel (covariance) at distances t
% sigma is a spatial extent parameter  
%
% Jan Kybic, September 2012
  
ts=2*sigma*sigma ;  
  
%z=exp(-t.^2/ts) ;
z=1./(sigma+ts) ;