function z=kernelinterp(x,y,x0,sigma) ;
% Given values y of a function at points x, evaluate the value at point
% x0 using kernel interpolation. sigma is the spatial extent, passed to 'kernel'
%
% Jan Kybic, September 2012
  
w=kernel(abs(x-x0),sigma) ;
z=(dot(y,w)+eps)/(sum(w)+eps) ;
  