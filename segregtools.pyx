# Cython module to speedup some operations from segreg.py
#
#
# First a very simple nearest neighbor classifier. Types are a
# problem, we expect uint8
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
# distutils: language = c++

import sys
import numpy as np
cimport numpy as np
cimport cython
from libc.math cimport log
from libc.math cimport ceil

from libcpp.set cimport set
from libcpp.vector cimport vector
#from libcpp.unordered_map cimport unordered_map
from libcpp.map cimport map
from cython.operator cimport dereference as deref , preincrement as inc

from libc.stdlib cimport malloc, free
from segregtools cimport *

DTYPEf = np.float
DTYPEw = np.int
DTYPE = np.uint8
DTYPEs = np.uint16


#def dist(np.ndarray x,np.ndarray y):
#    d=x-y
#    return np.dot(d,d)

cdef inline int dist(DTYPE_t[:] x, DTYPE_t[:] y) nogil:
    cdef unsigned i
    cdef int n=x.shape[0]
    # assert n==y.shape[0]
    cdef int s=0
    cdef int d
    for i in xrange(n):
        d=<int>(x[i])-<int>(y[i])
        s+=d*d
    return s

#@cython.wraparound(False)
cdef inline unsigned nearest_neighbor(DTYPE_t[:] r,DTYPE_t[:,:] etalons) nogil:
        cdef unsigned i=0
        cdef int v=dist(r,etalons[0])
        cdef unsigned j
        cdef int netalons=etalons.shape[0]
        cdef int d
        for j in xrange(1,netalons):
            d=dist(r,etalons[j])
            if d<v:
                v=d ; i=j
        return i


def nearest_neighbors(DTYPE_t[:,:] x,DTYPE_t[:,:] etalons):
    # assert x.dtype == DTYPE and etalons.dtype == DTYPE
    cdef unsigned i
    cdef int n=x.shape[0]
    cdef DTYPE_t[:,:] s=np.zeros((n,1),DTYPE)
    # pdb.set_trace()
    for i in xrange(n):
        s[i]=nearest_neighbor(x[i],etalons)
    return np.asarray(s)

cdef inline int int_max(int a, int b): return a if a >= b else b
cdef inline int int_min(int a, int b): return a if a <= b else b

def class_counts(DTYPE_t[:,:] f,DTYPE_t[:,:] g,
                 int xmin, int xmax, int ymin, int ymax, int dx, int dy,int ncls):
    """ Take region f[xmin:xmax,ymin:max] (upper boundaries non inclusive) and
                    g[xmin+dx:xmax+dx,ymin+dy:ymax+dy] containing values 0..ncls-1
        and return a matrix of size  ncls*ncls containing counts of coocurrences
        of class combinations in both images. Out of bounds values are ignored (not included in the count) """
    cdef int i,j
    cdef int[:,:] c=np.zeros((ncls,ncls),dtype='i4')
    cdef int nfx=f.shape[0]
    cdef int nfy=f.shape[1]
    cdef int ngx=g.shape[0]
    cdef int ngy=g.shape[1]
    cdef int cf,cg

    xmin=int_max(int_max(xmin,0),-dx)
    xmax=int_min(int_min(xmax,nfx),ngx-dx)
    ymin=int_max(int_max(ymin,0),-dy)
    ymax=int_min(int_min(ymax,nfy),ngy-dy)

    for i in xrange(xmin,xmax):
        for j in xrange(ymin,ymax):
            cf=f[i,j]
            cg=g[i+dx,j+dy]
            if cf==255 or cg==255: # uint8(-1)
              continue
            if cf>=ncls or cg>=ncls:
                raise ValueError("class_counts:Class number exceeds ncls")
            c[cf,cg]+=1

    return np.asarray(c)

cdef DTYPEf_t minv(DTYPEf_t[:] v) nogil:
  """ Find a minimum of a vector """
  cdef int i
  cdef DTYPEf_t z
  cdef DTYPEf_t r=1e308 # big number
  for i in xrange(v.shape[0]):
    z=v[i]
    if z<r: r=z
  return r

cdef void q_envelope_out(DTYPEf_t c, DTYPEf_t[:] h,DTYPEf_t[:] D) nogil:
    """ Given c and h[0..n-1], calculate for each q in [0..n-1],
        min_p (c (q-p)**2 + h[p] ) using the linear F&H algorithm """
    cdef int n=h.shape[0]
    cdef int q
    cdef int vj=0 # to make the Cython compiler happy
    cdef DTYPEf_t s,den
    cdef int j=0

    # treat the special case
    if c==0.:
      D[:]=minv(h)
      return

    #cdef int *v=<int *>malloc(n*sizeof(int))
    #cdef DTYPEf_t *z=<DTYPEf_t *>malloc((n+1)*sizeof(DTYPEf_t))


    cdef DTYPEf_t eps=1e-6
    cdef DTYPEf_t inf=1e308
    #cdef int[:] v=np.zeros(n,'i4')
    #cdef vector[int] vv=
    #vv.resize(n)
    cdef int *v=<int *>malloc(n*sizeof(int))
    cdef double *z=<double *>malloc((n+1)*sizeof(double))
    #cdef int[:] v= vv
    v[0]=0
    #cdef double[:] z=np.zeros(n+1)

    #    try:
    z[0]=-inf ; z[1]=inf ;
    for q in xrange(1,n):
            while True:
                vj=v[j]
                den=2*c*(q-vj)
                if den<>0.:
                    s= ((h[q]+c*q*q)-(h[vj]+c*vj*vj))/den
                else:
                    s=inf
                if s<=z[j] and j>0:
                    j-=1
                else:
                    j+=1
                    v[j]=q
                    z[j]=s
                    z[j+1]=inf
                    break
    j=0
    for q in xrange(0,n):
            while z[j+1]-eps<q:
                j+=1
            D[q]=c*(q-v[j])**2+h[v[j]]
            #finally:
    free(v)
    free(z)
    #del v


cdef void q_envelope2D_out(DTYPEf_t c,DTYPEf_t[:,:] h, DTYPEf_t[:,:] D) nogil:
    """ Apply q_envelope in both directions """
    cdef int n=h.shape[0]
    cdef int m=h.shape[1]
    cdef int i,j
    cdef DTYPEf_t[:] e
    cdef DTYPEf_t[:] f
    with gil:
      e=np.zeros(n)
      f=np.zeros(n)
    for i in xrange(n):
        q_envelope_out(c,h[i],D[i,:])
    for i in xrange(m):
        for j in xrange(n):
          e[j]=D[j,i]
        q_envelope_out(c,e,f)
        for j in xrange(n):
          D[j,i]=f[j]

def q_envelope2D(c,r):
  rout=np.zeros(r.shape)
  q_envelope2D_out(c,r,rout)
  return rout


# cdef double inf=float('inf')

# cpdef inline DTYPEf_t[:] q_envelope(double c, DTYPEf_t[:] h):
#     """ Given c and h[0..n-1], calculate for each q in [0..n-1],
#         min_p (c (q-p)**2 + h[p] ) using the linear F&H algorithm """
#     cdef int n=h.shape[0]
#     cdef DTYPEf_t[:] D=np.zeros(n)
#     # treat the special case
#     if c==0.:
#         D[:]=np.min(h)
#         return D
#     cdef int j=0
#     cdef int[:] v=np.zeros(n,'i4')
#     cdef double[:] z=np.zeros(n+1)
#     z[0]=-inf ; z[1]=inf ;
#     cdef int q,vj
#     cdef double s
#     for q in xrange(1,n):
#         while True:
#             vj=v[j]
#             den=2*c*(q-vj)
#             if den<>0.:
#                 s= ((h[q]+c*q*q)-(h[vj]+c*vj*vj))/den
#             else:
#                 s=inf
#             if s<=z[j] and j>0:
#                 j-=1
#             else:
#                 j+=1
#                 v[j]=q
#                 z[j]=s
#                 z[j+1]=inf
#                 break
#     j=0
#     cdef double eps=1e-6
#     for q in xrange(0,n):
#         while z[j+1]-eps<q:
#             j+=1
#         D[q]=c*(q-v[j])**2+h[v[j]]
#     return D

# def q_envelope2D(double c,DTYPEf_t[:,:] h):
#     """ Apply q_envelope in both directions """
#     cdef int n=h.shape[0]
#     cdef int m=h.shape[1]
#     cdef DTYPEf_t[:,:] D=np.zeros((n,m))
#     cdef int i
#     for i in xrange(n):
#         D[i,:]=q_envelope(c,h[i])
#     for i in xrange(m):
#         D[:,i]=q_envelope(c,D[:,i])
#     return np.asarray(D)

def thin_plate_spline_matrix(DTYPEf_t[:,:] pts):
    """ Create a thin plate spline matrix """
    cdef int n=pts.shape[0]
    assert pts.shape[1]==2
    cdef DTYPEf_t[:,:] a=np.zeros((n+3,n+3))
    cdef int i,j
    cdef double dx,dy,r2,v
    cdef double eps=1e-6

    for i in xrange(n):
        for j in xrange(i+1,n):
            dx=pts[i][0]-pts[j][0]
            dy=pts[i][1]-pts[j][1]
            r2=dx*dx+dy*dy
            v=0.5*r2*log(r2+eps)
            a[i,j]=v ; a[j,i]=v
        a[i,n]=1 ; a[i,n+1]=pts[i,0] ; a[i,n+2]=pts[i,1]
        a[n,i]=1 ; a[n+1,i]=pts[i,0] ; a[n+2,i]=pts[i,1]
    return np.asarray(a)

cpdef interpolate_tps_one(DTYPEf_t[:] coefs, DTYPEf_t[:,:] pts,x,y):
    """ Interpolate one point by TPS """
    cdef int n=pts.shape[0]
    assert pts.shape[1]==2

    cdef int i,j
    cdef double dx,dy,r2,v,res
    cdef double eps=1e-6

    res=coefs[n]+coefs[n+1]*x+coefs[n+2]*y
    for i in xrange(n):
        dx=pts[i][0]-x
        dy=pts[i][1]-y
        r2=dx*dx+dy*dy
        v=0.5*r2*log(r2+eps)
        res+=v*coefs[i]
    return res

def tps_interpolate_image(DTYPEf_t[:] coefsx, DTYPEf_t[:] coefsy, DTYPEf_t[:,:] pts,int nx,int ny,DTYPE_t[:,:] img):
    """ Interpolate image using TPS interpolation. Nearest neighbor interpolation for the moment """
    cdef DTYPE_t[:,:] imgout=np.zeros((nx,ny),'u1')
    cdef int i,x,y
    cdef double dx,dy,v,rx,ry
    cdef double eps=1e-6
    cdef int n=pts.shape[0]
    cdef int ix,iy
    assert pts.shape[1]==2

    for x in xrange(nx):
        for y in xrange(ny):
            rx=coefsx[n]+coefsx[n+1]*x+coefsx[n+2]*y
            ry=coefsy[n]+coefsy[n+1]*x+coefsy[n+2]*y
            for i in xrange(n):
                dx=pts[i][0]-x
                dy=pts[i][1]-y
                v=dx*dx+dy*dy
                v=0.5*v*log(v+eps)
                rx+=v*coefsx[i]
                ry+=v*coefsy[i]
# Nearest neighbor interpolation
#            ix=<int>(rx+0.5)
#            iy=<int>(ry+0.5)
#            if ix>=0 and ix<nx and iy>=0 and iy<ny:
#                imgout[x,y]=img[ix,iy]

# Linear interpolation
            if rx>0. and rx<nx-1 and rx>0. and ry<ny-1:
                ix=<int>(rx) # floor
                iy=<int>(ry) # floor
                alpha=rx-ix ; beta=ry-iy
                p1 = img[ix,iy]*(1-beta)+img[ix,iy+1]*beta
                p2 = img[ix+1,iy]*(1-beta)+img[ix+1,iy+1]*beta
                imgout[x,y]=<int>(p1*(1-alpha)+p2*alpha)
    return np.asarray(imgout)

def segmentation_to_planes(DTYPE_t[:,:] s,int ncls):
    """ Given a segmentation with classes between 0 and ncls-1,
        create a binary images (encoded as uint8) with ncls layers """
    cdef int nx=s.shape[0]
    cdef int ny=s.shape[1]
    cdef DTYPEs_t[:,:,:] out=np.zeros((nx,ny,ncls),DTYPEs)
    cdef int ix,iy
    cdef int c

    for ix in xrange(nx):
        for iy in xrange(ny):
            c=s[ix,iy]
            if c==255: # uint8(-1)
              continue
            assert c>=0 and c<ncls
            out[ix,iy,c]=1
    return np.asarray(out)

def reduce_planes(DTYPEs_t[:,:,:] s):
    """ Given a 'plane' image produced by e.g. segmentation_to_planes,
        create a twice smaller image with sum of counts in the lower level.
        Odd last column and row are ignored. """
    cdef int nx=s.shape[0]
    cdef int ny=s.shape[1]
    cdef int nc=s.shape[2]
    cdef int nxn = (nx+1) / 2 # round up
    cdef int nyn = (ny+1) / 2
    cdef int ix,iy,ic

    cdef DTYPEs_t[:,:,:] out=np.zeros((nxn,nyn,nc),DTYPEs)

    for ix in xrange(nxn):
        for iy in xrange(nyn):
            for ic in xrange(nc):
                out[ix,iy,ic]=s[2*ix,2*iy,ic]
                if 2*iy+1<ny:
                    out[ix,iy,ic]+=s[2*ix,2*iy+1,ic]
                if 2*ix+1<nx:
                    out[ix,iy,ic]+=s[2*ix+1,2*iy+1,ic]
                    if 2*iy+1<ny:
                        out[ix,iy,ic]+=s[2*ix+1,2*iy,ic]
    return np.asarray(out)

def class_counts_from_planes(DTYPEs_t[:,:,:] f,DTYPEs_t[:,:,:] g,
                 int xmin, int xmax, int ymin, int ymax, int dx, int dy):
    """ Take region f[xmin:xmax,ymin:max,:] (upper boundaries non inclusive) and
                    g[xmin+dx:xmax+dx,ymin+dy:ymax+dy,:] of ncls 'planes'
        and return a matrix of size  ncls*ncls containing counts of coocurrences
        of class combinations in both images. Boundary values are repeated for
        out-of-bounds positions """
    cdef int i,j,k,ii,jj,gii,gij
    cdef int nfx=f.shape[0]
    cdef int nfy=f.shape[1]
    cdef int ncls=f.shape[2]
    cdef int ngx=g.shape[0]
    cdef int ngy=g.shape[1]
    assert g.shape[2]==ncls
    cdef DTYPEs_t[:] cf,cg
    cdef DTYPEw_t[:,:] c=np.zeros((ncls,ncls),dtype=DTYPEw)

    #xmin=int_max(int_max(xmin,0),-dx)
    #xmax=int_min(int_min(xmax,nfx),ngx-dx)
    #ymin=int_max(int_max(ymin,0),-dy)
    #ymax=int_min(int_min(ymax,nfy),ngy-dy)

    for i in xrange(xmin,xmax+1):
        for j in xrange(ymin,ymax+1):
            ii=int_max(int_min(nfx-1,i),0)
            jj=int_max(int_min(nfy-1,j),0)
            gii=int_max(int_min(ngx-1,i+dx),0)
            gjj=int_max(int_min(ngy-1,j+dy),0)
            cf=f[ii,jj]
            cg=g[gii,gjj]
            for k in xrange(ncls):
                for l in xrange(ncls):
                    c[k,l]+=cf[k]*cg[l]

    return np.asarray(c)


cpdef inline int kernelize_size(int n,int order) nogil:

  if order==1:
      return n
  elif order==2:
      return n*(n+1)/2
  elif order==3:
      return n*(n+1)*(n+2)/6
  else:
      return -1

cdef inline void fast_kernelize(DTYPEf_t[:] v,int order,DTYPEf_t[:] y) nogil:
  """ Like kernelize but stores the result into an output y to avoid allocation """
  cdef int n = v.shape[0]
  cdef int i,j,k,l,m

  if order==1:
      for i in xrange(n):
          y[i]=v[i]
  elif order==2:
      m=n*(n+1)/2
      l=0 ;
      for i in xrange(n):
          for j in xrange(i,n):
              y[l]=v[i]*v[j]
              l+=1
      # assert (l==m)
  elif order==3:
      m=n*(n+1)*(n+2)/6
      l=0 ;
      for i in xrange(n):
          for j in xrange(i,n):
              for k in xrange(j,n):
                  y[l]=v[i]*v[j]*v[k]
                  l+=1



cpdef kernelize(DTYPEf_t[:] v,int order):
  """ Given a vector, apply a polynomial "kernel" of a given order to it.
      We assume that v[0]=1. If order=1, the vector returned as is, not copied.
  """
  assert v[0]==1.0

  cdef DTYPEf_t[:] y
  cdef int n = v.shape[0]
  cdef int i,j,k,l,m

  if order==1:
      return v
  elif order==2:
      m=n*(n+1)/2
      y=np.zeros(m,dtype=DTYPEf)
      # print "m=",m, "n=",n
      l=0 ;
      for i in xrange(n):
          for j in xrange(i,n):
              y[l]=v[i]*v[j]
              l+=1
      assert (l==m)
      return np.asarray(y)
  elif order==3:
      m=n*(n+1)*(n+2)/6
      y=np.zeros(m,dtype=DTYPEf)
      l=0 ;
      for i in xrange(n):
          for j in xrange(i,n):
              for k in xrange(j,n):
                  y[l]=v[i]*v[j]*v[k]
                  l+=1
      assert (l==m)
      return np.asarray(y)
  else:
      raise ValueError("kernelize: Unsupported order")

def mean_over_superpixels(int[:,:] labels,DTYPEf_t[:,:,:] fp,int nsuper):
  """ Each superpixel is assigned a mean of the features calculated for its pixels. An element is added in front and set to 1."""

  cdef int m=labels.shape[0]
  cdef int n=labels.shape[1]
  assert nsuper>0
  cdef int nf=fp.shape[2]
  cdef DTYPEw_t[:] c=np.zeros((nsuper,),dtype=DTYPEw)
  cdef DTYPEf_t[:,:] features=np.zeros((nsuper,nf+1))
  cdef int i,j,k,l

  assert fp.shape[0]==m
  assert fp.shape[1]==n

  for i in xrange(nsuper):
      features[i,0]=1

  for i in xrange(m):
      for j in xrange(n):
          k=labels[i,j]
          assert k>=0 and k<nsuper
          for l in xrange(nf):
              features[k,l+1]+=fp[i,j,l]
          c[k]+=1

  for i in xrange(nsuper):
      for l in xrange(nf):
          features[i,l+1]/=<float>c[i]

  return np.asarray(features)

def superpixel_sizes(int[:,:] labels,int nsuper):
  """ Count the size of each superpixel in pixels. """

  cdef int m=labels.shape[0]
  cdef int n=labels.shape[1]
  assert nsuper>0
  cdef DTYPEw_t[:] c=np.zeros((nsuper,),dtype=DTYPEw)

  cdef int i,j,k

  for i in xrange(m):
      for j in xrange(n):
          k=labels[i,j]
          assert k>=0 and k<nsuper
          c[k]+=1

  return np.asarray(c)


def distribute_to_pixels(DTYPEf_t[:,:] features,int[:,:] labels, int l):
  """ Each pixel is assigned a value of the corresponding superpixel feature number l """

  cdef int m=labels.shape[0]
  cdef int n=labels.shape[1]
  cdef int nsuper=features.shape[0]
  cdef DTYPEf_t[:,:] fp=np.zeros((m,n))
  cdef int i,j,k

  for i in xrange(m):
      for j in xrange(n):
          k=labels[i,j]
          assert k>=0 and k<nsuper
          fp[i,j]=features[k,l+1]

  return np.asarray(fp)




def distribute_to_pixels_vector(DTYPEf_t[:] features,int[:,:] labels):
  """ Each pixel is assigned a value of the corresponding superpixel feature """

  cdef int m=labels.shape[0]
  cdef int n=labels.shape[1]
  cdef int nsuper=features.shape[0]
  cdef DTYPEf_t[:,:] fp=np.zeros((m,n))
  cdef int i,j,k

  for i in xrange(m):
      for j in xrange(n):
          k=labels[i,j]
          assert k>=0 and k<nsuper
          fp[i,j]=features[k]

  return np.asarray(fp)

def distribute_labels_to_pixels(int[:] classes,int[:,:] labels):
  """ Each pixel is assigned a value of the corresponding superpixel feature """

  cdef int m=labels.shape[0]
  cdef int n=labels.shape[1]
  cdef int nsuper=classes.shape[0]
  cdef int[:,:] o=np.zeros((m,n),np.int32)
  cdef int i,j,k

  for i in xrange(m):
      for j in xrange(n):
          k=labels[i,j]
          if k==-1:
            o[i,j]=-1
          else:
            assert k>=0 and k<nsuper
            o[i,j]=classes[k]

  return np.asarray(o)



def close_superpixels(int[:,:] labels, int x, int y, int r):
    """ Return an array of superpixel labels closer to point (x,y) then r"""
    cdef set[int] s
    cdef int i,j
    cdef int nx=labels.shape[0]
    cdef int ny=labels.shape[1]

    cdef int i0= int_max ( 0, x-r )
    cdef int i1= int_min ( nx-1, x+r )
    cdef int j0 = int_max ( 0,y-r)
    cdef int j1 = int_min ( ny-1 , y+r)
    cdef int dx,dy
    cdef int r2=r*r

    for i in xrange(i0,i1+1):
        for j in xrange(j0,j1+1):
            dx=i-x
            dy=j-y
            if dx*dx+dy*dy<r2:
                s.insert(labels[i,j])

    return s

def overlay_classes(int[:] y1,int[:] y2,int[:,:] l1,int[:,:] l2,
                    DTYPEw_t[:] nsize,DTYPEw_t[:] shift1,DTYPEw_t[:] shift2,
                    DTYPE_t[:,:] classes):
    cdef DTYPE_t[:,:,:] o=np.zeros((nsize[0],nsize[1],3),dtype=DTYPE)

    cdef int ncls=classes.shape[0]
    cdef int i,j,l,c1,c2,i1,j1,i2,j2
    cdef int nl1=y1.shape[0]
    cdef int nl2=y2.shape[0]
    cdef int ll1,ll2
    cdef int nx1=l1.shape[0]
    cdef int ny1=l1.shape[1]
    cdef int nx2=l2.shape[0]
    cdef int ny2=l2.shape[1]


    for i in xrange(nsize[0]):
        for j in xrange(nsize[1]):
            i1=i-shift1[0] ; j1=j-shift1[1] ;
            if i1>=0 and i1<nx1 and j1>=0 and j1<ny1:
                ll1=l1[i1,j1]
                if ll1==-1:
                  c1=0
                else:
                  assert ll1>=0 and ll1<nl1
                  c1=y1[ll1]
                assert c1>=0 and c1<ncls
                for l in xrange(3):
                    o[i,j,l]=<DTYPE_t>((<int>classes[c1,l])/2)
            i2=i-shift2[0] ; j2=j-shift2[1] ;
            if i2>=0 and i2<nx2 and j2>=0 and j2<ny2:
                ll2=l2[i2,j2]
                if ll2==-1:
                  c2=0
                else:
                  assert ll2>=0 and ll2<nl2
                  c2=y2[ll2]
                assert c2>=0 and c2<ncls
                for l in xrange(3):
                    o[i,j,l]+=<DTYPE_t>((<int>classes[c2,l])/2)
    return o

def overlay_labels_over_image(DTYPE_t[:,:] img,int[:] lblr,int[:] l,DTYPE_t[:,:] cols,
                              float opacity):
    """ helper for show_classes_labels. img is flattened and will be overwritten """
    assert img.shape[1]==3 and cols.shape[1]==3
    cdef int ncols=cols.shape[0]
    cdef int npix=img.shape[0]
    assert lblr.shape[0]==npix
    cdef int i,li,q
    cdef DTYPE_t[:] col
    cdef int nsuper=l.shape[0]

    for i in xrange(npix):
        li=lblr[i]
        assert li<nsuper
        li=l[li]
        if li>=0:
            assert li<ncols
            col=cols[li]
            for q in xrange(3):
                img[i,q]=<DTYPE_t>(col[q]*opacity+img[i,q]*(1.-opacity))


def superpixel_overlaps(int[:,:] sl1, int[:,:] sl2, DTYPEw_t[:] nsize, DTYPEw_t[:] shift1, DTYPEw_t[:] shift2):
    """ Return a nx3 array of overlapping superpixels, each row will contain indices of superpixels in both images and the number of common pixels """

    cdef int i,j
    cdef int nx1=sl1.shape[0]
    cdef int ny1=sl1.shape[1]
    cdef int nx2=sl2.shape[0]
    cdef int ny2=sl2.shape[1]

    #    cdef unordered_map[int,int] h # hashtable
    cdef map[int,int] h # hashtable
    cdef int maxlb= 1 << 16
    cdef int ll1,ll2,c,i1,j1,i2,j2
    cdef int[:,:] o

    # it should be possible to iterate over a smaller range but I do not think it
    # makes any difference
    for i in xrange(nsize[0]):
        for j in xrange(nsize[1]):
            i1=i-shift1[0] ; j1=j-shift1[1] ;
            if i1>=0 and i1<nx1 and j1>=0 and j1<ny1:
                ll1=sl1[i1,j1]
                if ll1==-1:
                  continue
                assert ll1<maxlb and ll1>=0
                i2=i-shift2[0] ; j2=j-shift2[1] ;

                if i2>=0 and i2<nx2 and j2>=0 and j2<ny2:
                    ll2=sl2[i2,j2]
                    if ll2==-1:
                      continue
                    assert ll2<maxlb and ll2>=0
                    c=(ll1 << 16 ) | ll2
                    h[c]+=1

    cdef int numel=h.size()
    o=np.zeros((numel,3),dtype='i4')

    cdef map[int,int].iterator it = h.begin()
    i=0
    cdef int mask = (1<<16)-1
    cdef int val

    while it!=h.end():
         val=deref(it).first
         o[i,0]=val >> 16
         o[i,1]=val & mask
         o[i,2]=deref(it).second
         i+=1
         inc(it)

    return np.asarray(o)

def assemble_probability_matrix(int ncls,int[:,:] overlaps,
                                DTYPEf_t[:,:] z0,DTYPEf_t[:,:] z1):
    """ helper function for TwoImages:mutual_inf """
    cdef DTYPEf_t[:,:] P=np.zeros((ncls,ncls))
    # it is faster to normalize after
    cdef int totpix=0
    cdef int i,j,k,q0,q1,q2
    cdef int n=overlaps.shape[0]
    cdef int[:] q
    cdef int fdim=z0.shape[1]

    for i in xrange(n):
        q=overlaps[i]
        q0=q[0] ; q1=q[1] ; q2=q[2]
        for j in xrange(fdim):
            for k in xrange(fdim):
                P[j,k]+=q2*z0[q0,j]*z1[q1,k]
        totpix+=q2

    P=np.divide(P,<DTYPEf_t>totpix)

    return totpix,np.asarray(P)

def assemble_probability_matrix_from_labels(int ncls,int[:,:] overlaps,
                                int[:] l0,int[:] l1):
    """ helper function for TwoImages:optimal_graphcut_segmentation """
    cdef DTYPEf_t[:,:] P=np.zeros((ncls,ncls))
    # it is faster to normalize after
    cdef int totpix=0
    cdef int i,q0,q1,q2,ll0,ll1
    cdef int n=overlaps.shape[0]
    cdef int[:] q

    for i in xrange(n):
        q=overlaps[i]
        q0=q[0] ; q1=q[1] ; q2=q[2]
        ll0=l0[q0] ; ll1=l1[q1] ;
        if ll0==-1 or ll1==-1:
          continue
        assert ll0>=0 and ll1>=0 and ll0<ncls and ll1<ncls
        P[ll0,ll1]+=q2
        totpix+=q2

    P=np.divide(P,<DTYPEf_t>totpix)

    return totpix,np.asarray(P)


# cpdef zderiv(DTYPEf_t[:]z,DTYPEf_t[:]f,int k,int l,int kernel_order):
#    """ Calculates d z_{ik}/d a_l """
#    cdef DTYPEf_t[:] v=kernelize(f,kernel_order)
#    #print "after kernelize  ",np.asarray(f)
#    #sys.stdout.flush()
#    cdef double w=z[k]*((k==l)-z[l])
#    cdef int j
#    for j in xrange(v.shape[0]):
#        v[j]*=w
#     #print "after for loop  ",np.asarray(f)
#     #sys.stdout.flush()
#    return v


def mutual_inf_gradient(int ncls,int[:,:] overlaps,DTYPEf_t[:,:] z0,
                        DTYPEf_t[:,:] z1,
                        DTYPEf_t[:,:] f0,DTYPEf_t[:,:] f1,
                        DTYPEf_t[:,:] P,DTYPEf_t[:,:] gradP,
                        DTYPEf_t[:,:,:] grad,double totpix,int kernel_order):
    """ Helper function for TwoImages.mutual_inf. grad is modified in place """
    cdef int n=overlaps.shape[0]
    cdef int[:] q
    cdef double w,ww
    cdef int i,k,l,kk,ll,j,q2
    cdef int fdim=f0.shape[1]
    cdef DTYPEf_t[:] v0=np.zeros((kernelize_size(
                          fdim,kernel_order),))
    cdef DTYPEf_t[:] v1=np.zeros((kernelize_size(
                          fdim,kernel_order),))
    cdef DTYPEf_t[:] z0q0,z1q1

    # calculate the maximum probability for each feature vector
    # alpha=0.1 # how many to consider
    # cdef DTYPEf_t[:] mp0=np.max(z0,1)
    # mp=np.asarray(mp0).copy() ; mp.sort()
    # cdef DTYPEf_t thr0=mp[ceil(0.1*z0.shape[0])]
    # cdef DTYPEf_t[:] mp1=np.max(z1,1)
    # mp=np.asarray(mp1).copy() ; mp.sort()
    # cdef DTYPEf_t thr1=mp[ceil(0.1*z1.shape[0])]
    # del mp

    with nogil:
        for i in xrange(n):
          #print "i=",i," ",np.asarray(f0)[0],np.asarray(f1)[0]
          #sys.stdout.flush()
            q=overlaps[i]
          # if  mp0[q[0]]<thr0 and mp1[q[1]]<thr1:
            fast_kernelize(f0[q[0]],kernel_order,v0)
            fast_kernelize(f1[q[1]],kernel_order,v1)
            z0q0=z0[q[0]]
            z1q1=z1[q[1]]
            q2=q[2]
            for k in xrange(ncls):
                for l in xrange(ncls):
                    # for the first classifier parameters
                    w=q2*gradP[k,l]*z1q1[l]/totpix
                    for kk in xrange(ncls):
                        ww=w*z0q0[k]*((k==kk)-z0q0[kk])
                        for j in xrange(fdim):
                            grad[0,kk,j]+=ww*v0[j]
                    # for the second classifier parameters
                    w=q[2]*gradP[k,l]*z0q0[k]/totpix
                    for ll in xrange(ncls):
                        ww=w*z1q1[l]*((l==ll)-z1q1[ll])
                        for j in xrange(fdim):
                            grad[1,ll,j]+=ww*v1[j]


def get_edges(int[:,:] sl):
    """ Return an array n x 3, rows containing (i,j,c), where i and j
    are label of neighboring (8-neighborhood) superpixels and c is the number of
    such pairs. Only output pairs i>j (indices swapped otherwise)  """

    cdef int i,j,k,ii,jj,c
    cdef int nx=sl.shape[0]
    cdef int ny=sl.shape[1]
    cdef map[int,int] h # hashtable
    cdef int maxlb= 1 << 16
    cdef int ll1,ll2,ll3
    cdef int[:,:] o

    cdef int[:,:] shifts=np.array(((1,0),(0,1),(1,1)),dtype=np.int32)
    cdef int nshifts=shifts.shape[0]

    for i in xrange(nx):
        for j in xrange(ny):
            ll1=sl[i,j] # label at point (i,j). Now consider neighbors
            assert ll1<maxlb
            for k in xrange(nshifts):
                ii=i+shifts[k,0]
                jj=j+shifts[k,1]
                if ii<nx and jj<ny:
                  ll2=sl[ii,jj]
                  if ll1<>ll2:
                    if ll1<ll2:
                      ll3=ll1 ; ll1=ll2 ; ll2=ll3
                    c=(ll1 << 16) | ll2
                    h[c]+=1

    # convert to array
    cdef int numel=h.size()
    o=np.zeros((numel,3),dtype=np.int32)

    cdef map[int,int].iterator it = h.begin()
    i=0
    cdef int mask = (1<<16)-1
    cdef int val

    while it!=h.end():
         val=deref(it).first
         o[i,0]=val >> 16
         o[i,1]=val & mask
         o[i,2]=deref(it).second
         i+=1
         inc(it)

    return np.asarray(o)


def find_boundary_points(int[:,:] sl,int[:] l):
    """Given superpixel labels sl (coordinate->superpixel) and a segmentation

    (superpixel->class), return a n x 2 array of points containing
    coordinates of boundary points, i.e. points where at least three
    superpixels and two classes coincide
    """
    cdef int nx=sl.shape[0]
    cdef int ny=sl.shape[1]

    cdef int i,j,ll1,ll2,ll3,ll4,l1,l2,l3,l4
    cdef int[:,:] o # output
    cdef vector[int] v

    with nogil:
      for i in xrange(nx-1):
        for j in xrange(ny-1):
          ll1=sl[i,j] ; ll2=sl[i,j+1] ; ll3=sl[i+1,j] ; ll4=sl[i+1,j+1]
          if (ll1<>ll2)+(ll2<>ll3)+(ll1<>ll3)+(ll1<>ll4)+(ll2<>ll4)+(ll3<>ll4)>=5:
            # print "l1=",l1," l2=",l2," l3=",l3, " l4=",l4
            l1=l[ll1] ; l2=l[ll2] ; l3=l[ll3] ; l4=l[ll4]
            if (l1<>l2)+(l2<>l3)+(l1<>l3)+(l1<>l4)+(l2<>l4)+(l3<>l4)>=1:
              v.push_back(i)
              v.push_back(j)

    assert v.size()%2==0
    #print "size=",v.size()
    o=np.empty((v.size()/2,2),dtype=np.int32)
    for i in xrange(o.shape[0]):
      o[i,0]=v[2*i]
      o[i,1]=v[2*i+1]

    return np.asarray(o)
