# Experiments with registration accuracy measured on landmarks
# for IPMI2013 article

import matplotlib
# for non-interactive use
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import os
import os.path
import numpy as np
import scipy.misc
import string
import re
import time
import pdb
import math

import simseg
import segreg
import bigtools

prefix=os.path.expanduser('~/data/Medical/')
outprefix='output/le-'

def read_landmarks(lndn):
  print "Opening landmark file ", lndn
  f=open(lndn)
  x=string.strip(f.readline())
  assert x=="point"
  n=int(string.strip(f.readline()))
  a=np.zeros((n,2))
  for i in xrange(n):
    yx=map(int,re.split(' ',f.readline(),1))
    a[i]=yx[1::-1]
  return a

def read_landmarks_csv(lndn):
  print "Opening landmark file ", lndn
  f=open(lndn)
  f.readline() # skip first line
  lst=[]
  for l in f.readlines():
      yx=map(float,re.split(',',l,2))
      lst.append((yx[2],yx[1]))
  a=np.array(lst)
  return a


def show_landmarks(img,lnd,col='r'):
  plt.clf() ; plt.imshow(img)
  plt.plot(lnd[:,1],lnd[:,0],'o'+col,markersize=5)

def show_landmark_pairs(img1,img2,lnd1,lnd2):
  if img1 is not None:
      nsize=np.max(np.vstack((img1.shape,img2.shape)),0)
      shift=[ (nsize-i.shape)/2 for i in [img1,img2] ]
      img1w=np.zeros(nsize,dtype=img1.dtype)
      img2w=np.zeros(nsize,dtype=img1.dtype)
      img1w[shift[0][0]:shift[0][0]+img1.shape[0],shift[0][1]:shift[0][1]+img1.shape[1]]=img1
      img2w[shift[1][0]:shift[1][0]+img2.shape[0],shift[1][1]:shift[1][1]+img2.shape[1]]=img2
      img1=img1w ; img2=img2w
      lnd1=lnd1+shift[0][:2]
      lnd2=lnd2+shift[1][:2]
      plt.imshow(img1/2+img2/2)
  else:
    plt.imshow(img2)
  plt.plot(np.vstack((lnd1[:,1],lnd2[:,1])),np.vstack((lnd1[:,0],lnd2[:,0])),
           '->r',linewidth=3)
  plt.plot(lnd1[:,1],lnd1[:,0],'or',markersize=5,linewidth=3)
  plt.plot(lnd2[:,1],lnd2[:,0],'og',markersize=5,linewidth=3)


def landmark_stats(lnd1,lnd2):
  n=lnd1.shape[0]
  assert (lnd2.shape[0]==n)
  r=np.zeros(n)
  for i in xrange(n):
    r[i]=math.sqrt(segreg.dist(lnd1[i],lnd2[i]))
  mean=np.mean(r)
  std=np.std(r)
  median=np.median(r)
  worst=np.max(r)
  return mean, std, median, worst

def transform_landmarks(transf,lnd):
  xy=np.zeros(lnd.shape)
  for i in xrange(2):
    #pdb.set_trace()
    xy[:,i]=scipy.ndimage.interpolation.map_coordinates(transf.newcoords[i],lnd.T,order=1,mode='nearest')
  return xy

def one_experiment(img1n,img2n,lnd1n,lnd2n,outf,pref,saveimgs=False
):
  plt.close('all')
  lnd1=read_landmarks(lnd1n)
  lnd2=read_landmarks(lnd2n)
  assert lnd1.shape[0]==lnd2.shape[0]
  print "### Starting experiment ", img1n, " ", img2n, " ", img1n, " ", img2n
  img1=scipy.misc.imread(img1n)
  print "Read image ", img1n, " of size ", img1.shape
  img2=scipy.misc.imread(img2n)
  print "Read image ", img2n, " of size ", img2.shape
  def diag(i):
    return math.sqrt(i.shape[0]**2+i.shape[1]**2)
  mdiag=max([ diag(i) for  i in [img1,img2]])
  if saveimgs:
    plt.figure(1)
    plt.clf()
    show_landmarks(img1,lnd1)
    #plt.imshow(img1)
    segreg.savefigure(pref+'img1.png')
    plt.title('img1')
    plt.figure(2)
    plt.clf()
    show_landmarks(img2,lnd2,col='g')
    #plt.imshow(img2)
    segreg.savefigure(pref+'img2.png')
    plt.title('img2')
    plt.show(block=False)
    plt.figure(3) ; # segreg.show_combined_images(img1,img2) ;
    #segreg.savefigure(pref+'combinedimgs-before.png')
    #plt.title('combined imgs before')
    #plt.show(block=False)
    show_landmark_pairs(img1,img2,lnd1,lnd2)
    segreg.savefigure(pref+'combinedimgs-before.png')
    plt.title('combined imgs before')
    plt.show(block=False)
  par=bigtools.Parameters({'kernel_order':1,'supsize':1000,'supcomp':20,'opacity':0.5,
                             'use_barrier':False, 'beta':0.0,
                              'ninits':10, 'initr':5, 'ncls':4,
                             'lmbd':1e-3, 'lmbd_mi':1e-3, 'mindist':40,
                               'numiter':2, 'wsize':10, 'maxdispl':6, 'alpha':10.,
           'mlevels':1,'nproc':1,'abstol':1e-3,'reltol':1e-3,'show_edges':False,
           'warpinterporder':2})
  # The following was used for the ISBI experiment
  # par=bigtools.Parameters({'kernel_order':1,'supsize':1000,'supcomp':20,'opacity':0.5,
  #                            'use_barrier':False, 'beta':0.0,
  #                             'ninits':10, 'initr':5, 'ncls':4,
  #                            'lmbd':1e-3, 'lmbd_mi':1e-3, 'mindist':40,
  #                              'numiter':2, 'wsize':10, 'maxdispl':6, 'alpha':10.,
  #          'mlevels':1,'nproc':2,'abstol':1e-3,'reltol':1e-3,'show_edges':True,
  #          'warpinterporder':2})
  # par=bigtools.Parameters({'kernel_order':1,'supcomp':20,'opacity':0.5,
  #                            'use_barrier':False, 'beta':0.0,
  #                              'ninits':10, 'initr':5, 'ncls':2,
  #                            'lmbd':1e-3, 'lmbd_mi':1e-3, 'mindist':20,
  #                              'numiter':5, 'wsize':10, 'maxdispl':3, 'alpha':1.0,
  #          'mlevels':7,'nproc':2,'abstol':1e-3,'reltol':1e-3,'show_edges':True})

  c0=time.clock()
  t0=time.time()
  t=simseg.TwoImages(img1,img2,bigtools.Parameters(par,override={'mlevels':7,'do_affine':True,'maxdispl':3}))
  if saveimgs:
    fgs=t.show_superpixels()
    for i in xrange(2):
      plt.figure(fgs[i])
      segreg.savefigure(pref+'superpixels%d.png' % i)
    wimg,transf,segms=t.iterate(output_prefix=pref,visualize=True)
  else:
    wimg,transf,segms=t.iterate(visualize=False)
  lndw=transform_landmarks(transf,lnd1)
  if saveimgs:
    t.show_segmentation()
    t.show_overlaid_segmentations(maketitle=False) ;
    segreg.savefigure(pref+'combinedsegm-after.png')
    plt.show(block=False)
    plt.figure(1) ; plt.clf() ;
    segreg.show_combined_images(img1,wimg) ;
    segreg.savefigure(pref+'combinedimgs-after.png')
    plt.clf()
    show_landmark_pairs(None,img2,lndw,lnd2)
    segreg.savefigure(pref+'landmarks-after.png')
    plt.title('after final registration')
    plt.figure()
    # pdb.set_trace()
    transf.saveimg(pref+'deformation.png')
  print "Second stage started"
  #pdb.set_trace()
  pref2=pref+'2'
  t2=simseg.TwoImages(img1,wimg,par)
  if saveimgs:
    wimg2,transf2,segms2=t2.iterate(output_prefix=pref2,visualize=True)
  else:
    wimg2,transf2,segms2=t2.iterate(visualize=False)
  c0=time.clock()-c0
  t0=time.time()-t0
  ttransf=transf2.compose(transf)
  lndw2=transform_landmarks(ttransf,lnd1)
  if saveimgs:
    t2.show_segmentation()
    t2.show_overlaid_segmentations(maketitle=False) ;
    segreg.savefigure(pref2+'combinedsegm-after.png')
    plt.show(block=False)
    plt.figure(1) ; plt.clf() ;
    segreg.show_combined_images(img1,wimg2) ;
    segreg.savefigure(pref2+'combinedimgs-after.png')
    plt.clf()
    show_landmark_pairs(None,img2,lndw2,lnd2)
    segreg.savefigure(pref2+'landmarks-after.png')
    plt.title('after final registration')
    plt.figure()
    # pdb.set_trace()
    ttransf.saveimg(pref2+'deformation.png')
  mean0,std0,median0,worst0=landmark_stats(lnd1,lnd2)
  mean1,std1,median1,worst1=landmark_stats(lndw,lnd2)
  mean,std,median,worst=landmark_stats(lndw2,lnd2)
  s='%s %s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n' % (img1n,img2n,mdiag,mean0,std0,median0,worst0,mean1,std1,median1,worst1,mean,std,median,worst,c0,t0)
  outf.write(s)
  outf.flush()
  print "Experiment finished in CPUtime ",c0, " wall time ", t0, " diag=", mdiag
  print "Landmark error: mean %f (+- %f) ->  %f (+- %f) ->  %f (+- %f), median %f -> %f ->  %f , worst %f -> %f ->  %f " % ( mean0, std0, mean1, std1, mean,std, median0, median1, median, worst0, worst1, worst)
  print s

def all_experiments(fname,logfile):
  f=open(fname)
  ls=f.readlines()
  n=len(ls)
  #i=0 ; j=0
  i=12 ; j=0
  print "Reading control file ", fname
  while i+3<n:
    img1n=string.strip(prefix+ls[i])
    img2n=string.strip(prefix+ls[i+1])
    lnd1n=string.strip(prefix+ls[i+2])
    lnd2n=string.strip(prefix+ls[i+3])
    i+=4 ; j+=1
    pref=outprefix+('%d-' % j)
    outf=open(logfile,'a+')
    #one_experiment(img1n,img2n,lnd1n,lnd2n,outf,pref,saveimgs=True)
    one_experiment(img1n,img2n,lnd1n,lnd2n,outf,pref,saveimgs=False)
    outf.close()
  print "Control file finished"

def one_itk_experiment(img1n,img2n,lnd1n,lnd2n,outf,pref,respref):
  plt.close('all')
  lnd1=read_landmarks(lnd1n)
  lnd2=read_landmarks(lnd2n)
  assert lnd1.shape[0]==lnd2.shape[0]
  print "### Starting experiment ", img1n, " ", img2n, " ", img1n, " ", img2n
  img1=scipy.misc.imread(img1n)
  print "Read image ", img1n, " of size ", img1.shape
  img2=scipy.misc.imread(img2n)
  print "Read image ", img2n, " of size ", img2.shape
  def diag(i):
    return math.sqrt(i.shape[0]**2+i.shape[1]**2)
  mdiag=max([ diag(i) for  i in [img1,img2]])
  lnd2wn=os.path.join(respref,os.path.splitext(os.path.basename(lnd2n))[0]+'.csv')
  lndw=read_landmarks_csv(lnd2wn)
  plt.figure()
  plt.clf()
  show_landmark_pairs(None,img1,lnd1,lnd2)
  # pdb.set_trace()
  segreg.savefigure(pref+'itk-landmarks-before.png')
  plt.figure()
  plt.clf()
  show_landmark_pairs(None,img2,lndw,lnd2)
  # pdb.set_trace()
  segreg.savefigure(pref+'itk-landmarks-after.png')
  img2wn=os.path.join(respref,
                      os.path.splitext(os.path.basename(img2n))[0]+'-regist'+'.png')
  wimg=scipy.misc.imread(img2wn)
  plt.figure() ; plt.clf() ;
  segreg.show_combined_images(img1,wimg) ;
  segreg.savefigure(pref+'itk-combinedimgs-after.png')
  mean0,std0,median0,worst0=landmark_stats(lnd1,lnd2)
  mean,std,median,worst=landmark_stats(lndw,lnd2)
  # s='%s %s %f %f %f %f %f \n' % (img1n,img2n,mdiag,mean,std,median,worst)
  s='%s %s %f %f \n' % (img1n,img2n,mean0,mean)
  outf.write(s)
  outf.flush()
  print "Landmark error: mean %f (+- %f) ->  %f (+- %f), median %f -> %f, worst %f -> %f" % ( mean0, std0, mean, std, median0, median, worst0, worst)
  print s


def all_itk_experiments(fname,logfile,respref):
  """ Show results of all experiments done in ITK """
  f=open(fname)
  ls=f.readlines()
  n=len(ls)
  # n=4
  i=0 ; j=0
  print "Reading control file ", fname
  while i+3<n:
    img1n=string.strip(prefix+ls[i])
    img2n=string.strip(prefix+ls[i+1])
    lnd1n=string.strip(prefix+ls[i+2])
    lnd2n=string.strip(prefix+ls[i+3])
    i+=4 ; j+=1
    pref=outprefix+('%d-' % j)
    outf=open(logfile,'a+')
    one_itk_experiment(img1n,img2n,lnd1n,lnd2n,outf,pref,respref)
    outf.close()
  print "Control file finished"


if __name__ == "__main__":  # running interactively
  plt.close('all')
  # cfile=os.path.expanduser("~/work/students/borovec/borovec/scripts/eval-regist/reg_list-scale10pc.txt")
  # all_experiments(cfile,outprefix+'output10pc.txt')
  #cfile=os.path.expanduser("~/work/segreg/output/reglist-flagship.txt")
  #all_experiments(cfile,outprefix+'output-5pc-flagship.txt')
  #cfile=os.path.expanduser("~/work/students/borovec/borovec/scripts/eval-regist/reg_list-scale5pc.txt")
  #all_experiments(cfile,outprefix+'output5pc.txt')
  cfile=os.path.expanduser("~/work/segreg/regist-results/experiment-itk/reg_list-scale10-5pc.txt")
  all_experiments(cfile,outprefix+'output-10-5pc-speed.txt')
  # this is for ISBI landmark experiment
  #all_itk_experiments(cfile,outprefix+'output-itk-10-5pc.txt','regist-results/experiment-itk')
