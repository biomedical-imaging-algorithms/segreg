% test of lndinterp

addpath('~/work/matlab/landmarks') ;

x= [ 1  5  6  7  9  15 ] ;
 y= [ 3  5  4  5  3  6 ] ;
%y= [ 3  5  7  8  9  10 ] ;

t=-4:0.1:15 ;
n=length(t) ;
sigma=2.0 ;
z=gpinterp(x,y,t,sigma) ;
xe=[ x ; zeros(1,length(x)) ]';
c=lndthscoef( xe,y) ;

sigmak=0.7 ;
zk=zeros(1,n) ;
zl=zeros(1,n) ;
for i=1:n,
  zk(i)=kernelinterp(x,y,t(i),sigmak) ;
  zl(i)=lndthseval([t(i) ; 0],xe,c );
end ;




figure(1) ; clf ;
plot(x,y,'o') ;
hold on ;
plot(t,z,'b-') ;
plot(t,zk,'r-') ;
plot(t,zl,'g-') ;
hold off ;
legend('measured','GP','kernel','landmarks') 