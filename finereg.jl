# Fine (elastic) registration of segmented images based on keypoints
# and B-splines
# The images are now almost registered

module finereg

using ImmutableArrays

using segimgtools
using bsplines
using Debug

include("debugassert.jl")


# type for keypoints on the boundary of classes in the reference image
immutable Keypoint
    pos::Vector{Float32} # position of the keypoint
    count::Integer # number of boundary points this one represents
    normal::Vector{Float32} # normal vector
    classpos::Uint8 # class in the positive direction of the normal vector
    classneg::Uint8 # class in the negative direction of the normal vector
end





# 

# here we store temporary data about each boundary point
type ProtoKeyPoint
    count::Integer         # number of boundary pixels
    pos::Vector{Float64}   # sum of pixel coordinates x
    pos2::Matrix{Float64}  # sum of x^t x
    c1::Integer           # class 1
    x1sum::Vector{Float64} # sum of pixel coordinates of class 1
    c2::Integer           # class 2
    x2sum::Vector{Float64} # sum of pixel coordinates of class 2
end

function ProtoKeyPoint2D(c1,c2) # with constructor (a factory function)
    return ProtoKeyPoint(0,zeros(Float64,2),zeros(Float64,2,2),
                         c1,zeros(Float64,2),c2,zeros(Float64,2))
end

typealias Keypoints Vector{Keypoint}

# Find measurement points (keypoints). Given a superpixel segmented image,
# find boundaries between two superpixels. So far, this is only for 2D 

function find_keypoints{S<:Integer,T<:Integer}(s::Array{S,2},l::Vector{T},minboundarysize=5)
    # 's' is an integer matrix of the superpixel number for each pixel
    # 'l' is a mapping from superpixel numbers to classes
    # return an array of Keypoint2D
    ny=size(s,1) ; nx=size(s,2)
    # go through the image and find boundaries between superpixels
    # we only check vertical and horizontal boundaries
    # the dictionary kpts will accumulate coordinates of boundary points
    # between superpixels assigned to different classes
    kpts=Dict{(Integer,Integer),ProtoKeyPoint}()
    function getkpt!(s1,s2,c1,c2)
        # get the value corresponding to superpixels s1,s2 from "kpts",
        # creating it if needed. Treat (s1,s2) the same as (s2,s1)
        if s1>s2
            return get!(()->ProtoKeyPoint2D(c1,c2),kpts,(s1,s2))
        else
            return get!(()->ProtoKeyPoint2D(c2,c1),kpts,(s2,s1))
        end
    end

    function check_differs(x1,y1,x2,y2)
        s1=s[y1,x1] ; s2=s[y2,x2]
        if s1!=s2 # two different superpixels
            l1=l[s1] ; l2=l[s2]
            if l1!=l2 # two different classes, let us update the table
                cx=(x1+x2)/2. ; cy=(y1+y2)/2. # this is float
                # update the value for (s1,s2), creating it if needed
                kpt=getkpt!(s1,s2,l1,l2)
                kpt.count+=1
                kpt.pos[1]+=cy ; kpt.pos[2]+=cx
                # here we unroll x^T x for speed, to avoid allocation
                kpt.pos2[1,1]+=cy*cy ;
                cxcy=cx*cy ;  kpt.pos2[2,1]+=cxcy ;
                kpt.pos2[1,2]+=cxcy ;
                kpt.pos2[2,2]+=cx*cx
                if s1>s2
                    kpt.x1sum+=[x1,y1]
                    kpt.x2sum+=[x2,y2]
                else
                    kpt.x2sum+=[x1,y1]
                    kpt.x1sum+=[x2,y2]
                end  

            end
        end
    end
    for ix=1:nx-1
        for iy=1:ny-1
            check_differs(ix,iy,ix+1,iy)
            check_differs(ix,iy,ix,iy+1)
        end
    end
    # filter out very small boundaries
    kpts=filter((k,v) -> v.count>=minboundarysize,kpts)
    # construct Keypoint structures - landmark positions and normals
    function process_keypoint(keyval::((Integer,Integer),ProtoKeyPoint))
        (s1,s2),k=keyval # deconstruct
        center=k.pos/k.count
        m=k.pos2/k.count-center*center' # covariance matrix
        # normal is the eigenvector of the smallest eigenvalue   
        # WARNING: we assume the eigenvalues are sorted and the eigenvectors
        # are normalized to unit length
        eigl,eigv=eig(m)
        @debugassert(eigl[1]==minimum(eigl))
        normv=eigv[:,1]
        @debugassert(abs(norm(normv)-1)<0.01)
        #normv/=norm(normv) # normalize to unit lenght
        # we now need to determine the classes
        if dot(k.x2sum-k.x1sum,normv)>0.
            cpos=k.c2 ; cneg=k.c1
        else
            cpos=k.c1 ; cneg=k.c2
        end
        return Keypoint(center,k.count,normv,l[s2],l[s1])
    end
    return [ process_keypoint(keyval)::Keypoint for keyval in kpts ]::Keypoints
end

function create_image_with_keypoints(l,p,kpts,k)
    # create an image for visualization purposes from images of the pixel labels
    # "l", superpixel numbers for each pixel "p" and a list of keypoints.
    # "k" is the number of classes
    @assert(size(l)==size(p))
    img=classimage2rgb(l,k) # show segmentation
    #function set_gray(y,x)
    #   img[y,x,:]=50
    #end
    boundary_pixels!(p) do y,x # mark superpixel boundaries gray
       img[y,x,:]=0xa0
    end
    for kpt in kpts # show keypoints as black squares with the normal direction
        # println(kpt)
        y0=integer(kpt.pos[1]) ; x0=integer(kpt.pos[2])
        draw_line!(img,y0,x0,kpt.normal)
        draw_box!(img,y0,x0)
    end
    return img
end


function sample_normals(f::LabelImage,k::Integer,
                        kpts::Keypoints,
                        scale=1.0::Float64,maxdist::Integer=20)
    # given label image f with classes 0..k,  for each keypoint "kpt"
    # from "kpts" sample along the normal at points
    #   y[i]=kpt.pos+scale*kpt.normal*(i-maxdist-0.5)   for i=1..2*maxdist
    # and store it into array of class labels of size 2*maxdist \times numkpts

    @assert (maxdist>0 && maxdist<200)

    c=zeros(LabelType,2*maxdist,length(kpts))
    # TODO the following loops might be optimized to avoid garbage collection
    # (e.g. to create y)
    for j=1:length(kpts)
        kpt=kpts[j]
        for i=1:2*maxdist
            y=integer(kpt.pos+scale*kpt.normal*(i-maxdist-0.5))
            try
                c[i,j]=f[y...] # TODO - does it hinder performance?
            catch exc
                if !isa(exc,BoundsError)
                    rethrow()
                end
            end
        end
    end
    return c::Matrix{LabelType}
end
                
# B-splines basis functions
# control point position is x_i=h*(i-d)

function phi(x::Float64,i::Integer,h::Float64,d::Float64)
    return cbspln(x/h+d-i)
end

function MIL_contributions(w::Matrix{Float64},kpts::Keypoints,
                           cnormals::Matrix{LabelType},
                           maxdist::Integer)
    # convert the classes from sample_normals to local contributions of the MIL criterion
    # returns a matrix D of size 2*hmax+1 \times numkpts
    # if  a displacement at keypoint 'i' is 't[i]', the criterion is approximated as
    # sum_i D[dot(t[i],kpts[i].normal)+hmax+1,i]
    # hmax=floor(maxdist/2) is the maximum displacement and also the normal size of
    # the sampled region
    #
    # w is a matrix returned by mil.MIL_weights(f,g) with reference image 'f'
    # and moving image 'g'
    @assert(size(cnormals,1)==2*maxdist)
    hmax=div(maxdist,2)
    wt=w' # for faster access
    D=zeros(Float64,2*hmax+1,length(kpts))
    for i=1:length(kpts)
        kpt=kpts[i]     # keypoints from the first image
        c=cnormals[:,i] # samples from the second image
        # TODO: avoid allocation to speed up
        ipos=cumsum(wt[c,kpt.classpos])
        ineg=cumsum(wt[c,kpt.classneg])
        iposf= ii -> (ii>-maxdist) ? ipos[ii+maxdist] : 0 # value for displacement ii
        inegf= ii -> (ii>-maxdist) ? ineg[ii+maxdist] : 0
        # count is divided by two as there are points from both classes
        count_half=kpt.count*0.5
        for j=-hmax:hmax
            ## # slow version, for check
            ## s=0.
            ## for k=-hmax+1:0
            ##     s+=wt[c[k+j+maxdist],kpt.classneg]
            ## end
            ## for k=1:hmax
            ##     s+=wt[c[k+j+maxdist],kpt.classpos]
            ## end
            contrib=(iposf(j+hmax)-iposf(j)+inegf(j)-inegf(j-hmax))
            #println("bod $i, j=$j, slow=$s, fast=$contrib")
            #@assert(abs(s-contrib)<1e-5*abs(s))
            D[j+hmax+1,i]=count_half*contrib
        end
    end
    return D
end

function clip(x,minx,maxx)
    @debugassert(minx<=maxx)
    if x>maxx
        return maxx
    elseif x<minx
        return minx
    else
        return x
    end
end

function eval_sampled_MIL_criterion(kptsf::Keypoints,kptsg::Keypoints,
                                    D::Matrix{Float64},
                                     transf::Function)
    # given the keypoints 'kptsf' and 'kptsg' in images 'f' and 'g',
    # the keyword contributions 'D' calculated by
    # MIL_contributions and the deformation function
    # transf:Vector{Float64}->Vector{Float64} (or Float32),
    # calculate the MIL similarity criterion up to a constant, times a number of pixels
    hmaxd=size(D,1)
    hmax=div(hmaxd,2)
    @assert(2*hmax+1==hmaxd)
    @assert(size(D,2)==length(kptsf))
    @assert(length(kptsf)==length(kptsg))
    critval=0.
    # sumpix=0
    for i=1:length(kptsf)
        kptf=kptsf[i] ; kptg=kptsg[i]
        y=transf(kptf.pos) # vector of floats - output coordinates
        ## The following performs a nearest neighbor interpolation
        #h=integer(dot(y-kptg.pos,kptg.normal)) # project onto normal
        #h=clip(h,-hmax,+hmax)
        #critval+=D[h+hmax+1,i]
        # Linear interpolation
        h=dot(y-kptg.pos,kptg.normal) # project onto normal
        if h>=hmax
            val=D[2*hmax+1,i]
        elseif h<=-hmax
            val=D[1,i]
        else
            h+=hmax+1 ; hf=floor(h) ; hd=h-hf ; hi=integer(hf)
            val=D[hi,i]*(1.-hd)+D[hi+1,i]*hd
        end
        critval+=val
        # sumpix+=kptf.count
    end
    return critval
end

function register_fast_rigid(f::LabelImage,g::LabelImage,kptsf::Keypoints,kptsg::Keypoints,k;x0=[0;0;0],maxdist=20)
    # given two segmented images and the keypoints in both of them,
    # register them rigidly. x0 is the initial point, if given
    # Precalculating MIL matrix. The parameterisation is [angle,shift_vector]
    w=mil.MIL_weights(f,g,k)
    # Sampling normals
    cnormals=sample_normals(g,k,kptsg,1.0,maxdist)
    # Calculating contributions
    D=finereg.MIL_contributions(w,kptsf,cnormals,maxdist)
    npix=length(f)
    count=0
    # Criterion function
    function crit(theta,grad)
      @assert(length(grad)==0) # we do not provide a gradient
      count+=1
      A,t=create_rigid_transform(size(f),theta[1])
      tt=t+theta[2:3]
      transf = x-> A*x+tt
      m=finereg.eval_sampled_MIL_criterion(kptsf,kptsg,D,transf)/npix
      return m  
    end
    dof=3 # number of degrees of freedom
    opt=Opt(:LN_BOBYQA,dof)
    max_objective!(opt,crit)
    lower_bounds!(opt,[-200.,-max(size(f,2),size(g,2))/2,-max(size(f,1),size(g,1))/2])
    upper_bounds!(opt,[200.,max(size(f,2),size(g,2))/2,max(size(f,1),size(g,1))/2])
    xtol_abs!(opt,[0.5;0.1;0.1]) 
    maxeval!(opt,1000) # at most 1000 evaluations
    initial_step!(opt,[1.;1.;1.]) # initial step
    (optf,optx,ret)=optimize(opt,x0)
    println("register_rigid_fast: Optimization finished after $count iterations, crit=$optf")
    return coarsereg.RigidRegistrationResult2D(optx),optf  
end


        
end # module
