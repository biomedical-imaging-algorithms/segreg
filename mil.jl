# mutual information on labels, evaluation

module mil

using Debug

export calculateMIL, entropy

include("debugassert.jl")
switchasserts(false)

typealias Histogram Matrix{Int64}

function entropy(p::Array)
    # calculate an entropy from a 1D or 2D histogram
    sump=sum(p)
    H=0.
    for i=1:length(p)
        if p[i]>0
            pp=p[i]/sump
            H+=pp*log(pp)
        end
    end
    return -H
end

function joint_histogram{T1<:Integer,T2<:Integer,N}(
                         f::Array{T1,N},g::Array{T2,N},k::Integer)
    # given two images of the same size containing integers 0..k, calculate
    # their joint histogram k x k. The value '0' means 'unknown' and such data points
    # are ignored.
    @debugassert(size(f)==size(g))
    @assert(k>0 && k<200)
    n=length(f) # number of pixels
    p=zeros(Int64,k,k) # JK: corrected "ones" to "zeros"
    for i=1:n
        cf=f[i] ; cg=g[i]
        if cf>0 && cg>0 then
            @debugassert(cf>=1 && cf<=k)
            @debugassert(cg>=1 && cg<=k)
            p[cf,cg]+=1
        end
    end
    return p::Histogram
end

    
function calculateMIL{T1,T2,N}(f::Array{T1,N},g::Array{T2,N},k::Integer)
    # given two discrete N dimensional images f,g with values 1..k
    # f,g can also contain a special value '0' meaning "unknown class"
    # calculate the Mutual information on labels (MIL)
    p=joint_histogram(f,g,k)
    # calculate mutual information
    return entropy(sum(p,2))+entropy(sum(p,1))-entropy(p)
end


function MIL_weights{T1<:Integer,T2<:Integer,N}(f::Array{T1,N},g::Array{T2,N},k::Integer)
    # given two discrete images with values 0..k
    # calculate the MIL contribution weights
    # w(k,l)=log(P(k,l)/(P(k)*P(l)))
    # where P(k,l) is the global joint class probality matrix
    h=mil.joint_histogram(f,g,k)
    eps=1e-8 # to avoid logarithm of zero
    totcnt=sum(h)
    pf=(sum(h,2)+eps)/totcnt # marginal probability, size (3,1)
    pg=(sum(h,1)+eps)/totcnt # size (1,3)
    pfg=(h+eps)/totcnt
    w=log(pfg./(pf*pg))
    return w::Matrix{Float64}
end
 



end # module
