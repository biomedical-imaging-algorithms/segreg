# test mutual information

import numpy as np

def mi(c):
    """ calculate mutual information from a histogram """
    eps=1e-16
    totcnt=float(np.sum(c))+eps
    pf=(np.sum(c,axis=1)+eps)/totcnt
    pg=(np.sum(c,axis=0)+eps)/totcnt
    hf=-np.dot(pf,np.log(pf))
    hg=-np.dot(pg,np.log(pg))
    pfg=c/totcnt
    hfg=-np.tensordot(pfg,np.log(pfg+eps))
    inf=hf+hg-hfg
    print "hf=",hf, " hg=", hg, " hfg=", hfg, " inf=",inf
    return inf

def main():
    mi(np.array(((1,0),(0,0))))
    mi(np.array(((3,0),(0,3))))
    mi(np.array(((1,0),(0,1))))
    mi(np.array(((10,5),(5,10))))
    mi(np.eye(2))
    mi(np.eye(3))
    mi(np.array(((10,0,0),(0,5,5),(0,0,0))))
    mi(np.eye(4))
    mi(np.eye(4)*100)

if __name__ == "__main__":  # running interactively
    main()
