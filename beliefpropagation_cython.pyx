# Belief propagation according to Felzenszwalb & Huttenlocher
#
# Jan Kybic, October 2012
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#distutils: language = c++

import numpy as np
import pylab
import time
import pdb
# to import Cython
#import pyximport
#pyximport.install(reload_support=True)
import segregtools

from segregtools cimport *
cimport numpy as np
cimport cython
from libc.stdlib cimport malloc, free
from libcpp.vector cimport vector
import multiprocessing

DTYPEf = np.float
DTYPEw = np.int
DTYPE = np.uint8
DTYPEs = np.uint16

inf=float('inf')

def shiftmatrix(a,sx,sy,c):
    """ Create a shifted version of matrix a, such that b=shiftmatrix(a,sx,sy)
        with b[x,y]=a[x+sx,b+sy]. b has the same size and type as 'a' and
        the remaining elements are filled with constant 'c'.'"""
    if sx==0 and sy==0:
        return a
    # pdb.set_trace()
    b=np.ones(a.shape,dtype=a.dtype)*c
    nx=a.shape[0] ; ny=a.shape[1] ;
    if sx>0:
        if sx<nx:
            bxmin=0 ; bxmax=nx-sx
        else: return b
    else:
        if -sx<nx:
            bxmin=-sx ; bxmax=nx ;
        else: return b
    if sy>0:
        if sy<ny:
            bymin=0 ; bymax=ny-sy
        else: return b
    else:
        if -sy<ny:
            bymin=-sy ; bymax=ny ;
        else: return b
    b[bxmin:bxmax,bymin:bymax]=a[bxmin+sx:bxmax+sx,bymin+sy:bymax+sy]
    return b



class BPGraph:
    """ Belief propagation with quadratic functions on a graph

    The graph is described by self.* variables:
     D - list of n matrices of size m x m, containing the unary terms
     edges - list of n integer lists containing edges, normally symmetrical
     weights - list of n float lists corresponding to

     The following self.* variables will be defined automatically

     n - number of nodes
     m - size of the unary term matrices
     msgs - list of n arrays of m x m float arrays, containing messages received
            in the previous iteration
     bestlabel - array of 2-item arrays of the currently best solution


    """

    def init_msgs(self):
        """ initialize messages to zero. To be called before the other message related functions are called """
        self.n=len(self.D)
        self.m=self.D[0].shape[0]
        assert self.D[0].shape[1]==self.m

        self.msgs=[]
        for i in xrange(self.n):
            numedg=len(self.edges[i])
            self.msgs.append(np.zeros((numedg,self.m,self.m)))

    def calc_msg(self,p,q,c,offsets=None):
        """ calculate the message from node p to node q with edge weight c"""
        #print "calc_msg(p=%d,q=%d,c=%f)" % (p,q,c)
        #pdb.set_trace()
        if offsets is not None:
            shift=offsets[q]-offsets[p]
            sqx=max(0,shift[0]) ; sqy=max(0,shift[1]) ;
            spx=max(0,-shift[0]); spy=max(0,shift[1]) ;
            nr=(self.m,self.m)
        dp=self.D[p]
        if dp is None:
          if offsets is None:
            r=np.zeros((self.m,self.m))
          else:
            r=np.zeros((nr[0]+abs(shift[0]),nr[1]+abs(shift[1])))
        else:
          # nr=dp.shape
          if offsets is None:
            r=np.copy(dp) # unary term
          else:
            # pdb.set_trace()
            r=np.max(dp)*np.ones((nr[0]+abs(shift[0]),nr[1]+abs(shift[1])))
            r[spx:spx+nr[0],spy:spy+nr[1]]=dp
        e=self.edges[p] # edge indices
        n=len(e)
        m=self.msgs[p] # messages received by p
        for i in xrange(n): # go over all neighbors except q
            s=e[i] # neighbor index
            if s<>q:
                if offsets is None:
                    r+=m[i] # message received from s
                else:
                    r[spx:spx+nr[0],spy:spy+nr[1]]+=m[i]
        #print "r ",r
        #rout=segregtools.q_envelope2D(c,r)
        rout=np.zeros(r.shape)
        q_envelope2D_out(c,r,rout)
        #rout=q_envelope2D(c,r)
        # print "message is ",rout
        if offsets is None:
            return rout
        else:
            return rout[sqx:sqx+nr[0],sqy:sqy+nr[1]]


    def send_all_msgs(self,offsets=None):
        """ Return all messages """
        newmsgs=[]
        for q in xrange(self.n): # messages for node q
            e=self.edges[q]
            ne=len(e)
            mfq=np.zeros((ne,self.m,self.m))
            for i in xrange(ne): # go over all neighbors
                p=e[i]
                mfq[i]=self.calc_msg(p,q,self.weights[q][i],offsets=offsets)
            newmsgs.append(mfq)
        return newmsgs


    def estimate(self,offsets=None):
        """ Return the currently best labeling and the energy """
        uenergy=0
        best=[] # best labels
        for q in xrange(self.n): # for all nodes
            b=self.get_belief(q) # get belief for node q
            ind=np.argmin(b)
            ix,iy=np.unravel_index(ind,b.shape)
            best.append((ix,iy))
            if self.D[q] is not None:
              uenergy+=self.D[q][ix,iy]
        # add edge energy
        penergy=0.
        for q in xrange(self.n): # for all nodes
            e=self.edges[q]
            ne=len(e)
            for i in xrange(ne):
                p=e[i]
                if offsets is None:
                    dx=best[q][0]-best[p][0]
                    dy=best[q][1]-best[p][1]
                else:
                    dx=best[q][0]-best[p][0]+offsets[q][0]-offsets[p][0]
                    dy=best[q][1]-best[p][1]+offsets[q][1]-offsets[p][1]
                penergy+=0.5*self.weights[q][i]*(dx*dx+dy*dy)
        energy=uenergy+penergy
        print "uenergy=",uenergy," penergy=",penergy, " total=",energy
        return energy,best

    def get_belief(self,p):
      if self.D[p] is None:
        r=np.zeros((self.m,self.m))
      else:
        r=np.copy(self.D[p]) # unary term
      e=self.edges[p] # edge indices
      n=len(e)
      m=self.msgs[p] # messages received by p
      for i in xrange(n): # go over all neighbors
        r+=m[i] # message received from e[i]
      return r

    def solve(self,offsets=None,maxit=100,abstol=1e-3, reltol=1e-3):
        """ Iterate until convergence and return the best energy and labeling.
            Offsets are shifts offsets for each node, i.e. the displacement corresponding to the center of the cost matrix. The displacements are represented with these offsets """
        self.init_msgs()
        best_energy=inf
        best_labels=[(0,0)]*self.n
        reldiff=inf
        for i in xrange(maxit):
            energy,labels=self.estimate(offsets=offsets)
            if i>0 and best_energy<>0.0:
                reldiff= abs(energy-best_energy)/abs(best_energy)
            print "i=",i," energy=",energy, "prev best_energy=",best_energy, " reldiff=",reldiff
            if energy<best_energy:
                best_energy=energy ; best_labels=labels
            else:
                # if energy increases, break
                break
            if i>0 and (abs(energy)<abstol or reldiff<reltol):
                break
            self.msgs=self.send_all_msgs(offsets=offsets)
        if i<maxit:
            print "Convergence detected"
        else:
            print "Maximum number of iterations reached"
        # calculate offsets from labels
        shift=(self.m-1)/2
        #print "m=",self.m, " shift=",shift
        #best_labels=map(lambda (ix,iy): (ix-shift,iy-shift),best_labels)
        [ (ix-shift,iy-shift) for (ix,iy) in best_labels ]
        return best_energy, best_labels

def try_envelope2D():
    h=np.ones((10,5),'float')*10
    h[3,4]=2
    h[9,2]=3
    t=time.time()
    #r=q_envelope2D(0.2,h)
    r=segregtools.q_envelope2D(0.2,h)
    t=time.time()-t
    print "q_envelope took",t
    pylab.clf()
    pylab.subplot(2,1,1)
    pylab.imshow(h,interpolation='nearest')
    pylab.subplot(2,1,2)
    pylab.imshow(r,interpolation='nearest')
    # pdb.set_trace()


def try_envelope():
    h=np.array((5,10,2,1,3,8,2,10,3),'float')
    n=h.shape[0] ;
    # r=q_envelope(3,h)
    t=time.time()
    r=segregtools.q_envelope(0.5,h)
    t=time.time()-t
    print "q_envelope took",t
    pylab.clf()
    pylab.plot(np.arange(n),h,'ko',markersize=5)
    pylab.plot(np.arange(n),r,'r-')
    # pdb.set_trace()

def make_unary_potential(m,ix,iy):
    n=2*m+1
    t=np.arange(-m,m+1)
    yy,xx=np.meshgrid(t,t)
    return ((xx-ix)**2+(yy-iy)**2).astype('float')

def try_BPGraph():
    g=BPGraph()
    f=make_unary_potential
    m=5
    g.D=[ f(m,-3,0), f(m,1,0), f(m,0,2) ]
    #g.D=[ f(m,0,0), f(m,0,0), f(m,0,0) ]
    g.edges=[ [ 1,2 ] , [ 0, 2] , [0,1] ]
    #g.weights=[ [0.3,0.3] , [ 0.3,0.6], [0.3,0.6] ]
    #g.weights=[ [0.03,0.03] , [ 0.03,0.06], [0.03,0.06] ]
    g.weights=[ [0.03,0.03] , [ 0.03,10], [0.03,10] ]
    #g.weights=[ [0.,0.] , [ 0.,0.], [0.,0.0] ]
    #pdb.set_trace()
    e,l=g.solve()
    print "e=",e
    print "l=",l

def try_BPGraph_offset():
    g=BPGraph()
    f=make_unary_potential
    m=5
    offsets=np.array(((1,0),(0,0),(0,0)))
    g.D=[ f(m,-3-offsets[0][0],-offsets[1][0]), f(m,1,0), f(m,0,2) ]
    #g.D=[ f(m,0,0), f(m,0,0), f(m,0,0) ]
    g.edges=[ [ 1,2 ] , [ 0, 2] , [0,1] ]
    g.weights=[ [0.3,0.3] , [ 0.3,0.6], [0.3,0.6] ]
    #g.weights=[ [0.03,0.03] , [ 0.03,0.06], [0.03,0.06] ]
    #g.weights=[ [0.03,0.03] , [ 0.03,10], [0.03,10] ]
    #g.weights=[ [0.,0.] , [ 0.,0.], [0.,0.0] ]
    #pdb.set_trace()
    e,l=g.solve(offsets=offsets)
    print "e=",e
    print "l=",l

if __name__ == "__main__":  # running interactively
    #try_envelope()
    #try_envelope2D()
    #try_BPGraph()
    try_BPGraph_offset()
    #pylab.show()
