# Experiments with segmentation/registration in Julia
# Jan Kybic, June 2014


module testreg

using Debug
using PyPlot
using Images
using ImageView
using PyCall
using Clustering
using gaussian
using mil
using segimgtools
using coarsereg
using finereg

@pyimport scipy.ndimage as ndimage
@pyimport bigtools
@pyimport simseg
@pyimport pylab


function superpixels(img;supcomp=15,supsize=1000,show_superpixels=false)
    # calculate superpixels of a 2D color image
    # returns a Python SuperpixelImage
    par=pycall(bigtools.Parameters,PyObject,
               {"supsize"=>supsize,"supcomp"=>supcomp,"show_superpixels"=>show_superpixels})
    s=pycall(simseg.SuperpixelImage,PyObject,img,par)
    return s[:slabels].+1,s[:get_features]()[:,2:end].'
end

function vertdivimg()
    # create a simple test image
    i=zeros(Uint8,50,100,3)
    i[:,1:49,1]=255 ; i[:,50:end,2]=255
    ii=colorim(i)
    return ii
end

function kmeans_segmentation(slabels,sfeatures,k)
    # segment superpixels by k-means into k-classes, returning labels
    # per superpixel and also per pixels
    @assert(k<256)
    t0=time()
    r=kmeans(sfeatures,k; display=:final)
    t0=time()-t0
    @printf("kmeans took %.2fs, cost %f\n",t0,r.totalcost)
    if !r.converged
        error("kmeans did not converge")
    end
    # get pixelwise segmentation for display
    c=convert(Vector{Uint8},r.assignments)
    p=c[slabels]
    return r.assignments,p
end    

function test_kmeans()
    #img1i=imread("imgs/simple.png")
    img1i=imread("imgs/Rat_Kidney_HE_Section02-0.png")
    # img1=convert(Array,img1i)
    img1=img2array(img1i)
    slabels,sfeatures=superpixels(img1,show_superpixels=true)
    k=5 # number of classes
    a,p=kmeans_segmentation(slabels,sfeatures,k)
    #pylab.imshow(p)
    return a,p
end
    
function register()
    # read the image
    img1i=imread("imgs/simple.png")
    #display(img1i)
    img1=convert(Array,img1i)
    # rotate it in Python
    img2=ndimage.rotate(img1,10,reshape=false,order=1,mode="nearest")
    #img2i=colorim(img2)
    #display(img2)
    # calculate superpixels
    k=5 # number of classes
    s1l,s1f=superpixels(img1)
    s2l,s2f=superpixels(img2)
    a1,p1=kmeans_segmentation(s1l,s1f,k)
    a2,p2=kmeans_segmentation(s2l,s2f,k)
    pylab.figure(1)
    pylab.clf()
    pylab.imshow(p1)
    pylab.figure(2)
    pylab.clf()
    pylab.imshow(p2)
    # superpixel labels are in s1.slabels
    #pylab.show()
    #s1[:get_features]()
    #s1[:slabels]
    return p1,p2
end

    
function test_imgnormapprox()
    img1i=imread("imgs/simple.png")
    #display(img1i)
    k=3
    img1=convert(Array,img1i)
    s1l,s1f=superpixels(img1)
    a1,p1=kmeans_segmentation(s1l,s1f,k)
    pylab.figure(1)
    pylab.clf()
    pylab.imshow(img1)
    pylab.title("original")
    pylab.figure(2)
    pylab.clf()
    pylab.imshow(p1)
    pylab.title("segmentation")
    g=imgnormapprox(p1,k)
    for i=1:k
        println("i=",i,"m=",g[i].mu,"\nC=",g[i].C,"prob=",g[i].w)
    end
    pr=[ evalimgnormapprox(size(p1),g[i]) for i=1:3 ]
    for i=1:3
        pylab.figure(2+i)
        pylab.clf()
        pylab.imshow(pr[i])
        pylab.title("Normal approximation class $i")
        pylab.colorbar()
    end
    # return pr
end


function test_entropy_rotation()
     # read the image
    img1i=imread("imgs/simple.png")
    #display(img1i)
    img1=convert(Array,img1i)
    # rotate it in Python
    angle=+10 # true angle in degrees
    img2=ndimage.rotate(img1,angle,reshape=false,order=1,mode="nearest")
    # find superpixels, segment and approximate both images 
    k=3 # number of classes
    s1l,s1f=superpixels(img1)
    a1,p1=kmeans_segmentation(s1l,s1f,k)
    g1=imgnormapprox(p1,k)
    s2l,s2f=superpixels(img2)
    a2,p2=kmeans_segmentation(s2l,s2f,k)
    g2=imgnormapprox(p2,k)
    pr1=[ evalimgnormapprox(size(p1),g1[i]) for i=1:3 ]
    pr2=[ evalimgnormapprox(size(p2),g2[i]) for i=1:3 ]
    figure(1)
    clf()
    contour(pr2[1],levels=[1.],hold=true)
    contour(pr2[2],levels=[1.],hold=true)
    contour(pr2[3],levels=[1.],hold=true)
    # evaluate entropy for various angles
    ts=linspace(-170,180,37)
    ent=zeros(size(ts))
    local i
    for i in 1:length(ts)
        angle=ts[i]
        A,t=rotation_about_center(size(img1)[1:2],angle)
        local entropy=entropy_of_transformed(g1,g2,A,t)
        ent[i]=entropy
        println("angle=$angle entropy=$entropy")
    end
    figure(1) ; clf()
    plot(ts,ent,"o-")
    xlabel("angle") ; ylabel("entropy")
end

function test_affine_segmreg()
    # test affine segmentation registration
    img2i=imread("imgs/simple.png")
    #display(img1i)
    img2=img2array(img2i)
    k=3 # number of classes
    s2l,s2f=superpixels(img2)
    _,p2=kmeans_segmentation(s2l,s2f,k)
    angle=10 ; shift=[0;0]
    p1=transform_rigid(p2,size(p2),angle,shift)
    figure(1) ; clf()
    imshow(p1) ; title("image p1") ; colorbar()


    figure(2) ; clf()
    imshow(p2) ; title("image p2") ; colorbar()
    ts=linspace(-170,180,37)
    ms=zeros(size(ts))
    mbest=-Inf
    p2tbest=nothing
    abest=0.
    for i in 1:length(ts)
        a=ts[i] ; s=[0 ; 0] 
        p2t=transform_rigid(p2,size(p1),a,s)
        m=calculateMIL(p1,p2t,k)
        if m>mbest 
            mbest=m
            abest=a
            p2tbest=p2t
        end
        ms[i]=m
        println("angle=$a mil=$m")
    end
    figure(3) ; clf()
    plot(ts,ms,"o-")
    xlabel("angle") ; ylabel("MIL")
    println("best angle = $abest mil=$mbest")
    figure(4) ; clf()
    imshow(p2tbest) ; title("p2tbest")
end

function test_register_rigid()
    # test affine segmentation registration
    img2i=imread("imgs/simple.png")
    #display(img1i)
    img2=img2array(img2i)
    k=3 # number of classes
    s2l,s2f=superpixels(img2)
    _,p2=kmeans_segmentation(s2l,s2f,k)
    #angle=10 ; shift=[20;30]
    #angle=40 ; shift=[0;0]
    #angle=10 ; shift=[10;5]
    angle=40 ; shift=[20;30]
    p1=transform_rigid(p2,size(p2),angle,shift)
    figure(1) ; clf()
    imshow(p1) ; title("image p1") ; colorbar()
    figure(2) ; clf()
    imshow(p2) ; title("image p2") ; colorbar()
    r,_=register_rigid(p1,p2,k,x0=[40;20;30])
    println("Registration result: $r true values: $angle $shift")
    @time warped=apply_transform(p2,r,size(p1))
    figure(3) ; clf() 
    imshow((p1+p2)/2); title("overlay before") ; colorbar()
    figure(4) ; clf() 
    imshow((p1+warped)/2); title("overlay after") ; colorbar()

end

function test_register_rigid_global()
    # test affine segmentation registration
    img2i=imread("imgs/simple.png")
    #display(img1i)
    img2=convert(Array,img2i)
    k=3 # number of classes
    s2l,s2f=superpixels(img2)
    _,p2=kmeans_segmentation(s2l,s2f,k)
    #angle=10 ; shift=[20;30]
    #angle=40 ; shift=[0;0]
    #angle=10 ; shift=[10;5]
    angle=40 ; shift=[20;30]
    p1=transform_rigid(p2,size(p2),angle,shift)
    figure(1) ; clf()
    imshow(p1) ; title("image p1") ; colorbar()
    figure(2) ; clf()
    imshow(p2) ; title("image p2") ; colorbar()
    #r,_=register_rigid_global(p1,p2,k)
    @time r,_=register_rigid_global2(p1,p2,k)
    #r=RigidRegistrationResult2D(zeros(3))
    println("Registration result: $r true values: $angle $shift")
    #theta=tovector(r)
    warped=apply_transform(p2,r,size(p1))
    figure(3) ; clf() 
    imshow((p1+p2)/2); title("overlay before") ; colorbar()
    figure(4) ; clf() 
    imshow((p1+warped)/2); title("overlay after") ; colorbar()
    warpedimg=apply_transform(img2i,r,size(p1))
    figure(5) ; clf() 
    view(warpedimg); title("warped image img2i") 
    
end
        

function test_register_rigid_multiscale()
    # test affine segmentation registration
    img2i=imread("imgs/simple.png")
    #display(img1i)
    img2=img2array(img2i)
    k=3 # number of classes
    s2l,s2f=superpixels(img2)
    _,p2=kmeans_segmentation(s2l,s2f,k)
    #angle=10 ; shift=[20;30]
    #angle=40 ; shift=[0;0]
    #angle=10 ; shift=[10;5]
    angle=40 ; shift=[20;30]
    p1=transform_rigid(p2,size(p2),angle,shift)
    figure(1) ; clf()
    imshow(p1) ; title("image p1") ; colorbar()
    figure(2) ; clf()
    imshow(p2) ; title("image p2") ; colorbar()
    @time r,_=register_rigid_multiscale(p1,p2,k)
    #r=RigidRegistrationResult2D(zeros(3))
    println("Registration result: $r true values: $angle $shift")
    #theta=tovector(r)
    warped=apply_transform(p2,r,size(p1))
    figure(3) ; clf() 
    imshow((p1+p2)/2); title("overlay before") ; colorbar()
    figure(4) ; clf() 
    imshow((p1+warped)/2); title("overlay after") ; colorbar()
end

function test_register_case03()
    # register real images
    img1=imread("imgs/case03-5-he-small.png")
    img2=imread("imgs/case03-3-psap-small.png")
    #i1=convert(Array,img1)
    i1=img2array(img1)
    figure(1); clf()
    imshow(i1) ; title("image 1")
    #i2=convert(Array,img2)
    i2=img2array(img2)
    figure(2); clf()
    imshow(i2) ; title("image 2")
    k=3 ;
    s1l,s1f=superpixels(i1)
    _,p1=kmeans_segmentation(s1l,s1f,k)
    s2l,s2f=superpixels(i2)
    _,p2=kmeans_segmentation(s2l,s2f,k)
    figure(3) ; clf()
    imshow(p1) ; title("segmentation 1")
    figure(4) ; clf()
    imshow(p2) ; title("segmentation 2")
    figure(5) ; clf() 
    imshow(overlay(p1,p2)); title("overlay segmentation before") ; colorbar()
    figure(6) ; clf() 
    imshow(overlay(i1,i2)); title("overlay images before") ;
    # here starts the registration
    @time r,_=register_rigid_multiscale(p1,p2,k)
    warped=apply_transform(p2,r,size(p1))
    figure(7) ; clf() 
    imshow(overlay(p1,warped)); title("overlay segmentation after") ; colorbar()
    warpedimg=convert(Array,apply_transform(img2,r,size(p1)))
    figure(8) ; clf() 
    imshow(overlay(i1,warpedimg)); title("overlay images after") ; colorbar()
end


function test_superpixels()
    img1=imread("imgs/case03-5-he-small.png")
    i1=convert(Array,img1)
    figure(1); clf()
    imshow(i1) ; title("image 1")
    k=5 ;
    s1l,s1f=superpixels(i1,supcomp=30,show_superpixels=true)
    figure(2) 
    imshow(s1l) ; title("superpixels 1")
    _,p1=kmeans_segmentation(s1l,s1f,k)
    figure(3) ; clf()
    imshow(p1) ; title("segmentation 1")
end


function test_find_keypoints()
    k=4
    img1=imread("imgs/case03-5-he-small.png")
    # i1=convert(Array,separate(img1))
    i1=convert(Array,img1i)
    i1=img2array(img1i)
    #figure(1); clf()
    s1l,s1f=superpixels(i1,supcomp=30,show_superpixels=false)
    l1,p1=kmeans_segmentation(s1l,s1f,k)
    #figure(1) ; clf()
    #imshow(p1) ; title("segmentation 1")
    figure(1) ; clf()
    @time kpts=finereg.find_keypoints(s1l,l1)
    println("number of keypoints=",length(kpts))
    p2=finereg.create_image_with_keypoints(p1,s1l,kpts,k)
    imshow(p2) ; title("segmentation with keypoints")
end

function test_MIL_contributions()        
    img1i=imread("imgs/simple.png")
    #display(img1i)
    img1=img2array(img1i)
    #i1=convert(Array,img1i)
    k=3 # number of classes
    s1l,s1f=superpixels(img1,supsize=100,show_superpixels=true)
    l1,p1=kmeans_segmentation(s1l,s1f,k)
    # segmentation 2 will be slightly shifted in y-direction
    #angle=0. ; shift=[0;2]
    #p2=transform_rigid(p1,size(p1),0.,shift)
    p2=p1 # identical images
    #figure(1) ; clf() 
    #imshow(overlay(classimage2rgb(p1,k),classimage2rgb(p2,k)))
    #title("overlay before") ; colorbar()
    println("Finding keypoints")
    kpts=finereg.find_keypoints(s1l,l1)
    img1kpts=finereg.create_image_with_keypoints(p1,s1l,kpts,k)
    #figure(2) 
    #imshow(img1kpts) ; title("segmentation with keypoints")
    @show w=mil.MIL_weights(p1,p2,k)
    println("Sampling normals")
    imgscale=1.0 ; maxdist=5
    cnormals2=finereg.sample_normals(p2,k,kpts,imgscale,maxdist)
    D=finereg.MIL_contributions(w,kpts,cnormals2,maxdist)
    @show D
end

@debug function test_eval_sampled_MIL_criterion()
    img2i=imread("imgs/simple.png")
    #img2i=vertdivimg()
    #display(img1i)
    img2=img2array(img2i)    # convert(Array,img2i)
    k=3 # number of classes
    #k=2 # number of classes
    #s2l,s2f=superpixels(img2,supsize=100)
    s2l,s2f=superpixels(img2,supsize=20,show_superpixels=false)
    l2,p2=kmeans_segmentation(s2l,s2f,k)
    angle=0 ; shift=[0;0]
    img1=transform_rigid(img2i,size(img2)[1:2],angle,shift)
    img1i=img2array(img2i) # =convert(Array,img1)
    s1l,s1f=superpixels(img1i,supsize=100)
    l1,p1=kmeans_segmentation(s1l,s1f,k)
    ## figure(1) ; clf()
    ## imshow(p1) ; title("image p1") ; colorbar()
    ## figure(2) ; clf()
    ## imshow(p2) ; title("image p2") ; colorbar()
    println("Finding keypoints")
    kpts1=finereg.find_keypoints(s1l,l1)
    img1kpts=finereg.create_image_with_keypoints(p1,s1l,kpts1,k)
    figure(3) 
    imshow(img1kpts) ; title("segmentation with keypoints")
    println("Precalculating MIL matrix")
    @show w=mil.MIL_weights(p1,p2,k)
    println("Sampling normals")
    imgscale=1.0 ; maxdist=20
    cnormals2=finereg.sample_normals(p2,k,kpts1,imgscale,maxdist)
    println("Precalculating MIL contributions")
    D=finereg.MIL_contributions(w,kpts1,cnormals2,maxdist)
    println("Evaluating for different angles")
    ts=linspace(-20,20,60)
    ms=zeros(size(ts))  # results of the fast version
    mss=zeros(size(ts)) # results of the slow version
    mbest=-Inf
    p2tbest=nothing
    abest=0.
    npix=length(img1i)
    for i in 1:length(ts)
        a=ts[i] ; s=[0 ; 0]
        #a=0.  ; s=[0 ; ts[i] ] # horizontal shift 
        A,t=create_rigid_transform(size(p1),a)
        transf = x-> A*x+t+s
        m=finereg.eval_sampled_MIL_criterion(kpts1,kpts1,D,transf)/npix
        p2t=transform_rigid(p2,size(p1),a,s)
        mss[i]=calculateMIL(p1,p2t,k)
        # m=calculateMIL(p1,p2t,k)
        if m>mbest 
            mbest=m
            abest=a
        end
        ms[i]=m
        #println("angle=$a mil=$m mils=$(mss[i])")
        println("shift=$a mil=$m mils=$(mss[i])")
    end
    figure(4) ; clf()
    plot(ts,ms,"o-")
    xlabel("angle or shift") ; ylabel("MIL approx")
    figure(5) ; clf()
    plot(ts,mss,"xg-")
    xlabel("angle or shift") ; ylabel("MIL ref")
    figure(6) ; clf()
    plot(ts,(ms-maximum(ms)),"bx-",ts,mss-maximum(mss),"g-")
    println("best angle or shift  = $abest mil=$mbest")
end


function test_register_fast_rigid()
    # test affine segmentation registration
    img2i=imread("imgs/simple.png")
    #display(img1i)
    img2=img2array(img2i)
    k=3 # number of classes
    angle=3 ; shift=[2;0]
    img1i=transform_rigid(img2i,size(img2i),angle,shift)
    img1=img2array(img1i)
    s1l,s1f=superpixels(img1)
    l1,p1=kmeans_segmentation(s1l,s1f,k)
    s2l,s2f=superpixels(img2)
    l2,p2=kmeans_segmentation(s2l,s2f,k)
    figure(1) ; clf()
    imshow(p1) ; title("image p1") ; colorbar()
    figure(2) ; clf()
    imshow(p2) ; title("image p2") ; colorbar()
    kptsf=finereg.find_keypoints(s1l,l1)
    kptsg=kptsf
    @time r,_=finereg.register_fast_rigid(p1,p2,k,x0=[0;0;0])
    #@time r,_=finereg.register_fast_rigid(p1,p2,k,x0=[0;0;0])
    #Profile.clear()
    #@profile r,_=finereg.register_fast_rigid(p1,p2,k,x0=[0;0;0])
    #Profile.print()
    println("Registration result: $r true values: $angle $shift")
    @time warped=apply_transform(p2,r,size(p1))
    figure(3) ; clf() 
    imshow(overlay(classimage2rgb(p1,k),classimage2rgb(p2,k))); title("overlay before") ; colorbar()
    figure(4) ; clf() 
    imshow(overlay(classimage2rgb(p1,k),classimage2rgb(warped,k))); title("overlay after") ; colorbar()
end

        
        
## function test()
##     unknown_function()
## end

## function test2()
##     println(unknown_variable)
## end

## function test3()
##     println(3^[3;2])
## end

end # module testreg
