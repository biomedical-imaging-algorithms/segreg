# Multivariate Gaussian distributions

module gaussian

export GParams, dimension, imgnormapprox, evalnormapprox, evalimgnormapprox, product
export transform, entropy, entropy_of_transformed

using Debug

immutable GParams # parameters of a normal distribution
    w::Float64               # weight
    mu::Vector{Float64}     # mean
    C::Matrix{Float64}      # covariance matrix C
    #invC::Matrix{Float64}   # inverse of the covariance matrix C
    #detC::Float64            # determinant of C

    function GParams(w,mu::Vector{Float64},C::Array)
        d=length(mu)
        @assert(size(C,1)==d && size(C,2)==d)
        @assert w>=0.
        #invC=inv(C)
        #detC=det(C)
        #return new(w,mu,C,invC,detC)
        return new(w,mu,C)
    end
end

# the methods of GParams

dimension(g::GParams) = length(g.mu)



function imgnormapprox{T,N}(p::Array{T,N},k::Integer)
    # Calculate the mean and variance of all classes in p
    # Returns the parameters of the Gaussians, Array{GParams}
    # p should contain numbers between 1 and k
    @assert(ndims(p)==N)
    sizep=size(p)
    n=length(p)
    # first calculate the mean
    m=zeros(N,k)
    cnt=zeros(Int,k)
    for i=1:n # for all pixels
        c=p[i]
        @assert(c>=1 && c<=k)
        m[:,c]+=[ind2sub(sizep,i)...] # increment sum, beware of the order of indices
        cnt[c]+=1       # increment counter
    end
    for i=1:k # normalize for each class
        m[:,i]./=cnt[i]
    end
    # calculate the covariance matrix here
    C=zeros(N,N,k)
    for i=1:n # for all pixels
        c=p[i] # the values have been tested above
        x=[ind2sub(sizep,i)...]-m[:,c] # difference wrt of the mean
        C[:,:,c]+=x*x'
    end
    for i=1:k # normalize for each class
        C[:,:,i]./=cnt[i]
    end
    # create the parameters
    return [ GParams(cnt[i],m[:,i],C[:,:,i]) for i=1:k ]
end


function evalnormapprox(g::GParams,x::Vector{Float64})
    # Evaluate the normal approximation described by 'g' at point 'x'
    d=dimension(g)
    @assert length(x)==d
    if g.w==0.
        return 0.
    end
    return g.w*((2pi)^d*det(g.C))^(-1/2)*exp(-0.5*dot(x,inv(g.C)*x))
end
    
function evalimgnormapprox(sizep,g::GParams)
    # Evaluate the normal approximation calculated by 'imgnormapprox'
    # for class described by parameters g for an image of dimension 'sizep'
    y=zeros(sizep)
    d=length(sizep) # dimension
    invC=inv(g.C)
    detC=det(g.C)
    @assert (d==dimension(g))
    # precalculate constant
    c=g.w*((2pi)^d*detC)^(-1/2)
    
    mu=g.mu
    for i=1:length(y)
        x=[ind2sub(sizep,i)...]-mu
        y[i]=c*exp(-0.5*dot(x,invC*x))
    end
    return y
end

function productintegral(g::GParams,h::GParams)
    # integral of the product of two Gaussians is calculate by evaluating a normal pdf
    # see Matrix cookbook
    gh=GParams(g.w*h.w,g.mu,g.C+h.C)
    return evalnormapprox(gh,h.mu)
end

function normalize_columns(A)
    # scale each column so that it sums to 1
    # this fails if some column contains only zeros
    return A./sum(A,1)
end
    
function entropy(gs::Array{GParams},hs::Array{GParams})
    # joint entropy of two discrete probability fields
    # each component 'k' of 'gs' and 'hs' describes the probability of a classes 'k' in space
    # this is an approximation, since the approximated probabilities do not sum to 1  
    k=length(gs)
    @assert length(hs)==k
    # calculate the approximate joint probability matrix
    P=zeros(k,k)
    for i=1:k
        for j=1:k
            P[i,j]=productintegral(gs[i],hs[j])
        end
    end
    println("P=",P)
    # normalize the joint probability matrix
    P/=sum(P) ;
    println("normalized P=",P)
    # calculate entropy
    H=0.
    for i=1:k
        for j=1:k
            p=P[i,j]
            H+=p>0. ? -p*log(p) : 0. # just to deal with log(0)
        end
    end
    return H

end

function transform(g::GParams,A::Matrix{Float64},t::Vector{Float64})
    # given a normal distribution p(x) described by parameters g
    # find parameters of a distribution p(y)=p(x) with y=Ax+t
    return GParams(g.w,A*g.mu+t,A*g.C*A')
end

function transform(gs::Array{GParams},A::Matrix{Float64},t::Vector{Float64})
    return map(g->transform(g,A,t),gs)
end
    
function entropy_of_transformed(gs::Vector{GParams},hs::Vector{GParams},A::Matrix{Float64},t::Vector{Float64})
    # calculate the entropy between the class probability field described by 'gs'
    # and an affinely transformed version of 'hs'
    return entropy(gs,transform(hs,A,t))
end

end
    
    
    
