#!/usr/bin/python
#
# Graphcut segmentation on superpixels
#
# Jan Kybic, December 2012

import numpy as np
import scipy
import time
import scipy.ndimage
import segregtools
import pygco
import pdb

class SuperGraphcuts:
    """ Segmentation on superpixels """

    intf=1e4 # to convert floats to ints
    maxv=1e9 # maximum representable number (we leave a little room)

    def __init__(self,superimage,probs):
        """ Given a SuperpixelImage superimage, class probabilities for all superpixels probs,
        prepare to find anoptimal labeling by GraphCut """
        self.ncls=probs.shape[1]
        self.nsuper=superimage.nsuper
        self.s=superimage
        assert probs.shape[0]==self.nsuper
        # calculate unary potentials as real values
        self.unary=np.clip(-self.intf*np.log(probs),-self.maxv,self.maxv).astype(np.int32)
        self.edges=segregtools.get_edges(superimage.slabels)
        self.totedgw=np.sum(self.edges[:,2]) # total edge weight

    def solve(self,gamma):
        """ Find optimal labeling for a given Potts model weight gamma """
        intf=100000
        pairwise= -gamma*np.eye(self.ncls)*(self.edges.shape[0]/float(self.totedgw))
        pairwise=np.clip(self.intf*pairwise,-self.maxv,self.maxv).astype(np.int32)
        #pdb.set_trace()
        t=time.time()
        print "Starting graphcut segmentation with gamma=", gamma
        r=pygco.cut_from_graph(self.edges,self.unary,pairwise)
        #r=np.asarray(r,'int')
        print "Graphcut segmentation took ", time.time()-t
        return r

# ----------------------------------------------------------------------



