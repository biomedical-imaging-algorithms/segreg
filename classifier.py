# Classifiers in Python
#
# Jan Kybic, November 2013
#
# TODO: update according to classifier_python.pyx - add 'self.par.beta'

import numpy as np
import scipy
import scipy.optimize
import bigtools
import pdb
import time
import segregtools
import math
import sys
import classifier_cython

class SoftmaxClassifier:
    """ train and evaluate a softmax classifier. Assume that the first
        element of the feature vector is always 1 """



    def __init__(self,tr,par):
        """ Create a classifier, tr is a vector of training examples - pairs of
        weights and features for all classes """
        self.par=bigtools.Parameters(par,default={'lmbd':1.0, 'kernel_order':1})
        self.ncls=len(tr)
        #pdb.set_trace()
        self.fdim=tr[0][1].shape[1]
        self.kdim=segregtools.kernelize_size(self.fdim,self.par.kernel_order)
        self.tr=tr
        order=self.par.kernel_order
        eps=sys.float_info.min
        def crit(av):
           """ av is a vector of length ncls*kdim that will be reshaped to a matrix
             of coefficients a[ncls,kdim]. 'crit' returns function
             value and a gradient """
           a=av.reshape(self.ncls,self.kdim)
           s=0. # here we sum the results
           sumw=0.
           g=np.zeros(a.shape) # to sum the gradient
           for k in xrange(self.ncls): # k is the class number
               w,f=tr[k] # weight and features
               n=f.shape[0]
               assert f.shape[1]==self.fdim
               for i in xrange(n): # for all training samples
                   # apply the kernel phi function
                   v=segregtools.kernelize(f[i],order)
                   e=np.nan_to_num(np.exp(np.dot(a,v)))
                   # denominator \sum \exp \phi(x_n)
                   den=np.sum(e)
                   # get probabilities
                   z=e/den
                   s-=w[i]*math.log(z[k]+eps)
                   z[k]-=1
                   sumw+=w[i]
                   g+=np.outer(z,v)*w[i]
           # pdb.set_trace()
           s=s/sumw+self.par.lmbd/(2.*self.ncls)*np.sum(a**2)
           g=g/sumw+a*(self.par.lmbd/self.ncls)
           # print "av=",a," s=",s, " s1=",self.par.lmbd/(2.*self.ncls)*np.sum(a**2), " g=",g
           return s,g.reshape((self.ncls*self.kdim,))
        a0=np.zeros((self.ncls*self.kdim,))
        a0=np.random.normal(scale=1e-16,size=(self.ncls*self.kdim,))
        # test_gradfun(crit,a0,h=1e-8)
        print "Sotfmax classifier training started"
        t=time.time()
        self.a,fopt,d=scipy.optimize.fmin_l_bfgs_b(crit,a0,m=10,factr=10,
         pgtol=1e-6,iprint=1,maxfun=1000)
        #self.a,fopt=scipy.optimize.fmin_powell(lambda x: crit(x)[0],a0,full_output=1,maxiter=1000)
        print "SoftmaxClassifier training took ", time.time()-t, " d=",\
               d['warnflag'],' funcalls=', d['funcalls']
        self.a=self.a.reshape((self.ncls,self.kdim))

    def evalprob(self,f):
        """ Evaluate class probabilities for given feature vectors """
        #print "Evaluating probabilities"
        #t=time.time()
        n=f.shape[0]
        assert f.shape[1]==self.fdim
        z=np.zeros((n,self.ncls))
        for i in xrange(n):
            v=segregtools.kernelize(f[i],self.par.kernel_order)
            e=np.exp(np.dot(self.a,v))
            e=np.nan_to_num(e) # to handle infinities
            den=np.sum(e)
            z[i]=e/den  # get probabilities
        #print "Evaluating probabilities took ", time.time()-t
        return z

    def hard_classify(self,f):
        """ Return the most likely class for each feature vector """
        t=time.time()
        z=self.evalprob(f)
        l=np.argmax(z,1)
        print "Classification (including calculating probabilities) took ", time.time()-t
        return l

def test_gradfun(f,x0,h=1e-5):
    """ Test the accuracy of any analytical gradient """
    n=x0.shape[0]
    assert x0.shape==(n,)
    J,g=f(x0)
    maxabserr=0.
    maxrelerr=0.
    for i in xrange(n):
        x=x0.copy() ; x[i]+=h
        J1,_=f(x)
        gn=(J1-float(J))/h
        abserr=abs(gn-g[i])
        relerr=abserr/abs(gn)
        maxabserr=max(abserr,maxabserr)
        maxrelerr=max(relerr,maxrelerr)
        print "i=%3d granal=%10.3g grnum=%10.3g abseerr=%10.3g relerr=%10.3g" % (i,g[i],gn,abserr,relerr)
    print "maxabserr=", maxabserr, " maxrelerr=", maxrelerr

def test_learn():
  tr=[(np.array([2661, 2193]), np.array([[ 1.   , -0.773, -0.327, -0.207],
       [ 1.   , -1.045, -0.504, -0.385]])), (np.array([4546, 2947, 3406]), np.array([[ 1.   , -0.86 , -0.984, -0.977],
       [ 1.   , -1.006, -1.08 , -1.084],
       [ 1.   , -0.834, -0.864, -0.858]])), (np.array([2916, 2916, 2809, 2861]), np.array([[ 1.   ,  1.225,  1.212,  1.189],
       [ 1.   ,  1.222,  1.211,  1.195],
       [ 1.   ,  1.228,  1.217,  1.19 ],
       [ 1.   ,  1.227,  1.214,  1.192]]))]
  par=bigtools.Parameters({'lmbd':1e-3, 'kernel_order':1})
  import classifier_cython
  cc=classifier_cython.SoftmaxClassifier(tr,par=par)
  c=SoftmaxClassifier(tr,par=par)
  for i in xrange(len(tr)):
    print "class ",i, " ", c.hard_classify(tr[i][1])
    print "class cython ",i, " ", cc.hard_classify(tr[i][1])
  pdb.set_trace()

if __name__ == "__main__":  # running interactively
  test_learn()
