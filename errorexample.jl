# Error in immutable arrays

using ImmutableArrays

#immutable Mytype
#    pos::Vector2{Float32}
#end

type Mytype
    pos::Vector2{Float32}
end

#

println(Mytype([4.; 5.]))

# prints
#Mytype(Float32[4.0,5.0])
#Mytype(Float32[0.0,2.25])
println(Mytype([4f0; 5f0]))
println(convert(Vector2{Float32},[4.;5.]))

immutable T
    x::Float32
end
