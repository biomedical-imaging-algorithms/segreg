# Tools for working with segmented images

module segimgtools

export transform_linear, create_rigid_transform, rotation_matrix
export transform_rigid, downsample_majority
export larger_size, extend_to_size, overlay
export Keypoint,Keypoint2D,find_keypoints,create_image_with_keypoints
export classimage2rgb,boundary_pixels!,draw_line!,draw_box!
export LabelType,LabelImage
export img2array

using Images
using ImageView
using Debug
using Color
using FixedPointNumbers

typealias LabelType  Uint8
typealias LabelImage  Array{LabelType}


function img2array(img)
    # convert a colour RGB image to an Uint8 array
    # return convert(Array,uint8(integer(255*separate(img))))
    return reinterpret(Uint8,convert(Array,separate(img)))
    #return reinterpret(Uint8,separate(img))
end


function transform_linear{T}(f::Array{T,2},outpsize,A::Matrix,t::Vector)
    # given a class image f(y) and a linear transformation y=Ax+t, calculate
    # a transformed image g(x) of size 'outpsize'. Nearest neighbor interpolation
    # is used as there is no sense in interpolating class labels
    # for the moment, we are making a specialized 2D version
    ngy,ngx=outpsize
    nfy,nfx=size(f)
    g=zeros(T,outpsize)
    for ix=1:ngx
        for iy=1:ngy
            #x=[float(ix);float(iy)]
            #y=integer(A*x+t) # we rely on 'integer' doing rounding
            #if 1<=y[2]<=nfy && 1<=y[1]<=nfx then
            #    g[iy,ix]=f[y[2],y[1]]
            # the following avoids garbage collection in the loop
            x1=float(iy) ; x2=float(ix) ;
            y1=integer(A[1,1]*x1+A[1,2]*x2+t[1])
            y2=integer(A[2,1]*x1+A[2,2]*x2+t[2])
            if 1<=y1<=nfy && 1<=y2<=nfx 
                g[iy,ix]=f[y1,y2]
            end
        end
    end
    return g
end


# a version of transform_linear for (possibly color) images
function transform_linear{T,N}(img::AbstractImage{T,N},outpsize,A::Matrix,t::Vector)
    @assert sdims(img)==2
    f=img2array(img)
    # numcol=size(img,colordim(img)) # number of colors
    if length(size(f))==3 # numcol>1
        # f=convert(Array,img)
        numcol=size(f,3)
        newsize=vcat(outpsize...,numcol)
        #out=zeros(T,newsize...)
        out=zeros(Uint8,newsize...)
        for i = 1:numcol # for all colors
            out[:,:,i]=transform_linear(f[:,:,i],outpsize,A,t)
        end
        #return reinterpret(Ufixed8,colorim(out))
        return colorim(reinterpret(Ufixed8,out))
    else
        # grayscale images
        return grayim(transform_linear(f,outpsize,A,t))
    end
end

function majority(inpx::Vector)
    # return the most frequent component of the array
    # if no parameter has the majority, the largest is returned
    # sort the array and find the longest run
    lx=length(inpx)
    @assert(lx>0)
    x=sort(inpx)
    runval=x[1]
    runlen=1 ;
    bestrunval=runval ; bestrunlen=runlen ;
    for i=2:lx
        if x[i]==runval
            runlen+=1
            if runlen>bestrunlen
                bestrunlen=runlen ; bestrunval=runval
            end
        else
            runval=x[i] ; runlen=1
        end
    end
    return bestrunval
end
        

function downsample_majority(f::Array{Uint8,2})
    # create an image half the size of the original one, each new pixel will be the majority vote of the four old ones
    sy=div(size(f,1),2) # new image size
    sx=div(size(f,2),2)
    g=zeros(Uint8,(sy,sx))
    for ix=1:sx
        for iy=1:sy
            g[iy,ix]=majority(reshape(f[iy*2-1:iy*2,ix*2-1:ix*2],4))
        end
    end
    return g
end

function rotation_matrix(degrees)
    # create a 2D rotation matrix given an anticlockwise angle in degrees
    f=pi*degrees/180.
    cf=cos(f) ; sf=sin(f)
    return [ cf sf ; -sf cf ]
end

function create_rigid_transform(imgsize,degrees,scale=1.0)
    # imgsize is an image size
    # shift vector is given in [y x] order
    @assert length(imgsize)==2 # 2D only for the moment
    c= 0.5*([ imgsize[1] ; imgsize[2] ].+1)  # center coordinates
    A=scale*rotation_matrix(degrees)
    return (A,c-A*c)
end

function transform_rigid(f,outpsize,angle,shift::Vector,scale::Float64=1.0)
    # transform image by rotating it by "angle" in degrees and shifting it
    # by "shift" pixels using transform_linear
    A,t=create_rigid_transform(outpsize,angle,scale)
    t+=shift
    return transform_linear(f,outpsize,A,t)
end

function larger_size(size1,size2)
    # return the larger of the sizes
    return max(size1,size2)
end

function extend_to_size(img,sz)
    # extend image img (represented by an array) to size sz
    s=size(img)
    if s==sz
        return img #no operation
    end
    sa=[s...] ; sza=[sz...] # tuple to array
    @assert all(sa.<=sza)
    prepad=div(sza-sa,2)
    postpad=sza-sa-prepad
    r=padarray(img,prepad,postpad,"value",zero(eltype(img)))
    @assert size(r)==sz
    return r
end

function combine_images{T<:Integer}(i1::Array{T},i2::Array{T})
    if length(size(i1))==2
        return i1.+i2
    else
        return div(i1.+i2,2)
    end
end

function overlay(img1,img2)
    s=larger_size(size(img1),size(img2))
    ie1=extend_to_size(img1,s)
    ie2=extend_to_size(img2,s)
    return combine_images(ie1,ie2)
end




                    
    
                    
function classimage2rgb{S<:Integer}(c::Matrix{S},k::Integer)
    # given a class image with values 0,1,..,k
    # create an RGB Uint8 image of the same size for visualization, mapping class to
    # colors. 0 always maps to white
    @assert(maximum(c)>=0)
    @assert(minimum(c)<=k)
    @assert(ndims(c)==2)
    # first prepare the color table
    tr=zeros(Uint8,k+1)
    tg=zeros(Uint8,k+1)
    tb=zeros(Uint8,k+1)
    tr[1]=tg[1]=tb[1]=255 # 0 maps to white
    c2u = x -> integer(max(0,min(255,x*255)))
    for i=1:k
        col=convert(RGB,HSV(360/k*i,1.,1.))
        tr[i+1]=c2u(col.r)
        tg[i+1]=c2u(col.g)
        tb[i+1]=c2u(col.b)
    end
    c1=c.+1
    return cat(3,tr[c1],tg[c1],tb[c1])
end
    
function boundary_pixels(a::Matrix)
    # given an image, find boundaries between areas of different values
    # and return their coordinates (y,x) using the iterator protocol
    # the boundaries (which are between pixels) are shifted to lower coordinates
    ny=size(a,1) ; nx=size(a,2)
    for ix=1:nx-1
        for iy=1:ny-1
            a0=a[iy,ix]
            if a0!=a[iy,ix+1] || a0!=a[iy+1,ix] || a0!=a[iy+1,ix+1]
                produce(iy,ix)
            end
        end
    end
end


function boundary_pixels!(f,a::Matrix)
    # given an image, find boundaries between areas of different values
    # and calls f(y,x) for each of the pixels.
    # the boundaries (which are between pixels) are shifted to lower coordinates
    ny=size(a,1) ; nx=size(a,2)
    for ix=1:nx-1
        for iy=1:ny-1
            a0=a[iy,ix]
            if a0!=a[iy,ix+1] || a0!=a[iy+1,ix] || a0!=a[iy+1,ix+1]
                f(iy,ix)
            end
        end
    end
end


sqr = x -> x*x # square a number

const black = [0x00;0x00;0x00]
const white = [0xff;0xff;0xff]
const gray = [0xa0;0xa0;0xa0]

function draw_box!(img,y0,x0,h=3,col=black)
    # set pixels in a circle of radius h around (x0,y0) to black
    # in an RGB image "img"
    @assert(ndims(img)==3 && size(img,3)==3 )
    ny=size(img,1) ; nx=size(img,2)
    const h2=h*h
    for ix=max(1,min(nx,x0-h)):max(1,min(nx,x0+h))
        for iy=max(1,min(ny,y0-h)):max(1,min(ny,y0+h))
            if sqr(ix-x0)+sqr(iy-y0)<=h2
                img[iy,ix,:]=col
            end
        end
    end
    return img
end


function draw_line!(img::Array{Uint8,3},y0,x0,v,hmin=-6,hmax=6,col=white)
    # sets pixels on a one-pixel wide line from (x0,y0)+hmin*v to
    # (x0,y0)+hmax*v to color "col" (black by default)
    # we assume that norm(v)=1
    ny=size(img,1) ; nx=size(img,2)
    @assert(abs(norm(v)-1)<0.1) 
    for h=hmin:hmax
        x=integer(x0+v[2]*h)
        y=integer(y0+v[1]*h)
        if x>=1 && x<=nx && y>=1 && y<=ny
            img[y,x,:]=col
        end
    end
    return img
end

    

end # module

