function z=kernel(t,sigma)
% evaluate a 1D kernel at points t
% sigma is a spatial extent parameter  
% the kernel is not necessarily normalized but it must be non-negative
% currently a Gaussian kernel is implemented
%
% Jan Kybic, September 2012
  
ts=2*sigma*sigma ;  
  
z=exp(-t.^2/ts) ;
%z=1./(1+(t/ts)) ;
%z=exp(-abs(t)/ts) ;